package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class EmergencyActivity extends AppCompatActivity {

    private ImageView radio_yes, radio_no;
    private TextView btn_previous, btn_next;
    private String option = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);
        initViews();
        clickViews();
    }

    private void initViews() {

        radio_yes = findViewById(R.id.radio_yes);
        radio_no = findViewById(R.id.radio_no);
        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        radio_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                option = "Yes";
                radio_yes.setImageResource(R.drawable.mark_ic);
                radio_no.setImageResource(R.drawable.unmark_ic);
            }
        });

        radio_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                option = "No";
                radio_no.setImageResource(R.drawable.mark_ic);
                radio_yes.setImageResource(R.drawable.unmark_ic);
            }
        });

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(EmergencyActivity.this, ProjectLocationActivity.class);
                startActivity(intent1);
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (option.isEmpty()) {
                    Toast.makeText(EmergencyActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                } else {
                    Utilities.saveString(EmergencyActivity.this, "emergency", option);
                    Intent intent = new Intent(EmergencyActivity.this, LocationTypeActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
