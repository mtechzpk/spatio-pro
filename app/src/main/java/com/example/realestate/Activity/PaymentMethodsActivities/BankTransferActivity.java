package com.example.realestate.Activity.PaymentMethodsActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class BankTransferActivity extends AppCompatActivity {

    private ImageView go_back;
    private Button continue_btn;
    private String packge_plan = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transfer);
        Paper.init(this);
        go_back = findViewById(R.id.go_back);
        continue_btn = findViewById(R.id.continue_btn);

        if (Utilities.getString(this, "packge_plan").equals("service")) {
            packge_plan = "service_package";

        } else {
            packge_plan = "property_package";
        }

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginApiCall1();


            }
        });
    }

    public void LoginApiCall1() {
//        final ProgressDialog progressdialog;
//        progressdialog = new ProgressDialog(LoginActivity.this);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.show();
        Utilities.showProgressDialog(BankTransferActivity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", Paper.book().read("payment_name"));
        params.put("email", Paper.book().read("payment_email"));
        params.put("phone", Paper.book().read("payment_phone"));
        params.put("payment_method", Paper.book().read("payment_method"));
        params.put("pakage_plan", packge_plan);
        params.put("id", Paper.book().read("user_id"));
        params.put("pakage_name", Utilities.getString(BankTransferActivity.this, "package_name"));

        params.put("do", "send_payment_mail");
        params.put("apikey", "mtech");


        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/send_payment_mail", BankTransferActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("success");
                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            Utilities.hideProgressDialog();
                            Utilities.hideProgressDialog();
                            Toast.makeText(BankTransferActivity.this, Message, Toast.LENGTH_SHORT).show();

                        } else {
//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
                            Toast.makeText(BankTransferActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Utilities.hideProgressDialog();
                    Toast.makeText(BankTransferActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
