package com.example.realestate.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.realestate.Activity.LoginDetail.SignUpOrLoginActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class ProfileActivity1 extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    Button logoutBtn, bLoginWithGoogle;
    TextView userName, userEmail, userId;
    ImageView profileImage;
    private GoogleApiClient googleApiClient;
    private GoogleSignInOptions gso;
    String personName, personEmail, personId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile1);

        logoutBtn = (Button) findViewById(R.id.logoutBtn);
        userName = (TextView) findViewById(R.id.name);
        userEmail = (TextView) findViewById(R.id.email);
        userId = (TextView) findViewById(R.id.userId);
        bLoginWithGoogle = findViewById(R.id.bLoginWithGoogle);
        profileImage = (ImageView) findViewById(R.id.profileImage);

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                if (status.isSuccess()) {
//                                    gotoMainActivity();
                                    showCustomDialog1();
                                } else {
                                    Toast.makeText(getApplicationContext(), "Session not close", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }
        });

        bLoginWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWithGoogleApiCall();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {

            GoogleSignInAccount account = result.getSignInAccount();
            if (account != null) {
                personName = account.getDisplayName();
                personEmail = account.getEmail();
                personId = account.getId();
                Uri personPhoto = account.getPhotoUrl();
                SessionManager sessionManager = new SessionManager(ProfileActivity1.this);
                sessionManager.createLoginSession1(personName, personEmail, personId);
                Utilities.saveString(ProfileActivity1.this, "user_id_google", personId);
            }
            userName.setText(account.getDisplayName());
            userEmail.setText(account.getEmail());
            userId.setText(account.getId());

            try {
                Glide.with(this).load(account.getPhotoUrl()).into(profileImage);
            } catch (NullPointerException e) {
                Toast.makeText(getApplicationContext(), "image not found", Toast.LENGTH_LONG).show();
            }

        } else {
            loginWithGoogleApiCall();
        }
    }

    private void gotoMainActivity() {
        Intent intent = new Intent(this, RealEstateActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(ProfileActivity1.this);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Logout?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.colorAccent,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finish();
                                pDialog.dismiss();
                            }
                        }
                )
                .show();

    }

    private void loginWithGoogleApiCall() {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(ProfileActivity1.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.login_with_google, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
                        JSONObject obj = object.getJSONObject("data");
                        int id = obj.getInt("id");
                        String google_id = obj.getString("google_id");
                        String email = obj.getString("email");

                        Utilities.saveString(ProfileActivity1.this, "user_email", email);
                        Utilities.saveString(ProfileActivity1.this, "user_google_id", google_id);
                        Utilities.saveInt(ProfileActivity1.this, "user_id", id);
                        Utilities.saveString(ProfileActivity1.this, "login", "google");

//                        Utilities.saveString(ProfileActivity1.this, "user_photo", "https://www.freeiconspng.com/thumbs/profile-icon-png/profile-icon-9.png");
                        Utilities.saveString(ProfileActivity1.this, "google_status", "yes");
                        Utilities.saveString(ProfileActivity1.this, "subscription", "yes");
                        Toast.makeText(ProfileActivity1.this, message, Toast.LENGTH_SHORT).show();
                        progressdialog.dismiss();
                        startActivity(new Intent(ProfileActivity1.this, RealEstateActivity.class));
                    } else {
                        Toast.makeText(ProfileActivity1.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "Invalid Credential";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (ProfileActivity1.this != null)
                    Toast.makeText(ProfileActivity1.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("google_id", personId);
                params.put("email", personEmail);
                params.put("name", personName);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
                return params;
            }


        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(ProfileActivity1.this).addToRequestQueue(RegistrationRequest);


    }

}
