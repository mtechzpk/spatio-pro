package com.example.realestate.Activity.help;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.realestate.R;
import com.github.barteksc.pdfviewer.PDFView;

public class PrivacyActivity extends AppCompatActivity {
    PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        pdfView=findViewById(R.id.pdfView);
        pdfView.fromAsset("privacy.pdf").load();
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}