package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.realestate.Adapters.CityAdapter;
import com.example.realestate.Models.CityEntity;
import com.example.realestate.R;
import com.github.promeg.pinyinhelper.Pinyin;
import com.github.promeg.tinypinyin.lexicons.android.cncity.CnCityDict;

import java.util.List;

import me.yokeyword.indexablerv.EntityWrapper;
import me.yokeyword.indexablerv.IndexableAdapter;
import me.yokeyword.indexablerv.IndexableLayout;
import me.yokeyword.indexablerv.SimpleHeaderAdapter;

public class AllCategoriesActivity extends AppCompatActivity {
    private List<CityEntity> mDatas;
    private SearchFragment mSearchFragment;
    private SearchView mSearchView;
    private FrameLayout mProgressBar;
    private ImageView go_back;
    private Button continue_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);

        go_back = findViewById(R.id.go_back);
        mSearchFragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.search_fragment);
        IndexableLayout indexableLayout = (IndexableLayout) findViewById(R.id.indexableLayout);
        mSearchView = (SearchView) findViewById(R.id.searchview);
        mProgressBar = (FrameLayout) findViewById(R.id.progress);
        indexableLayout.setLayoutManager(new LinearLayoutManager(this));

        Pinyin.init(Pinyin.newConfig().with(CnCityDict.getInstance(this)));
        indexableLayout.setCompareMode(IndexableLayout.MODE_FAST);
        CityAdapter adapter = new CityAdapter(this);
        indexableLayout.setAdapter(adapter);


        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AllCategoriesActivity.this.finish();
            }
        });




        // set Datas
        mDatas = FindProActivity.list;

        adapter.setDatas(mDatas, new IndexableAdapter.IndexCallback<CityEntity>() {
            @Override
            public void onFinished(List<EntityWrapper<CityEntity>> datas) {
                // 数据处理完成后回调
                mSearchFragment.bindDatas(mDatas);
                mProgressBar.setVisibility(View.GONE);
            }
        });

        // set Center OverlayView
        indexableLayout.setOverlayStyle_Center();

        // set Listener
        adapter.setOnItemContentClickListener(new IndexableAdapter.OnItemContentClickListener<CityEntity>() {
            @Override
            public void onItemClick(View v, int originalPosition, int currentPosition, CityEntity entity) {
                if (originalPosition >= 0) {
//                    Toast.makeText(MainActivity.this, entity.getName() +"  "+ currentPosition, Toast.LENGTH_SHORT).show();
//                    ToastUtil.showShort(PickCityActivity.this, "选中:" + entity.getName() + "  当前位置:" + currentPosition + "  原始所在数组位置:" + originalPosition);
                } else {
//                    Toast.makeText(MainActivity.this, entity.getName(), Toast.LENGTH_SHORT).show();
//                    ToastUtil.showShort(PickCityActivity.this, "选中Header:" + entity.getName() + "  当前位置:" + currentPosition);
                }
            }
        });

        adapter.setOnItemTitleClickListener(new IndexableAdapter.OnItemTitleClickListener() {
            @Override
            public void onItemClick(View v, int currentPosition, String indexTitle) {
            }
        });


//        mHotCityAdapter = new SimpleHeaderAdapter<>(adapter, "heat", "Populer cities", iniyHotCityDatas());
//        indexableLayout.addHeaderAdapter(mHotCityAdapter);
//        final List<CityEntity> gpsCity = iniyGPSCityDatas();
//        final SimpleHeaderAdapter gpsHeaderAdapter = new SimpleHeaderAdapter<>(adapter, "set", "Current city", gpsCity);
//        indexableLayout.addHeaderAdapter(gpsHeaderAdapter);
//        indexableLayout.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                gpsCity.get(0).setName("杭州市");
//                gpsHeaderAdapter.notifyDataSetChanged();
//            }
//        }, 3000);

        continue_btn = findViewById(R.id.continue_btn);

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AllCategoriesActivity.this, GetStartedActivity.class);
                startActivity(intent);

            }
        });

        initSearch();
    }
//    private List<CityEntity> initDatas() {
//        List<CityEntity> list = new ArrayList<>();
//        List<String> cityStrings = Arrays.asList(getResources().getStringArray(R.array.city_array));
//        for (String item : cityStrings) {
//            CityEntity cityEntity = new CityEntity();
//            cityEntity.setName(item);
//            list.add(cityEntity);
//        }
//        return list;
//    }
//    private List<CityEntity> iniyHotCityDatas() {
//        List<CityEntity> list = new ArrayList<>();
//        list.add(new CityEntity("multan"));
//        list.add(new CityEntity("karachi"));
//        list.add(new CityEntity("isb"));
//        list.add(new CityEntity("Lahore"));
//        return list;
//    }
//    private List<CityEntity> iniyGPSCityDatas() {
//        List<CityEntity> list = new ArrayList<>();
//        list.add(new CityEntity("positioning..."));
//        return list;
//    }
    private void initSearch() {
        getSupportFragmentManager().beginTransaction().hide(mSearchFragment).commit();

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.trim().length() > 0) {
                    if (mSearchFragment.isHidden()) {
                        getSupportFragmentManager().beginTransaction().show(mSearchFragment).commit();
                    }
                } else {
                    if (!mSearchFragment.isHidden()) {
                        getSupportFragmentManager().beginTransaction().hide(mSearchFragment).commit();
                    }
                }

                mSearchFragment.bindQueryText(newText);
                return false;
            }
        });
    }
}
