package com.example.realestate.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.realestate.Activity.LoginDetail.SignUpOrLoginActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.R;

public class LanguagePreActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner spinner_language;
    private Button btn_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_pre);

        btn_continue = findViewById(R.id.btn_continue);
        spinner_language = findViewById(R.id.spinner_language);

        ArrayAdapter<CharSequence> adapters = ArrayAdapter.createFromResource(this,
                R.array.language, android.R.layout.simple_spinner_item);
        adapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_language.setAdapter(adapters);
        spinner_language.setOnItemSelectedListener(this);

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LanguagePreActivity.this, SignUpOrLoginActivity.class);
                LanguagePreActivity.this.finish();
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = (String) parent.getItemAtPosition(position);
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
