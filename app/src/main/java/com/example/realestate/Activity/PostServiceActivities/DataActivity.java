package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.realestate.R;

public class DataActivity extends AppCompatActivity {
    private ImageView go_back;
    private Button continue_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        initViews();
        clickViews();
    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);
        continue_btn = findViewById(R.id.continue_btn);
    }
    private void clickViews(){
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //continue to next
                Intent intent = new Intent(DataActivity.this,YouAreActivity.class);
                startActivity(intent);
            }
        });
    }
}
