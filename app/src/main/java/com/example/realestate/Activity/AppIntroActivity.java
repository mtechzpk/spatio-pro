package com.example.realestate.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;

import com.example.realestate.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        addSlide(AppIntroFragment.newInstance("Location", "Define Location for your space...",
                R.drawable.intro1, ContextCompat.getColor(getApplicationContext(), R.color.intro1)));

        addSlide(AppIntroFragment.newInstance("Price", "Define your price range and search fiters...",
                R.drawable.intro2, ContextCompat.getColor(getApplicationContext(), R.color.intro2)));

        addSlide(AppIntroFragment.newInstance("Space", "Find your space on map or list...\nLets go!!",
                R.drawable.intro3, ContextCompat.getColor(getApplicationContext(), R.color.intro3)));

        addSlide(AppIntroFragment.newInstance("Needs", "What do you need in your space?",
                R.drawable.intro4, ContextCompat.getColor(getApplicationContext(), R.color.intro4)));

        addSlide(AppIntroFragment.newInstance("Best Providers",
                "Find and quote with the best providers of professional services...",
                R.drawable.intro5, ContextCompat.getColor(getApplicationContext(), R.color.intro5)));

        addSlide(AppIntroFragment.newInstance("Solutions",
                "Find the best solution available in your area efficiently and easily !",
                R.drawable.intro6, ContextCompat.getColor(getApplicationContext(), R.color.intro6)));
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), LanguagePreActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), LanguagePreActivity.class);
        startActivity(intent);
        finish();
    }

}
