package com.example.realestate.Activity.PostServiceActivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.realestate.Activity.AddPostActivities.EnterOtpActivity;
import com.example.realestate.Activity.AddPostActivities.PersonalInfo1Activity;
import com.example.realestate.Activity.AddPostActivities.PropertyTypeActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import static com.example.realestate.Activity.AddPostActivities.PersonalInfo1Activity.auth;

public class ServiceOtpActivity extends AppCompatActivity {

    private EditText et1, et2, et3,et4,et5,et6;
    private LinearLayout resend_linear, call_verify_linear;
    private ImageView go_back;
    private String otp="";
    private Button continue_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_otp);

        initViews();
        clickViews();

        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));
        et5.addTextChangedListener(new GenericTextWatcher(et5));
        et6.addTextChangedListener(new GenericTextWatcher(et6));
    }
    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }
        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et1:
                    if (text.length() == 1)
                        et2.requestFocus();
                    break;
                case R.id.et2:
                    if (text.length() == 1)
                        et3.requestFocus();
                    else if (text.length() == 0)
                        et1.requestFocus();
                    break;
                case R.id.et3:
                    if (text.length() == 1)
                        et4.requestFocus();
                    else if (text.length() == 0)
                        et2.requestFocus();
                    break;
                case R.id.et4:
                    if (text.length() == 1)
                        et5.requestFocus();
                    else if (text.length() == 0)
                        et3.requestFocus();
                    break;
                case R.id.et5:
                    if (text.length() == 1)
                        et6.requestFocus();
                    else if (text.length() == 0)
                        et4.requestFocus();
                    break;
                case R.id.et6:
                    if (text.length() == 0)
                        et5.requestFocus();
                    break;

            }
        }
        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    public void initViews(){
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);
        et5 = findViewById(R.id.et5);
        et6 = findViewById(R.id.et6);

        continue_btn= findViewById(R.id.continue_btn);


        go_back = findViewById(R.id.go_back);

        resend_linear = findViewById(R.id.resend_linear);
        call_verify_linear = findViewById(R.id.call_verify_linear);
    }

    public void clickViews(){
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = et1.getText().toString()+et2.getText().toString()+et3.getText().toString()+
                        et4.getText().toString()+et5.getText().toString()+et6.getText().toString();
                if (!otp.isEmpty()){
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(PersonalInfo1Activity.verificationCode, otp);
                    SigninWithPhone(credential);
                }else{
                    Utilities.makeToast(ServiceOtpActivity.this,"please Enter Otp First");
                }
            }
        });
    }

    private void SigninWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(ServiceOtpActivity.this, PropertyTypeActivity.class));
                            Toast.makeText(ServiceOtpActivity.this, "Verified successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ServiceOtpActivity.this,"Incorrect OTP", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
