package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.EnterCodeActivity;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.example.realestate.dialogs.HelpDialog;
import com.example.realestate.dialogs.PaymentAssistanceDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class UniqueCodeActivity extends AppCompatActivity {
    private Button btn_post, btn_save, btn_buy_code;
    private EditText unique_code;
    private ImageView go_back;
    EditText et1, et2, et3, et4;
    int u_id;
    String getText;
    Button readButton;

    private String filename = "SampleFile.txt";
    private String filepath = "MyFileStorage";
    File myExternalFile;
    String myData = "";
    private ArrayList<String> atentoin;
    String serviceoption;
    TextView tvPaymentAssistance, tvContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unique_code);
        initViews();
        clickViews();
        Utilities.saveString(this, "packge_plan", "service");
        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));
    }


    private void initViews() {
        go_back = findViewById(R.id.go_back);

        unique_code = findViewById(R.id.unique_code);
        btn_buy_code = findViewById(R.id.btn_buy_code);
        tvPaymentAssistance = findViewById(R.id.tvPaymentAssistance);
        btn_save = findViewById(R.id.btn_save);
        tvContactUs = findViewById(R.id.tvContactUS);
//        readButton = findViewById(R.id.getExternalStorage);
        btn_post = findViewById(R.id.btn_post);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);
        getText = et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString();

    }

    private void clickViews() {
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet(v);

            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream fos = new FileOutputStream(myExternalFile);
                    fos.write(getText.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                et1.setText("");
//                response.setText("SampleFile.txt saved to External Storage...");

                Toast.makeText(UniqueCodeActivity.this, getText + "saved to External Storage", Toast.LENGTH_SHORT).show();
            }
        });


//        readButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    FileInputStream fis = new FileInputStream(myExternalFile);
//                    DataInputStream in = new DataInputStream(fis);
//                    BufferedReader br =
//                            new BufferedReader(new InputStreamReader(in));
//                    String strLine;
//                    while ((strLine = br.readLine()) != null) {
//                        myData = myData + strLine;
//                    }
//                    in.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
////                inputText.setText(myData);
////                response.setText("SampleFile.txt data retrieved from Internal Storage...");
//            }
//        });
        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
            btn_save.setEnabled(false);
        } else {
            myExternalFile = new File(getExternalFilesDir(filepath), filename);
        }
        tvPaymentAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet1(v);
            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_buy_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UniqueCodeActivity.this, PremiumPlansActivity.class);
                startActivity(intent);

            }
        });
        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                serviceoption = Paper.book().read("serviceoption");
                serviceoption = Utilities.getString(UniqueCodeActivity.this, "serviceoption");
                atentoin = Paper.book().read("attentionList");
                u_id = Paper.book().read("user_id");
                LoginApiCall();
//                if (atentoin.size() > 0) {
//                    if (!et1.getText().toString().equals("") &&
//                            !et2.getText().toString().equals("") &&
//                            !et3.getText().toString().equals("") &&
//                            !et4.getText().toString().equals("")) {
//                        PostServiecApiCall();
//                    } else {
//                        Toast.makeText(UniqueCodeActivity.this, "Please Enter Code..!", Toast.LENGTH_SHORT).show();
//
//                    }
//
//                } else {
//                    Toast.makeText(UniqueCodeActivity.this, "Fill All fields", Toast.LENGTH_SHORT).show();
//                }

            }
        });

    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public void showBottomSheet1(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PaymentAssistanceDialog userPopUp = new PaymentAssistanceDialog();
        userPopUp.show(fragmentManager, "sms");
    }

    public void LoginApiCall() {
//        final ProgressDialog progressdialog;
//        progressdialog = new ProgressDialog(UniqueCodeActivity.this);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.show();
        Utilities.showProgressDialog(UniqueCodeActivity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();

        params.put("user_id", String.valueOf(u_id));
        params.put("personal_info", Utilities.getString(UniqueCodeActivity.this, "service_personal_info"));
        params.put("user_name", Utilities.getString(UniqueCodeActivity.this, "user_name"));
        params.put("phone", Utilities.getString(UniqueCodeActivity.this, "user_phone"));
//                params.put("personal_info", Utilities.getString(UniqueCodeActivity.this, "user_name"));
        params.put("service_type", Utilities.getString(UniqueCodeActivity.this, "user_type"));
        params.put("service_options", serviceoption);
        params.put("address", Utilities.getString(UniqueCodeActivity.this, "addres"));
        params.put("coordinate_tag", Utilities.getString(UniqueCodeActivity.this, "lati") + "," + Utilities.getString(UniqueCodeActivity.this, "lngi"));
        params.put("register_type", Utilities.getString(UniqueCodeActivity.this, "register_type"));
        params.put("name", Utilities.getString(UniqueCodeActivity.this, "ser_name"));
        params.put("register_no", Utilities.getString(UniqueCodeActivity.this, "register_no"));
        params.put("registration_type", "registration_type");
        params.put("status", "active");
        params.put("features", "features");
        params.put("description", Utilities.getString(UniqueCodeActivity.this, "service_desc"));
        params.put("certifications", Utilities.getString(UniqueCodeActivity.this, "certificate_img"));
        params.put("certification_name", Utilities.getString(UniqueCodeActivity.this, "certificate"));
        params.put("service_attention", atentoin.toString());
        params.put("service_code", et1.getText().toString() + et2.getText().toString() +
                et3.getText().toString() + et4.getText().toString());
        params.put("person_image", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "imgp"));
        params.put("image1", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img1"));
        params.put("image2", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img2"));
        params.put("image3", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img3"));
        params.put("image4", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img4"));
        params.put("image5", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img5"));
        params.put("image6", "data:image/jpeg;base64," + Utilities.getString(UniqueCodeActivity.this, "img6"));

        params.put("building_no", Utilities.getString(UniqueCodeActivity.this, "ser_building"));
        params.put("streat", Utilities.getString(UniqueCodeActivity.this, "ser_street"));
        params.put("city", Utilities.getString(UniqueCodeActivity.this, "ser_city"));
        params.put("province", Utilities.getString(UniqueCodeActivity.this, "ser_province"));
        params.put("country", Utilities.getString(UniqueCodeActivity.this, "ser_country"));
        params.put("email", Utilities.getString(UniqueCodeActivity.this, "user_email"));
        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/add_services", UniqueCodeActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("success");
//                        String Message = jsonObject.getString("data");
                        String msg = jsonObject.getString("Message");

                        if (status == 1) {
                            Utilities.hideProgressDialog();
//                            progressdialog.dismiss();
                            Toast.makeText(UniqueCodeActivity.this, msg, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(UniqueCodeActivity.this, RealEstateActivity.class));
                            finish();

                        } else {
//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
                            Toast.makeText(UniqueCodeActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(UniqueCodeActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {

            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et1:
                    if (text.length() == 1)
                        et2.requestFocus();
                    break;
                case R.id.et2:
                    if (text.length() == 1)
                        et3.requestFocus();
                    else if (text.length() == 0)
                        et1.requestFocus();
                    break;
                case R.id.et3:
                    if (text.length() == 1)
                        et4.requestFocus();
                    else if (text.length() == 0)
                        et2.requestFocus();
                    break;
                case R.id.et4:
                    if (text.length() == 0)
                        et3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    public void showBottomSheet(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HelpDialog userPopUp = new HelpDialog();
        userPopUp.show(fragmentManager, "sms");
    }
}
