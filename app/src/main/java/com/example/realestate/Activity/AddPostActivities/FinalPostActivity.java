package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class FinalPostActivity extends AppCompatActivity {
    private ImageView go_back;
    private TextInputEditText exp_price, other_charges, per_square;
    private Button post_btn;
    private CheckBox cb_duty_charges;
    String date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_post);
        initViews();
        clickViews();
    }
    private void initViews() {
        go_back = findViewById(R.id.go_back);

        exp_price = findViewById(R.id.exp_price);
        other_charges = findViewById(R.id.other_charges);
        per_square = findViewById(R.id.per_square);

        cb_duty_charges = findViewById(R.id.cb_duty_charges);

        post_btn = findViewById(R.id.post_btn);

//        String dateStr = "04/05/2010";

         date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

    }

    private void clickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(FinalPostActivity.this,EnterCodeActivity.class);
                startActivity(intent);

//                Toast.makeText(FinalPostActivity.this, "Posted..,", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
