package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.PropertyCategoryActivity;
import com.example.realestate.Adapters.CityAdapter;
import com.example.realestate.Adapters.PropertyTypeAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.CityEntity;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.github.promeg.pinyinhelper.Pinyin;
import com.github.promeg.tinypinyin.lexicons.android.cncity.CnCityDict;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;
import me.yokeyword.indexablerv.EntityWrapper;
import me.yokeyword.indexablerv.IndexableAdapter;
import me.yokeyword.indexablerv.IndexableLayout;
import me.yokeyword.indexablerv.SimpleHeaderAdapter;

public class ServiceTypeActivity extends AppCompatActivity {
    private Button continue_btn;
    private ImageView go_back;
    public static final int MODE_FAST = 0;
    private List<CityEntity> mDatas;
    //    private BlankFragment mSearchFragment;
    private SearchView mSearchView;
    private FrameLayout mProgressBar;
    private SimpleHeaderAdapter mHotCityAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_type);
        initViews();
        clickViews();

//        mSearchFragment = (BlankFragment) getSupportFragmentManager().findFragmentById(R.id.search_fragment);
        IndexableLayout indexableLayout = (IndexableLayout) findViewById(R.id.indexableLayout);
        mSearchView = (SearchView) findViewById(R.id.searchview);
        mProgressBar = (FrameLayout) findViewById(R.id.progress);
        indexableLayout.setLayoutManager(new LinearLayoutManager(this));


        Pinyin.init(Pinyin.newConfig().with(CnCityDict.getInstance(this)));
        indexableLayout.setCompareMode(MODE_FAST);
        final CityAdapter adapter = new CityAdapter(this);
        indexableLayout.setAdapter(adapter);
        // set Datas
        final ProgressDialog progressdialog = new ProgressDialog(this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/service_option", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    List<CityEntity> list = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status == 1) {
                        progressdialog.dismiss();
                        JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("service_option");
                            list.add(new CityEntity(id, name, "abc"));


                        }

                        adapter.setDatas(list, new IndexableAdapter.IndexCallback<CityEntity>() {
                            @Override
                            public void onFinished(List<EntityWrapper<CityEntity>> datas) {
                                // 数据处理完成后回调
//                mSearchFragment.bindDatas(mDatas);
                                mProgressBar.setVisibility(View.GONE);
                            }
                        });

                    } else {

                        Toast.makeText(ServiceTypeActivity.this, "No Category Avilable..!", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (this != null)
                    Toast.makeText(ServiceTypeActivity.this, message, Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "get_service_option");
                params.put("apikey", "mtech");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(this).addToRequestQueue(RegistrationRequest);

        // set Center OverlayView
        indexableLayout.setOverlayStyle_Center();

        // set Listener

        adapter.setOnItemContentClickListener(new IndexableAdapter.OnItemContentClickListener<CityEntity>() {
            @Override
            public void onItemClick(View v, int originalPosition, int currentPosition, CityEntity entity) {
                if (originalPosition >= 0) {



//                    Toast.makeText(ServiceTypeActivity.this, entity.getName() + "  " + currentPosition, Toast.LENGTH_SHORT).show();
//                    ToastUtil.showShort(PickCityActivity.this, "选中:" + entity.getName() + "  当前位置:" + currentPosition + "  原始所在数组位置:" + originalPosition);
                } else {
//                    Toast.makeText(ServiceTypeActivity.this, entity.getName(), Toast.LENGTH_SHORT).show();
//                    ToastUtil.showShort(PickCityActivity.this, "选中Header:" + entity.getName() + "  当前位置:" + currentPosition);
                }
            }
        });

        adapter.setOnItemTitleClickListener(new IndexableAdapter.OnItemTitleClickListener() {
            @Override
            public void onItemClick(View v, int currentPosition, String indexTitle) {
//                Toast.makeText(ServiceTypeActivity.this, indexTitle, Toast.LENGTH_SHORT).show();
            }
        });


//        mHotCityAdapter = new SimpleHeaderAdapter<>(adapter, "heat", "Populer cities", iniyHotCityDatas());
//        indexableLayout.addHeaderAdapter(mHotCityAdapter);
//        final List<CityEntity> gpsCity = iniyGPSCityDatas();
//        final SimpleHeaderAdapter gpsHeaderAdapter = new SimpleHeaderAdapter<>(adapter, "set", "Current city", gpsCity);
//        indexableLayout.addHeaderAdapter(gpsHeaderAdapter);
//        indexableLayout.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                gpsCity.get(0).setName("杭州市");
//                gpsHeaderAdapter.notifyDataSetChanged();
//            }
//        }, 3000);

//        initSearch();

    }

    //    private List<CityEntity> initDatas() {
//        final List<CityEntity> list = new ArrayList<>();
//
//
//
//
//    }



    private void initViews() {
        go_back = findViewById(R.id.go_back);
        continue_btn = findViewById(R.id.continue_btn);
    }

    private void clickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> atention = Paper.book().read("serviceoption");
                if (atention!=null){
                    Intent intent = new Intent(ServiceTypeActivity.this, ServiceLocationActivity.class);
                    startActivity(intent);
                }else {
                    Toast.makeText(ServiceTypeActivity.this, "Kindly Select anyone..!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
