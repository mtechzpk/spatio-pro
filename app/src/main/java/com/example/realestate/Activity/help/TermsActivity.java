package com.example.realestate.Activity.help;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.realestate.R;
import com.github.barteksc.pdfviewer.PDFView;

public class TermsActivity extends AppCompatActivity {
PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        pdfView=findViewById(R.id.pdfView);
        pdfView.fromAsset("terms.pdf").load();

    }

    @Override
    public void onBackPressed() {
  finish();
    }
}