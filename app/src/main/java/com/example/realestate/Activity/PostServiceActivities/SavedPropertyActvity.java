package com.example.realestate.Activity.PostServiceActivities;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.example.realestate.Adapters.SavedPropertiesAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.example.realestate.dialogs.SavedpropropertiesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

public class SavedPropertyActvity extends AppCompatActivity {
    int user_id;
    ImageView ivBack;
    private RecyclerView recyclerView, rvProfilePost;
    private SavedPropertiesAdapter pAdapter;
    private ArrayList<SavedpropropertiesModel> savedpropropertiesModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_property_actvity);
        user_id=Utilities.getInt(SavedPropertyActvity.this,"user_id");
        recyclerView=findViewById(R.id.rv_poemName);
        ivBack=findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getCategoriesApi();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            super.onBackPressed(); //replaced
        }
    }
    public void getCategoriesApi() {
        Utilities.showProgressDialog(SavedPropertyActvity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", String.valueOf(user_id));
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST,"http://spatiopro.com/api/my_property",SavedPropertyActvity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        savedpropropertiesModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("success");
//                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            final JSONArray objUser = object.getJSONArray("data");

                            for (int i = 0; i < objUser.length(); i++) {
//
                                JSONObject jsonObject = objUser.getJSONObject(i);

//                            String id = jsonObject.getString("id");
//                            String shopCode = jsonObject.getString("shop_code");
                                String title = jsonObject.getString("title");
                                int id = jsonObject.getInt("id");
                                String price = jsonObject.getString("price");

                                String price_unit = jsonObject.getString("price_unit");
                                String country = jsonObject.getString("country");
                                String province = jsonObject.getString("province");
                                String streat = jsonObject.getString("streat");
                                String post_unit = jsonObject.getString("post_unit");

                                String building_no = jsonObject.getString("building_no");
                                String bed = jsonObject.getString("bed");
                                String bath = jsonObject.getString("bath");
                                String floor = jsonObject.getString("floor");
                                String total_floor = jsonObject.getString("total_floor");
                                String email = jsonObject.getString("email");
                                String city = jsonObject.getString("city");
                                String category = jsonObject.getString("category");
                                String full_name = jsonObject.getString("full_name");
                                String phone_number = jsonObject.getString("phone_number");
                                String address = jsonObject.getString("address");
                                String user_image1 = jsonObject.getString("user_image");
                                String user_image = jsonObject.getString("image1");
//                            Utilities.saveString(getContext(), "name", name);
//                            Utilities.saveString(getContext(), "follow_id", follow_id);
//                            String noOfQ = jsonObject.getString("number_of_persons");

//                            Utilities.saveString(getContext(), "clerkQListId", clerkQListId);
//                            Utilities.saveString(getContext(), "noOfPerson", noOfQ);

                                savedpropropertiesModels.add(new SavedpropropertiesModel(id,post_unit,country,city,building_no,address,phone_number,price,title,email,bed,bath,floor,streat,user_image,full_name,total_floor,category,price_unit));

                            }
                            pAdapter = new SavedPropertiesAdapter(SavedPropertyActvity.this, savedpropropertiesModels);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SavedPropertyActvity.this);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setAdapter(pAdapter);


                        } else {

                            if (status == 0) {
                                String error = object.getString("message");
                                Utilities.hideProgressDialog();
                                new PrettyDialog(SavedPropertyActvity.this)
                                        .setTitle(error)
                                        .setIcon(R.drawable.pdlg_icon_info)
                                        .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                        .show();
                            }
                        }
//

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(SavedPropertyActvity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}