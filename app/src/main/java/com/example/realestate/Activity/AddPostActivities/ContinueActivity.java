package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.R;

public class ContinueActivity extends AppCompatActivity {

    ImageView back_image;
    Button continue_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_continue);
        initViews();
        clickViews();

    }

    private void initViews() {
        back_image = findViewById(R.id.back_image);
        continue_btn = findViewById(R.id.continue_btn);

    }

    private void clickViews() {
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(ContinueActivity.this, "Continue", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ContinueActivity.this, PersonalInfoActivity.class));

            }
        });
    }


}
