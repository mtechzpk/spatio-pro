package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.realestate.Activity.DetailPreviewActivity;
import com.example.realestate.Adapters.HomeScrollRecyclerAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.NearbyData;
import com.example.realestate.R;

import java.util.ArrayList;

import io.paperdb.Paper;

public class SearchListActivity extends AppCompatActivity {
    private RecyclerView bottomgrid_listview;
    private ImageView go_back;
    private HomeScrollRecyclerAdapter scrollRecyclerAdapter;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);

        go_back = findViewById(R.id.go_back);
        bottomgrid_listview = findViewById(R.id.grid_listview);

        ClickViews();

    }

    private void ClickViews() {
        ArrayList<NearbyData> nearbyData = new ArrayList<NearbyData>();
        nearbyData = Paper.book().read("nearbyDataList");


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SearchListActivity.this, LinearLayoutManager.VERTICAL, false);
        bottomgrid_listview.setLayoutManager(mLayoutManager);
        bottomgrid_listview.setItemAnimator(new DefaultItemAnimator());

        scrollRecyclerAdapter = new HomeScrollRecyclerAdapter(SearchListActivity.this,nearbyData);
        bottomgrid_listview.setAdapter(scrollRecyclerAdapter);
        scrollRecyclerAdapter.onItemClickListner(new HomeScrollRecyclerAdapter.onclickListner() {
            @Override
            public void itemClick(NearbyData object) {
//                                Toast.makeText(getActivity(), "itemClick test", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(SearchListActivity.this, DetailPreviewActivity.class);
//                                intent.putExtra("object",object.toString());
                Paper.book().write("object", object);
                Toast.makeText(SearchListActivity.this, String.valueOf(object.getAmenitieslist()), Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });


        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
