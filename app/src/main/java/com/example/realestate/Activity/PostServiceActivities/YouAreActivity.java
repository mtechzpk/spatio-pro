package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class YouAreActivity extends AppCompatActivity {

    private ImageView go_back, pro_select, company_select, technician_select;
    private Button continue_btn;
    private String selected_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_are);
        initViews();
        clickViews();

    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        pro_select = findViewById(R.id.pro_select);
        company_select = findViewById(R.id.company_select);
        technician_select = findViewById(R.id.technician_select);

        continue_btn = findViewById(R.id.continue_btn);
    }

    private void clickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_type.equals("")) {
                    Toast.makeText(YouAreActivity.this, "Please Select one..!", Toast.LENGTH_SHORT).show();
                } else {
                    Utilities.saveString(YouAreActivity.this, "service_personal_info", selected_type);
                    Intent intent = new Intent(YouAreActivity.this, ServicePersonalInfoActivity.class);
                    startActivity(intent);

                }
            }
        });

        pro_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pro_select.setImageResource(R.drawable.mark_ic);
                company_select.setImageResource(R.drawable.b);
                technician_select.setImageResource(R.drawable.b);

                selected_type = "Certified Professional";
            }
        });
        company_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pro_select.setImageResource(R.drawable.b);
                company_select.setImageResource(R.drawable.mark_ic);
                technician_select.setImageResource(R.drawable.b);
                selected_type = "Company";
            }
        });
        technician_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                company_select.setImageResource(R.drawable.b);
                pro_select.setImageResource(R.drawable.b);
                technician_select.setImageResource(R.drawable.mark_ic);
                selected_type = "Registered Technician";
            }
        });
    }
}
