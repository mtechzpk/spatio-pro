package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class PropertyTypeActivity extends AppCompatActivity {
private Button continue_btn;
private ImageView go_back,rent_select,sell_select;
private TextView user_name_set;
private String selected_type = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_type);

        continue_btn = findViewById(R.id.continue_btn);
        go_back = findViewById(R.id.go_back);
        rent_select = findViewById(R.id.rent_select);
        sell_select = findViewById(R.id.sell_select);
        user_name_set= findViewById(R.id.user_name_set);

        user_name_set.setText(Utilities.getString(this,"user_name"));
        sell_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell_select.setImageResource(R.drawable.mark_ic);
                rent_select.setImageResource(R.drawable.b);
                selected_type = "Sale";
            }
        });
        rent_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell_select.setImageResource(R.drawable.b);
                rent_select.setImageResource(R.drawable.mark_ic);
                selected_type = "Rent";
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected_type.equals("")){
                    Utilities.saveString(PropertyTypeActivity.this,"property_type" , selected_type);
                    startActivity(new Intent(PropertyTypeActivity.this,PropertyCategoryActivity.class));
                }else {
                    Toast.makeText(PropertyTypeActivity.this, "Select anyone", Toast.LENGTH_SHORT).show();
                }
            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertyTypeActivity.this.finish();
            }
        });
    }
}
