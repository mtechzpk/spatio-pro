package com.example.realestate.Activity.PostServiceActivities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImageReferentActivity extends AppCompatActivity implements View.OnClickListener {
    private CircleImageView referent_image;
    private LinearLayout camera_linear;
    private TextView btn_gallery, btn_previous, btn_next;

    Bitmap selected_img_bitmap;
    String input = "";
    String imageEncoded;
    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_referent);
        initViews();
        clickViews();
    }

    private void initViews() {

        referent_image = findViewById(R.id.referent_image);

        camera_linear = findViewById(R.id.camera_linear);
        btn_gallery = findViewById(R.id.btn_gallery);

        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        camera_linear.setOnClickListener(this);
        btn_gallery.setOnClickListener(this);

        btn_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.camera_linear:
                doProfilePhoto();
                break;

            case R.id.btn_gallery:
                doProfilePhoto();
                break;

            case R.id.btn_previous:
                ImageReferentActivity.this.finish();
                break;

            case R.id.btn_next:
                if (imageUri != null && !imageUri.equals(Uri.EMPTY)) {
                    Intent intent = new Intent(ImageReferentActivity.this, ProjectDescriptionActivity.class);
                    Bitmap immagex = selected_img_bitmap;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

                    input = imageEncoded;
                    input = input.replace("\n", "");
                    input = input.trim();
                    input = "data:image/png;base64," + input;
                    Utilities.saveString(ImageReferentActivity.this, "referent_image", input);
                    startActivity(intent);

                }else{
                    Toast.makeText(this, "select Image", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void doProfilePhoto() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropMenuCropButtonTitle("Done")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imageUri = result.getUri();


                try {

                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

                    referent_image.setImageBitmap(selected_img_bitmap);



                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
