package com.example.realestate.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.FindServicesActivities.FindProActivity;
import com.example.realestate.Activity.PostServiceActivities.DataActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class RealEstateActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout searchProperty_linear, findService_linear, postProperty_linear,
            help_linear, postService_linear;
    String newToken;
    private ImageView logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_estate);
        initViews();
//        LoginApiCall();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
//            Log.e("newToken", newToken);
//            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();

            final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/add_token", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONObject object = new JSONObject(response);
                        int status = object.getInt("Success");
                        String message = object.getString("message");
                        if (status == 1) {
                            Toast.makeText(RealEstateActivity.this, message, Toast.LENGTH_SHORT).show();
                        } else {
                            String error = object.getString("message");
                            Toast.makeText(RealEstateActivity.this, error, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ServerError) {
                        message = "The server could not be found. Please try again after some time!!";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing error! Please try again after some time!!";
                    } else if (volleyError instanceof NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection.";
                    }
                    if (RealEstateActivity.this != null)
                        Toast.makeText(RealEstateActivity.this, message, Toast.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", String.valueOf(Utilities.getInt(RealEstateActivity.this, "user_id")));
                    params.put("do", "add token");
                    params.put("token", newToken);
                    params.put("apikey", "mtech");

                    return params;
                }
            };

            RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                    25000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MySingleton.getInstance(RealEstateActivity.this).addToRequestQueue(RegistrationRequest);

        });


    }

    private void initViews() {
        logout = findViewById(R.id.logout);
        searchProperty_linear = findViewById(R.id.searchProperty_linear);
        findService_linear = findViewById(R.id.findService_linear);
        postProperty_linear = findViewById(R.id.postProperty_linear);
        help_linear = findViewById(R.id.help_linear);
        postService_linear = findViewById(R.id.postService_linear);

        //setting Onclick Listener
        searchProperty_linear.setOnClickListener(this);
        findService_linear.setOnClickListener(this);
        postProperty_linear.setOnClickListener(this);
        help_linear.setOnClickListener(this);
        postService_linear.setOnClickListener(this);

        logout.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logout:
                Logout();
                break;
            case R.id.searchProperty_linear:
                Intent intent2 = new Intent(new Intent(RealEstateActivity.this, LandingActivity.class));
                Utilities.saveString(RealEstateActivity.this, "check_for_search_login", "search_p");
                startActivity(intent2);
                break;
            case R.id.findService_linear:
                Intent intent3 = new Intent(new Intent(RealEstateActivity.this, FindProActivity.class));
                Utilities.saveString(RealEstateActivity.this, "check_for_search_login", "search_s");

                startActivity(intent3);
                break;
            case R.id.postProperty_linear:
                Intent intent = new Intent(new Intent(RealEstateActivity.this, ContinueActivity.class));
                Utilities.saveString(RealEstateActivity.this, "post_feact", "post_prop");
                startActivity(intent);
                break;
            case R.id.help_linear:
                startActivity(new Intent(RealEstateActivity.this, HelpActivity.class));
                break;
            case R.id.postService_linear:
                Intent intent1 = new Intent(new Intent(RealEstateActivity.this, DataActivity.class));
                Utilities.saveString(RealEstateActivity.this, "post_feact", "post_ser");
                startActivity(intent1);
                break;

        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
////respond to menu item selection
//
//        switch (item.getItemId()) {
//            case R.id.about:
//                Toast.makeText(RealEstateActivity.this, "About", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.logout:
//                Logout();
//                return true;
//
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }
//
    private void Logout() {
        new AlertDialog.Builder(this)
                .setTitle("Logout")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        SessionManager sessionManager = new SessionManager(RealEstateActivity.this);
                        sessionManager.logoutUser();
                        Utilities.clearSharedPref(RealEstateActivity.this);
                        Toast.makeText(RealEstateActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent parentIntent = NavUtils.getParentActivityIntent(this);
                if(parentIntent == null) {
                    finish();
                    return true;
                } else {
                    parentIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(parentIntent);
                    finish();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void LoginApiCall() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", Utilities.getString(RealEstateActivity.this,"user_email"));
        params.put("password", Utilities.getString(RealEstateActivity.this,"user_password"));
        params.put("apikey", "travces.com");
        params.put("do", "login_user");
        params.put("token", newToken);


        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/login_user", RealEstateActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

//                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("Success");
//                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            JSONObject obj = jsonObject.getJSONObject("result");
                            SessionManager sessionManager = new SessionManager(RealEstateActivity.this);

                            int id = obj.getInt("id");
                            String email = obj.getString("email");
                            String name = obj.getString("name");
                            Utilities.saveInt(RealEstateActivity.this, "user_id", id);

//                        String user_id = obj.getString("id");
//                        String photooo = obj.getString("photo");

                            //                        String photo = Server.BASE_URL_PHOTO + obj.getString("photo");

//                            Paper.book().write("user_id", id);
                            sessionManager.createLoginSession(name, email, id);

//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();

                        } else {
//                            progressdialog.dismiss();
//                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
//                            Toast.makeText(RealEstateActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
//                    Utilities.hideProgressDialog();
                    Toast.makeText(RealEstateActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
