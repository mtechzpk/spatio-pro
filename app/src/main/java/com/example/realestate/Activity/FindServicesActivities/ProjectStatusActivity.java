package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Activity.PostServiceActivities.ProjectTimeActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class ProjectStatusActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView radio_hire, radio_planing, radio_parts;
    private TextView btn_previous, btn_next;
    private String project_status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_status);
        initViews();
        clickViews();
    }

    private void initViews() {
        radio_hire = findViewById(R.id.radio_hire);
        radio_planing = findViewById(R.id.radio_planing);
        radio_parts = findViewById(R.id.radio_parts);

        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        radio_hire.setOnClickListener(this);
        radio_planing.setOnClickListener(this);
        radio_parts.setOnClickListener(this);

        btn_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radio_hire:
                project_status = "hire";
                radio_planing.setImageResource(R.drawable.unmark_ic);
                radio_parts.setImageResource(R.drawable.unmark_ic);
                radio_hire.setImageResource(R.drawable.mark_ic);
                break;

            case R.id.radio_planing:
                project_status = "planning";
                radio_planing.setImageResource(R.drawable.mark_ic);
                radio_parts.setImageResource(R.drawable.unmark_ic);
                radio_hire.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_parts:
                project_status = "spare_parts";
                radio_planing.setImageResource(R.drawable.unmark_ic);
                radio_parts.setImageResource(R.drawable.mark_ic);
                radio_hire.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.btn_previous:
                ProjectStatusActivity.this.finish();
                break;

            case R.id.btn_next:
                if (project_status.isEmpty() || project_status.equals(" ")){
                    Toast.makeText(ProjectStatusActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(ProjectStatusActivity.this, ProjectTimeActivity.class);
                    Utilities.saveString(ProjectStatusActivity.this,"project_status",project_status);
                    startActivity(intent);
                }
                break;
        }
    }
}
