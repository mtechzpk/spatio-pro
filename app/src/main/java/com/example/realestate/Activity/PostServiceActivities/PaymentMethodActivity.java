package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.realestate.Activity.PaymentMethodsActivities.BankTransferActivity;
import com.example.realestate.Activity.PaymentMethodsActivities.CashPaymentActivity;
import com.example.realestate.Activity.PaymentMethodsActivities.DigitalPaymentActivity;
import com.example.realestate.Activity.PaymentMethodsActivities.PaypalActivity;
import com.example.realestate.R;

import io.paperdb.Paper;

public class PaymentMethodActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_have_code, btn_cancel;
    private LinearLayout linear_paypal, linear_cash, linear_bank, linear_assistant;
    private ImageView go_back;

//    private final static String TAG = "Paypal";
//    public static final String PAYPAL_KEY = "";
//
//    private static final int REQUEST_CODE_PAYMENT=1;
//    private static final int REQUEST_CODE_FUTURE_PAYMENT=2;
//    private static final String CONFIG_ENVIRONMENT= PayPalConfiguration.ENVIRONMENT_SANDBOX;
//    private static PayPalConfiguration config;
//    PayPalPayment thingsToBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        Paper.init(this);
        initViews();
        clickViews();
//        configPayPal();
    }

    private void initViews() {

        go_back = findViewById(R.id.go_back);

        btn_have_code = findViewById(R.id.btn_have_code);
        btn_cancel = findViewById(R.id.btn_cancel);

        linear_paypal = findViewById(R.id.linear_paypal);
        linear_cash = findViewById(R.id.linear_cash);
        linear_bank = findViewById(R.id.linear_bank);
        linear_assistant = findViewById(R.id.linear_assistant);
    }

    private void clickViews() {
        go_back.setOnClickListener(this);

        btn_have_code.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);

        linear_paypal.setOnClickListener(this);
        linear_cash.setOnClickListener(this);
        linear_bank.setOnClickListener(this);
        linear_assistant.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.go_back:
                PaymentMethodActivity.this.finish();
                break;
                case R.id.btn_have_code:
                    PaymentMethodActivity.this.finish();
                break;
            case R.id.btn_cancel:
                PaymentMethodActivity.this.finish();
                break;

            case R.id.linear_paypal:
                Intent intent1 = new Intent(PaymentMethodActivity.this, PaypalActivity.class);
                Paper.book().write("payment_method","Paypal");
                startActivity(intent1);
//                MakePayment();

                break;
            case R.id.linear_cash:
                Intent intent2 = new Intent(PaymentMethodActivity.this, CashPaymentActivity.class);
                Paper.book().write("payment_method","Cash");
                startActivity(intent2);
                break;
            case R.id.linear_bank:
                Intent intent3 = new Intent(PaymentMethodActivity.this, BankTransferActivity.class);
                Paper.book().write("payment_method","Bank Transfer");

                startActivity(intent3);
                break;
            case R.id.linear_assistant:
                Intent intent4 = new Intent(PaymentMethodActivity.this, DigitalPaymentActivity.class);
                Paper.book().write("payment_method","Digital Payment");

                startActivity(intent4);
                break;
        }
    }
}
