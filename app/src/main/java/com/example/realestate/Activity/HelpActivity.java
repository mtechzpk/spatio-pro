package com.example.realestate.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.realestate.R;
import com.example.realestate.dialogs.HelpDialog;
import com.example.realestate.dialogs.PaymentAssistanceDialog;

public class HelpActivity extends AppCompatActivity {
    TextView tvAssistance, tvContactUs;
    ImageView back_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        tvAssistance = findViewById(R.id.tvPaymentAssistance);
        tvContactUs = findViewById(R.id.tvContactUS);
        back_image = findViewById(R.id.back_image);
        tvAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                PaymentAssistanceDialog userPopUp = new PaymentAssistanceDialog();
                userPopUp.show(fragmentManager, "sms");
            }
        });
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet(v);
            }
        });
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void showBottomSheet(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HelpDialog userPopUp = new HelpDialog();
        userPopUp.show(fragmentManager, "sms");
    }
}