package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.LoginDetail.ResetPasswordActivity;
import com.example.realestate.Activity.LoginDetail.SignUpActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class PostServiceLoginActivity extends AppCompatActivity {
    TextView reset_password_text, signup_text;
    Button login_btn;
    ImageView go_back;
    TextInputEditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_service_login);
        initViews();
        clickViews();
    }
    public void  initViews(){
        reset_password_text = findViewById(R.id.reset_password_text);
        signup_text = findViewById(R.id.signup_text);
        login_btn = findViewById(R.id.login_btn);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        go_back = findViewById(R.id.go_back);
    }

    public void clickViews(){

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        reset_password_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostServiceLoginActivity.this, ResetPasswordActivity.class));
            }
        });

        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostServiceLoginActivity.this, SignUpActivity.class));
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(LoginActivity.this, "Hit API Here", Toast.LENGTH_SHORT).show();

                if (!email.getText().toString().isEmpty()&&!password.getText().toString().isEmpty()){

                    LoginApiCall(email.getText().toString(),password.getText().toString());
//                startActivity(new Intent(LoginActivity.this, PropertyCategoryActivity.class));
                }else {
                    Toast.makeText(PostServiceLoginActivity.this, "please fill in all required fields..!", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    public void LoginApiCall(final String email, final String password) {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(PostServiceLoginActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();
//
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        params.put("apikey", "travces.com");
        params.put("do", "login_user");


        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/login_user", PostServiceLoginActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("Success");
//                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            JSONObject obj = jsonObject.getJSONObject("result");
                            SessionManager sessionManager = new SessionManager(PostServiceLoginActivity.this);

                            int id = obj.getInt("id");
                            String email = obj.getString("email");
                            String name = obj.getString("name");
//                        String user_id = obj.getString("id");
//                        String photooo = obj.getString("photo");

                            //                        String photo = Server.BASE_URL_PHOTO + obj.getString("photo");

                            Paper.book().write("user_id", id);

                            Utilities.saveInt(PostServiceLoginActivity.this, "user_id", id);


                            sessionManager.createLoginSession(name, email, id);

                            progressdialog.dismiss();
                            Toast.makeText(PostServiceLoginActivity.this,"Login Successfuly", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(PostServiceLoginActivity.this, DataActivity.class));
                            finish();

                        } else {
                            Utilities.hideProgressDialog();
                            progressdialog.dismiss();
                            String message = jsonObject.getString("message");

                            Toast.makeText(PostServiceLoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Utilities.hideProgressDialog();
                    Toast.makeText(PostServiceLoginActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
