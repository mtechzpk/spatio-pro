package com.example.realestate.Activity.PostServiceActivities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.realestate.Activity.AddPostActivities.PropertyDetailActivity;
import com.example.realestate.Activity.AddPostActivities.PropertyLocationActivity;
import com.example.realestate.Activity.Custom.CheckConnection;
import com.example.realestate.Activity.Custom.GPSTracker;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.WorkaroundMapFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.thebrownarrow.permissionhelper.ActivityManagePermission;
import com.thebrownarrow.permissionhelper.PermissionResult;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ServiceLocationActivity extends ActivityManagePermission implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private ImageView go_back;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private Button continue_btn;
    ScrollView mScrollView;
    private String permissionAsk[] = {PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE,
            PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_ACCESS_FINE_LOCATION,
            PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Double currentLatitude = null;
    private Double currentLongitude = null;
    private String catg_name = "", building = "", street = "", city = "",
            country = "", province="", address = "", service_location="";
    private EditText building_no, street_ad, city_ad, country_name, province_ad;
    private String map_key = "AIzaSyCzd6uOtpcuGqik5zp0xvlRZpUhIl3gJ0U";

    public ServiceLocationActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_location);
        Places.initialize(this, map_key);
        initViews();
        clickViews();
        Intent intent = getIntent();
        catg_name = intent.getStringExtra("property_cat_name");
        if (!CheckConnection.haveNetworkConnection(this)) {
            Toast.makeText(this, getString(R.string.network), Toast.LENGTH_LONG).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {
                    if (!GPSEnable()) {
                        tunonGps();
                    } else {
                        getcurrentlocation();
                    }
                }

                @Override
                public void permissionDenied() {

                }

                @Override
                public void permissionForeverDenied() {

                    openSettingsApp(ServiceLocationActivity.this);
                }
            });

        } else {
            if (!GPSEnable()) {
                tunonGps();
            } else {
                getcurrentlocation();
            }

        }
    }

    public Boolean GPSEnable() {

        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            return true;

        } else {
            return false;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public void tunonGps() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(30 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            getcurrentlocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and setting the result in onActivityResult().
                                status.startResolutionForResult(ServiceLocationActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }

    public void getcurrentlocation() {

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            if (resultCode == RESULT_OK) {
                String result = data.getStringExtra("result");
                getcurrentlocation();

            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
//        else if (requestCode == PLACE_PICKER_REQUEST) {
//            if (resultCode == RESULT_OK) {
//                pickup = PlaceAutocomplete.getPlace(getActivity(), data).freeze();
//                pickup_location.setText(pickup.getAddress());
//                pickuplloction = pickup.getLatLng();
//            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
//                Toast.makeText(getActivity(), status.getStatusMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }

    }

    private void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                street = street_ad.getText().toString().trim();
                building = building_no.getText().toString().trim();
                province = province_ad.getText().toString().trim();
                city = city_ad.getText().toString().trim();
                country = country_name.getText().toString().trim();
                service_location = building + " " + street + " " + city +" " +province+ " " + country;

                if (!building.isEmpty()) {
                    if (!street.isEmpty()) {
                        if (!city.isEmpty()) {
                            if (!province.isEmpty()) {
                                if (!country.isEmpty()) {
                                    if (currentLatitude == null && currentLongitude == null) {
                                        Toast.makeText(ServiceLocationActivity.this, "Please Select your property location on map...!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(ServiceLocationActivity.this, ServiceDetailActivity.class);
                                        intent.putExtra("cat_name", catg_name);
                                        Utilities.saveString(ServiceLocationActivity.this, "addres", address);
                                        Utilities.saveString(ServiceLocationActivity.this, "lati", currentLatitude.toString());
                                        Utilities.saveString(ServiceLocationActivity.this, "lngi", currentLongitude.toString());
                                        Utilities.saveString(ServiceLocationActivity.this, "cat_nam", catg_name);
                                        Utilities.saveString(ServiceLocationActivity.this, "service_location", service_location);

                                        Utilities.saveString(ServiceLocationActivity.this,"ser_building",building);
                                        Utilities.saveString(ServiceLocationActivity.this,"ser_street",street);
                                        Utilities.saveString(ServiceLocationActivity.this,"ser_city",city);
                                        Utilities.saveString(ServiceLocationActivity.this,"ser_province",province);
                                        Utilities.saveString(ServiceLocationActivity.this,"ser_country",country);

                                        startActivity(intent);
                                    }

                                } else {
                                    country_name.setError("Country required");
                                }
                            }else {
                                province_ad.setError("Province Required");
                            }
                        } else {
                            city_ad.setError("City required");
                        }
                    } else {
                        street_ad.setError("Street required");
                    }
                } else {
                    building_no.setError("Building number required");
                }


            }
        });

    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        building_no = findViewById(R.id.building_no);
        province_ad = findViewById(R.id.province_ad);
        street_ad = findViewById(R.id.street_ad);
        city_ad = findViewById(R.id.city_ad);
        country_name = findViewById(R.id.country_name);

        continue_btn = findViewById(R.id.continue_btn);

        WorkaroundMapFragment mapFragment = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map));
        mScrollView = (ScrollView) findViewById(R.id.mScrollView);

        ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ADDRESS);
        if (fields != null) {
            autocompleteFragment.setPlaceFields(fields);
        }

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
//                Toast.makeText(PropertyLocationActivity.this, "Place: " + place.getName() + ", " + place.getLatLng(), Toast.LENGTH_SHORT).show();

                address = place.getAddress();
                LatLng latLng = place.getLatLng();
                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;
                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.title(currentLatitude + " : " + currentLongitude);
                    map.clear();
                    map.addMarker(markerOptions);

                    CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15);
                    map.animateCamera(camera);
                } else {
                    Toast.makeText(ServiceLocationActivity.this, getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Toast.makeText(ServiceLocationActivity.this, "An error occurred: " + status, Toast.LENGTH_SHORT).show();
            }
        });
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

//      map types
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().isMyLocationButtonEnabled();
//        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                // Clears the previously touched position
                map.clear();

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                map.addMarker(markerOptions);

                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;


                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(ServiceLocationActivity.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(currentLatitude, currentLongitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (!addresses.isEmpty()) {

                    address = addresses.get(0).getAddressLine(0);
                }// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//                String city = addresses.get(0).getLocality();
//                String state = addresses.get(0).getAdminArea();
//                String country = addresses.get(0).getCountryName();
//                String postalCode = addresses.get(0).getPostalCode();
//                String knownName = addresses.get(0).getFeatureName();
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                //TODO check error here krny wala rehta h ye
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);
            } else {
                Double currentLatitude;
                Double currentLongitude;
                //If everything went fine lets get latitude and longitude
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {

//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.title(currentLatitude + " : " + currentLongitude);
//                    map.clear();
//                    map.addMarker(markerOptions);


                    CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15);
                    map.animateCamera(camera);
                } else {

                    Toast.makeText(this, getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {
                }

                @Override
                public void permissionDenied() {
                }

                @Override
                public void permissionForeverDenied() {
                    Toast.makeText(ServiceLocationActivity.this, R.string.allow_permission, Toast.LENGTH_SHORT).show();
//                    Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                    openSettingsApp(ServiceLocationActivity.this);
                }
            });

        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(this, "onConnectionSuspended", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(this, "onLocationChanged", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(this, "onStatusChanged", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "onProviderEnabled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "onProviderDisabled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "onConnectionFailed", Toast.LENGTH_SHORT).show();
    }
}
