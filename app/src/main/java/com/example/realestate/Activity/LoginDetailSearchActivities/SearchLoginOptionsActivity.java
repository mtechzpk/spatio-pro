package com.example.realestate.Activity.LoginDetailSearchActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.LoginDetail.SignUpOrLoginActivity;
import com.example.realestate.Activity.PostServiceActivities.PostServiceLoginActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class SearchLoginOptionsActivity extends AppCompatActivity {

    private LinearLayout google_btn_linear, facebook_btn_linear, email_btn_linear;
    private ImageView go_back;
    String data="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_login_options);

        data = Utilities.getString(this,"check_for_search_login");

        InitViews();
        clickViews();
    }

    private void InitViews() {
        go_back = findViewById(R.id.go_back);

        google_btn_linear = findViewById(R.id.google_btn_linear);
        facebook_btn_linear = findViewById(R.id.facebook_btn_linear);
        email_btn_linear = findViewById(R.id.email_btn_linear);
    }

    private void clickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        google_btn_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sign in with google account
            }
        });

        facebook_btn_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sign in with facebook account
            }
        });

        email_btn_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sign in with email account
                if (data.equals("search_p")){

                    Intent intent = new Intent(SearchLoginOptionsActivity.this, LoginSearchActivity.class);
                    startActivity(intent);
                }else {
                    Intent intent1 = new Intent(SearchLoginOptionsActivity.this, SearchServiceLoginActivity.class);
                    startActivity(intent1);

                }
            }
        });
    }
}
