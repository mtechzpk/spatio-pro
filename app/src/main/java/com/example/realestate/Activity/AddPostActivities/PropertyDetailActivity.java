package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.PostServiceActivities.ImageReferentActivity;
import com.example.realestate.Activity.PostServiceActivities.ServiceDetailActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Adapters.ServiceAttentionAdapter;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.AreaTypeModel;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.Models.ServiceAttentionModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.example.realestate.dialogs.MonthYearPickerDialog;
import com.google.gson.JsonObject;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;
import com.yanzhenjie.years.YearsView;

import net.alhazmy13.mediapicker.Image.ImagePicker;
import net.alhazmy13.mediapicker.Video.VideoPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.paperdb.Paper;

public class PropertyDetailActivity extends AppCompatActivity
        implements View.OnClickListener, AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {
    private Spinner spinner_area_unit, spinner_type, spPropertFeaturs, price_unit, sppostUnit;
    private EditText area, area_title, area_description, area_price, bedrooms,
            bathrooms, floor_level, construction_area, neighbourhood;
    private ImageView image1, image2, image3, image4, image5, image6, ivConstructionYear;
    private String image, area_unit;
    private LinearLayout flats_data, property_type;
    private RadioGroup rg_furnished;
    private RadioButton rb_yes, rb_no;
    private Button btn_continue;
    private ImageView go_back, ivCalender;
    private static final int SELECT_VIDEO = 1;
    DatePickerDialog picker;
    SimpleDateFormat simpleDateFormat;
    String input1 = "", input5 = "", input2 = "", input3 = "", input4 = "", input6 = "";
    String monthYearStr, get_property_feature, get_area_type;
    Uri uri1 = null, uri2 = null, uri3 = null, uri4 = null, uri5 = null, uri6 = null;

    SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
    String submisssionDate, getpriceUnit, getPostUnit;
    private String selectedVideoPath;
    TextView tvSubmissionDate, tvYearofConstructins;
    List<String> prop_type, area_type;
    private RecyclerView rv_service_aminities;
    private String IMG1 = "", img2 = "", img3 = "", img4 = "", img5 = "", img6 = "", catg_name, area_name;
    private ArrayList<ServiceAttentionModel> serviceAttentionModels;
    private ServiceAttentionAdapter serviceAttentionAdapter;
    private Bitmap u_img1 = null, u_img2 = null, u_img3 = null, u_img4 = null, u_img5 = null, u_img6 = null;
    final String[] str = {"price unit", "DOP", "USD"};
    final String[] postUnit = {"Select Post Unit", "PP", "PS", "PU"};
    Bitmap selected_img_bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_detail);
        simpleDateFormat = new SimpleDateFormat("dd MM yyyy", Locale.US);

        initViews();
        clickViews();
        initAdapter();
        Intent intent = getIntent();
        catg_name = intent.getStringExtra("cat_name");
        ArrayAdapter<String> adp1 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, str);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        price_unit.setAdapter(adp1);
        price_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                getpriceUnit = str[position];
                Utilities.saveString(PropertyDetailActivity.this, "price_unit", getpriceUnit);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, postUnit);
        adp2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sppostUnit.setAdapter(adp2);
        sppostUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                // TODO Auto-generated method stub
                getPostUnit = postUnit[position];
                Utilities.saveString(PropertyDetailActivity.this, "post_unit", getPostUnit);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        getprop_type();
        getareaunit();

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
        image5.setOnClickListener(this);
        image6.setOnClickListener(this);

//
//        if (catg_name.equals("Lands & Plots")) {
//            property_type.setVisibility(View.VISIBLE);
//            flats_data.setVisibility(View.GONE);
//            floor_level.setVisibility(View.GONE);
//        } else if (catg_name.equals("Houses")) {
//            property_type.setVisibility(View.GONE);
//            flats_data.setVisibility(View.VISIBLE);
//            floor_level.setVisibility(View.GONE);
//
//        } else if (catg_name.equals("Apartments & Flats")) {
//            property_type.setVisibility(View.GONE);
//            flats_data.setVisibility(View.VISIBLE);
//            floor_level.setVisibility(View.VISIBLE);
//
//        } else if (catg_name.equals("Shops - Offices - Commercial Space")) {
//            property_type.setVisibility(View.GONE);
//            flats_data.setVisibility(View.GONE);
//            floor_level.setVisibility(View.GONE);
//
//        } else if (catg_name.equals("Portions & Floors")) {
//            property_type.setVisibility(View.GONE);
//            flats_data.setVisibility(View.VISIBLE);
//            floor_level.setVisibility(View.VISIBLE);
//
//        } else {
//            property_type.setVisibility(View.GONE);
//            flats_data.setVisibility(View.VISIBLE);
//            floor_level.setVisibility(View.VISIBLE);
//        }


    }

    private void initAdapter() {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(PropertyDetailActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/amenities",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject object = new JSONObject(response);
                            int status = object.getInt("Success");
                            if (status == 1) {
                                serviceAttentionModels = new ArrayList<>();

                                JSONArray objUser = object.getJSONArray("data");
                                for (int i = 0; i < objUser.length(); i++) {
                                    JSONObject jsonObject = objUser.getJSONObject(i);
                                    int id = jsonObject.getInt("id");
                                    String name = jsonObject.getString("aminties");
//                            propertyTypeModels.add(new PropertyTypeModel(id,name));
                                    serviceAttentionModels.add(new ServiceAttentionModel(name, id));

                                }
                                progressdialog.dismiss();
                                serviceAttentionAdapter = new ServiceAttentionAdapter(PropertyDetailActivity.this, serviceAttentionModels);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PropertyDetailActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                                rv_service_aminities.setLayoutManager(linearLayoutManager);
                                rv_service_aminities.setAdapter(serviceAttentionAdapter);
                                rv_service_aminities.setNestedScrollingEnabled(false);
                            } else {
//                                String message = object.getString("message");
                                Toast.makeText(PropertyDetailActivity.this, "Something wrong", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressdialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (PropertyDetailActivity.this != null)
                    Toast.makeText(PropertyDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("em", email);
////                params.put("token", token);
//                params.put("do", "amenities");
//                params.put("apikey", "travces.com");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(PropertyDetailActivity.this).addToRequestQueue(RegistrationRequest);

    }

    private void getprop_type() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/all_property_feature", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status == 1) {

                        prop_type = new ArrayList<>();
                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("feature");
                            String type = jsonArray.get(i).toString();
                            prop_type.add(name);
                        }

                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PropertyDetailActivity.this, android.R.layout.simple_spinner_item, prop_type);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_type.setAdapter(dataAdapter);
                        prop_type.add(0, "Select Property feature");
                        spinner_type.setSelection(0);
                        spinner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                get_property_feature = parent.getItemAtPosition(position).toString();
                                Utilities.saveString(PropertyDetailActivity.this, "prop_fect", get_property_feature);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
                        String message = object.getString("message");
                        Toast.makeText(PropertyDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (PropertyDetailActivity.this != null)
                    Toast.makeText(PropertyDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                params.put("do", "all_property_feature");
                params.put("apikey", "travces.com");
//                params.put("property_type",catg_name);

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(PropertyDetailActivity.this).addToRequestQueue(RegistrationRequest);


    }

    private void getareaunit() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/area_unit", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<AreaTypeModel> areaTypeModels = new ArrayList<AreaTypeModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status == 1) {

                        area_type = new ArrayList<>();
                        JSONObject jsonObject;
                        JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            area_name = jsonObject.getString("area_unit");
                            areaTypeModels.add(new AreaTypeModel(id, area_name));
                            area_type.add(area_name);

                        }


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(PropertyDetailActivity.this, android.R.layout.simple_spinner_item, area_type);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_area_unit.setAdapter(dataAdapter);
                        area_type.add(0, "Select area unit");
                        spinner_area_unit.setSelection(0);
                        spinner_area_unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                get_area_type = parent.getItemAtPosition(position).toString();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
                        String message = object.getString("message");
                        Toast.makeText(PropertyDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (PropertyDetailActivity.this != null)
                    Toast.makeText(PropertyDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                params.put("do", "area_units");
                params.put("apikey", "travces.com");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(PropertyDetailActivity.this).addToRequestQueue(RegistrationRequest);


    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        spinner_area_unit = findViewById(R.id.spinner_area_unit);
        spinner_type = findViewById(R.id.spinner_type);
        area = findViewById(R.id.area);
        tvSubmissionDate = findViewById(R.id.tvSubmissionDate);
        area_title = findViewById(R.id.area_title);
        area_description = findViewById(R.id.area_description);
        area_price = findViewById(R.id.area_price);
        tvYearofConstructins = findViewById(R.id.tvYearofConstructins);
        floor_level = findViewById(R.id.floor_level);
        price_unit = findViewById(R.id.price_unit);
        neighbourhood = findViewById(R.id.neighbourhood);
        ivCalender = findViewById(R.id.ivCalender);

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        ivConstructionYear = findViewById(R.id.ivConstructionYear);
        image6 = findViewById(R.id.image6);

        bedrooms = findViewById(R.id.bedrooms);
        bathrooms = findViewById(R.id.bathrooms);
        flats_data = findViewById(R.id.flats_data);
        property_type = findViewById(R.id.property_type);
        construction_area = findViewById(R.id.construction_area);
        sppostUnit = findViewById(R.id.sppostUnit);

        rg_furnished = findViewById(R.id.rg_furnished);
        rb_yes = findViewById(R.id.rb_yes);
        rb_no = findViewById(R.id.rb_no);

        rv_service_aminities = findViewById(R.id.rv_service_aminities);

        btn_continue = findViewById(R.id.btn_continue);


    }

    private void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ivConstructionYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YearsDialog();

            }
        });
        ivCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
//                picker.getDatePicker().setCalendarViewShown(false);

                // date picker dialog
                picker = new DatePickerDialog(PropertyDetailActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                                picker.getDatePicker().setCalendarViewShown(false);
                                submisssionDate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                                Utilities.saveString(PropertyDetailActivity.this, "submission_date", submisssionDate);
                                tvSubmissionDate.setText(submisssionDate);
                            }
                        }, year, month, day);
                picker.show();
            }
        });
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (get_property_feature.equals("Select Property feature")) {
//                    Toast.makeText(PropertyDetailActivity.this, "select property feature first", Toast.LENGTH_SHORT).show();
//                }
                if (u_img1 != null) {
                    Bitmap immagex = u_img1;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input1 = imageEncoded;
                    input1 = input1.replace("\n", "");
                    input1 = input1.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img1", input1);
                }
                if (u_img2 != null) {
                    Bitmap immagex = u_img2;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input2 = imageEncoded;
                    input2 = input2.replace("\n", "");
                    input2 = input2.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img2", input2);
                }
                if (u_img3 != null) {
                    Bitmap immagex = u_img3;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input3 = imageEncoded;
                    input3 = input3.replace("\n", "");
                    input3 = input3.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img3", input3);
                }
                if (u_img4 != null) {
                    Bitmap immagex = u_img4;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input4 = imageEncoded;
                    input4 = input4.replace("\n", "");
                    input4 = input4.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img4", input4);
                }
                if (u_img5 != null) {
                    Bitmap immagex = u_img5;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input5 = imageEncoded;
                    input5 = input5.replace("\n", "");
                    input5 = input5.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img5", input5);
                }
                if (u_img6 != null) {
                    Bitmap immagex = u_img6;
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                    input6 = imageEncoded;
                    input6 = input6.replace("\n", "");
                    input6 = input6.trim();
                    Utilities.saveString(PropertyDetailActivity.this, "img6", input6);
                }

                if (u_img1 != null) {

                    if (!(get_property_feature.equals("Select Property feature"))) {
                        if (!(get_area_type.equals("Select area unit"))) {
                            if (!(getpriceUnit.equals("price unit"))) {
                                if (!(getPostUnit.equals("Select Post Unit"))) {
                                    Utilities.saveString(PropertyDetailActivity.this, "area", area.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "construction_area", construction_area.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "area_title", area_title.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "area_description", area_description.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "bedrooms", bedrooms.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "bathrooms", bathrooms.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "floor_level", floor_level.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "area_price", area_price.getText().toString());
                                    Utilities.saveString(PropertyDetailActivity.this, "neighbourhood", neighbourhood.getText().toString());
                                    startActivity(new Intent(PropertyDetailActivity.this, PropertyStatusActivity.class));

                                } else {
                                    Toast.makeText(PropertyDetailActivity.this, "Please Select price unit first", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(PropertyDetailActivity.this, "Please Select area unit first", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(PropertyDetailActivity.this, "Please Select area unit first", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(PropertyDetailActivity.this, "Please Select a property feature first", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(PropertyDetailActivity.this, "Please Select Image", Toast.LENGTH_SHORT).show();

                }


            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        area_unit = "" + parent.getItemAtPosition(position);
//        Toast.makeText(parent.getContext(), area_unit, Toast.LENGTH_SHORT).show();
        Utilities.saveString(PropertyDetailActivity.this, "areaunit", area_unit);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image1:
                IMG1 = "1";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
            case R.id.image2:
                img2 = "2";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
            case R.id.image3:
                img3 = "3";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
            case R.id.image4:
                img4 = "4";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
            case R.id.image5:
                image = "five";
                img5 = "5";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
            case R.id.image6:
                image = "six";
                img6 = "6";
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(this);
                break;
//            case R.id.image6:
//                new VideoPicker.Builder(PropertyDetailActivity.this)
//                        .mode(VideoPicker.Mode.CAMERA_AND_GALLERY)
//                        .directory(VideoPicker.Directory.DEFAULT)
//                        .extension(VideoPicker.Extension.MP4)
//                        .enableDebuggingMode(true)
//                        .build();
//                break;
        }
    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && IMG1.equals("1")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//
//            image1.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//            u_img1 = BitmapFactory.decodeFile(mPaths.get(0));
////            Bitmap selected_img_bitmap = MediaStore.Images.Media.getBitmap(PropertyDetailActivity.this.getContentResolver(), BitmapFactory.decodeFile(mPaths.get(0)));
//
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img2.equals("2")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//
//            image2.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//            u_img2 = BitmapFactory.decodeFile(mPaths.get(0));
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img3.equals("3")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//            u_img3 = BitmapFactory.decodeFile(mPaths.get(0));
//            image3.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img4.equals("4")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//            u_img4 = BitmapFactory.decodeFile(mPaths.get(0));
//            image4.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img5.equals("5")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//            u_img5 = BitmapFactory.decodeFile(mPaths.get(0));
//            image5.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img6.equals("6")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            img4 = "";
//            img5 = "";
//            img6 = "";
//            u_img6 = BitmapFactory.decodeFile(mPaths.get(0));
//            image6.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK && IMG1.equals("1")) {
                uri1 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                try {
                    u_img1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri1);
                    image1.setImageBitmap(u_img1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_OK && img2.equals("2")) {
                uri2 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                try {
                    u_img2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri2);
                    image2.setImageBitmap(u_img2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_OK && img3.equals("3")) {
                uri3 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                try {
                    u_img3 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri3);
                    image3.setImageBitmap(u_img3);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img4.equals("4")) {
                uri4 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                try {
                    u_img4 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri4);
                    image4.setImageBitmap(u_img4);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img5.equals("5")) {
                uri5 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                try {
                    u_img5 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri5);
                    image5.setImageBitmap(u_img5);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img6.equals("6")) {
                uri6 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                try {
                    u_img6 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri6);
                    image6.setImageBitmap(u_img6);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


//                try {
//                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//
//                    ivImage1.setImageBitmap(selected_img_bitmap);
//
//                    Bitmap immagex = selected_img_bitmap;
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                    byte[] b = baos.toByteArray();
//                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
//
//                    input = imageEncoded;
//                    input = input.replace("\n", "");
//                    input = input.trim();
//                    input = "data:image/png;base64," + input;
//
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }


        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Toast.makeText(this, "Cropping failed: ", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
//        tvSubmissionDate.setText(simpleDateFormat.format(calendar.getTime()));
        tvSubmissionDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

    }

    public void showDate(int year, int monthOfYear, int dayOfMonth, int spinnerTheme) {
        new SpinnerDatePickerDialogBuilder()
                .context(PropertyDetailActivity.this)
                .spinnerTheme(spinnerTheme)
                .defaultDate(year, monthOfYear, dayOfMonth)
                .build()
                .show();
    }

    public void YearsDialog() {
        MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
        pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
//                Toast.makeText(PropertyDetailActivity.this, year + "-" + month, Toast.LENGTH_SHORT).show();
                String construction_year = month + "-" + year;
                tvYearofConstructins.setText(construction_year);
                Utilities.saveString(PropertyDetailActivity.this, "Construction_year", construction_year);
            }
        });
        pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");
    }

    String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

}
