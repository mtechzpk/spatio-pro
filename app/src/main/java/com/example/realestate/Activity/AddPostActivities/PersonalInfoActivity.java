package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class PersonalInfoActivity extends AppCompatActivity {
    Button continue_btn;
    RadioGroup rg_ownership;
    RadioButton rb_builder, rb_agent, rb_owner;
    ImageView go_back;
    private ImageView rent_select, sell_select, builder_select;
    private String selected_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);
        initViews();
        clickViews();
    }

    private void initViews() {
        continue_btn = findViewById(R.id.continue_btn);

        go_back = findViewById(R.id.go_back);

        rent_select = findViewById(R.id.rent_select);
        sell_select = findViewById(R.id.sell_select);
        builder_select = findViewById(R.id.builder_select);

        sell_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell_select.setImageResource(R.drawable.mark_ic);
                rent_select.setImageResource(R.drawable.b);
                builder_select.setImageResource(R.drawable.b);
                Utilities.saveString(PersonalInfoActivity.this, "saller_type", "owner");
                selected_type = "owner";
            }
        });
        rent_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell_select.setImageResource(R.drawable.b);
                rent_select.setImageResource(R.drawable.mark_ic);
                builder_select.setImageResource(R.drawable.b);
                Utilities.saveString(PersonalInfoActivity.this, "saller_type", "agent");

                selected_type = "agent";
            }
        });
        builder_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell_select.setImageResource(R.drawable.b);
                rent_select.setImageResource(R.drawable.b);
                builder_select.setImageResource(R.drawable.mark_ic);
                Utilities.saveString(PersonalInfoActivity.this, "saller_type", "builder");

                selected_type = "builder";
            }
        });
    }

    private void clickViews() {

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected_type.equals("")){
                    Utilities.saveString(PersonalInfoActivity.this,"personal_info" , selected_type);
                    startActivity(new Intent(PersonalInfoActivity.this, PersonalInfo1Activity.class));
                }else {
                    Toast.makeText(PersonalInfoActivity.this, "Select anyone", Toast.LENGTH_SHORT).show();
                }
            }
        });

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
