package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.UiAutomation;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class PremiumPlansActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout property_relative,service_relative;
    private ImageView go_back ;
    Intent intent1,intent2;
    private TextView packg_test_2, packg_test, month_price, months_price;
    private String package_name="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_plans);

        initViews();

        property_relative.setOnClickListener(this);
        service_relative.setOnClickListener(this);
        go_back.setOnClickListener(this);
    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        property_relative = findViewById(R.id.property_relative);
        service_relative = findViewById(R.id.service_relative);
        packg_test_2 = findViewById(R.id.packg_test_2);
        packg_test = findViewById(R.id.packg_test);

        month_price = findViewById(R.id.month_price);
        months_price = findViewById(R.id.months_price);


        if ( Utilities.getString(this,"packge_plan").equals("service")){
            packg_test_2.setText("Post Service");
            packg_test.setText("Post Service");
            month_price.setText("790 Dominican Peso / 15.01 USD");
            months_price.setText("2,370 Dominican Peso / 45.03 USD");
            package_name="1";
        }else {
            packg_test_2.setText("Post Property");
            packg_test.setText("Post Property");
            month_price.setText("295 Dominican Peso / 5.61 USD");
            months_price.setText("885 Dominican Peso / 16.82 USD");
            package_name="2";
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.property_relative:
                intent1 = new Intent(PremiumPlansActivity.this,PaymentMethodActivity.class);
                Utilities.saveString(this,"package_name",package_name);
                startActivity(intent1);
                finish();
                break;
            case R.id.service_relative:
                intent2 = new Intent(PremiumPlansActivity.this,PaymentMethodActivity.class);
                Utilities.saveString(this,"package_name",package_name);
                startActivity(intent2);
                finish();
                break;
            case R.id.go_back:
                PremiumPlansActivity.this.finish();
                break;
        }
    }
}
