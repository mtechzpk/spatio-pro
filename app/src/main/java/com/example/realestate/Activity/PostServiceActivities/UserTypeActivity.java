package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Activity.AddPostActivities.PropertyCategoryActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class UserTypeActivity extends AppCompatActivity {
    private ImageView go_back, service_select, header_select;
    private Button continue_btn;
    private TextView user_name_set;
    private String selected_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        initViews();
        clickViews();
    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);
        service_select = findViewById(R.id.service_select);
        header_select = findViewById(R.id.header_select);
        continue_btn = findViewById(R.id.continue_btn);
        user_name_set= findViewById(R.id.user_name_set);
    }
    private void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!selected_type.equals("")){
                    Toast.makeText(UserTypeActivity.this, selected_type, Toast.LENGTH_SHORT).show();
                    Utilities.saveString(UserTypeActivity.this,"user_type" , selected_type);
                    Intent intent = new Intent(UserTypeActivity.this, ServiceKindActivity.class);
                    startActivity(intent);
                }else {

                    Toast.makeText(UserTypeActivity.this, "Select anyone...!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        user_name_set.setText(Utilities.getString(this,"user_name"));
        service_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service_select.setImageResource(R.drawable.mark_ic);
                header_select.setImageResource(R.drawable.b);
                selected_type = "Service Provider";
            }
        });
        header_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service_select.setImageResource(R.drawable.b);
                header_select.setImageResource(R.drawable.mark_ic);
                selected_type = "Header space advertise";
            }
        });
    }
}
