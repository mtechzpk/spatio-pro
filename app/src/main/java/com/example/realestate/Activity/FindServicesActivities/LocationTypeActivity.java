package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class LocationTypeActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView radio_home, radio_business, radio_industry,
            radio_farm, radio_health, radio_especial, radio_outdoor;
    private TextView btn_previous, btn_next;
    private String location_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_type);
        initViews();
        clickViews();
    }

    private void initViews() {
        radio_home = findViewById(R.id.radio_home);
        radio_business = findViewById(R.id.radio_business);
        radio_industry = findViewById(R.id.radio_industry);
        radio_farm = findViewById(R.id.radio_farm);
        radio_health = findViewById(R.id.radio_health);
        radio_especial = findViewById(R.id.radio_especial);
        radio_outdoor = findViewById(R.id.radio_outdoor);

        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        radio_home.setOnClickListener(this);
        radio_business.setOnClickListener(this);
        radio_industry.setOnClickListener(this);
        radio_farm.setOnClickListener(this);
        radio_health.setOnClickListener(this);
        radio_especial.setOnClickListener(this);
        radio_outdoor.setOnClickListener(this);

        btn_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.radio_home:
                location_type = "Home";
                radio_home.setImageResource(R.drawable.mark_ic);
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);

                break;

            case R.id.radio_business:
                location_type = "Business";
                radio_business.setImageResource(R.drawable.mark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_industry:
                location_type = "Industry";
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.mark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_farm:
                location_type = "Farm";
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.mark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_health:
                location_type = "Health";
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.mark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_especial:
                location_type = "Especial";
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.mark_ic);
                radio_outdoor.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_outdoor:
                location_type = "Outdoor";
                radio_business.setImageResource(R.drawable.unmark_ic);
                radio_home.setImageResource(R.drawable.unmark_ic);
                radio_industry.setImageResource(R.drawable.unmark_ic);
                radio_farm.setImageResource(R.drawable.unmark_ic);
                radio_health.setImageResource(R.drawable.unmark_ic);
                radio_especial.setImageResource(R.drawable.unmark_ic);
                radio_outdoor.setImageResource(R.drawable.mark_ic);
                break;

            case R.id.btn_previous:
                LocationTypeActivity.this.finish();
                break;

            case R.id.btn_next:
                if (location_type.isEmpty()){
                    Toast.makeText(LocationTypeActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(LocationTypeActivity.this,ProjectStatusActivity.class);
                    Utilities.saveString(LocationTypeActivity.this,"location_type",location_type);
                    startActivity(intent);
                    }
                break;
        }
    }
}
