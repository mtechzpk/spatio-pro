package com.example.realestate.Activity.LoginDetailSearchActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.LoginDetail.ResetPasswordActivity;
import com.example.realestate.R;
import com.google.android.material.textfield.TextInputEditText;

public class ResetPasswordSearchActivity extends AppCompatActivity {
    private TextInputEditText email;
    private Button done_btn;
    private String email_st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password_search);
        initViews();

        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email_st = email.getText().toString();
                Toast.makeText(ResetPasswordSearchActivity.this, "Email: "+email_st, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ResetPasswordSearchActivity.this, LoginSearchActivity.class));
            }
        });
    }

    private void initViews() {
        done_btn = findViewById(R.id.done_btn);
        email = findViewById(R.id.email);
    }
}
