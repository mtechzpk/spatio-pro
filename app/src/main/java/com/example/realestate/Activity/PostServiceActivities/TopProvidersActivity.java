package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Adapters.QouteAdapter;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.QouteModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class TopProvidersActivity extends AppCompatActivity {
    private RecyclerView qoute_rv;
    private RecyclerView.Adapter qouteAdapter;
    private ArrayList<QouteModel> qouteModelList;
    String service_option_param = "";
    private ImageView go_back;
    ArrayList<String> servicoption = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_providers);
        Paper.init(TopProvidersActivity.this);
//        if (Utilities.getString(TopProvidersActivity.this, "service_selection").equals("")) {
        if (Utilities.getString(TopProvidersActivity.this, "serviceoption").equals("")) {
            servicoption = Paper.book().read("serviceoption");
            service_option_param = servicoption.toString();
        } else {
//            service_option_param = Utilities.getString(TopProvidersActivity.this, "service_selection");
            service_option_param = Utilities.getString(TopProvidersActivity.this, "serviceoption");
        }

        qoute_rv = findViewById(R.id.qoute_rv);
        go_back = findViewById(R.id.go_back);
        qoute_rv.setHasFixedSize(true);
        qoute_rv.setLayoutManager(new LinearLayoutManager(this));

        qouteModelList = new ArrayList<>();

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findservice();
    }

    public void findservice() {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(TopProvidersActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/search_service", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<String> service_options_list = new ArrayList<>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {

//                        JSONObject obj = object.getJSONObject("data");
                        JSONArray jsonArray = object.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String c_name = jsonObject.getString("personal_info");
                            String provider_name = jsonObject.getString("full_name");
                            String service_type = jsonObject.getString("service_type");
                            String building_no = jsonObject.getString("building_number");
                            String street = jsonObject.getString("streat");
                            String city = jsonObject.getString("city");
                            String user_image = jsonObject.getString("user_image");
                            String image1 = jsonObject.getString("image1");
                            String image2 = jsonObject.getString("image2");
                            String image3 = jsonObject.getString("image3");
                            String image4 = jsonObject.getString("image4");
                            String image5 = jsonObject.getString("image5");
                            String image6 = jsonObject.getString("image6");
                            String register_type = jsonObject.getString("register_type");
                            String person_name = jsonObject.getString("name");
                            String description = jsonObject.getString("description");
                            String certifications = jsonObject.getString("certificate_name");
                            String service_attention = jsonObject.getString("service_attention");
                            String email = jsonObject.getString("email");
                            JSONArray service_options = jsonObject.getJSONArray("service_option");
                            for (int loop = 0; loop < service_options.length(); loop++) {
                                service_options_list.add(service_options.get(loop).toString());
                            }

                            String address = building_no + "," + street;
                            qouteModelList.add(new QouteModel(c_name, service_type, address, register_type, person_name, description, certifications, email, provider_name, user_image, image1,
                                    image2, image3, image4, image5, image6, service_attention, service_options_list));
                        }

                        qouteAdapter = new QouteAdapter(TopProvidersActivity.this, qouteModelList);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TopProvidersActivity.this);
                        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                        qoute_rv.setLayoutManager(linearLayoutManager);
                        qoute_rv.setAdapter(qouteAdapter);
                        qoute_rv.setNestedScrollingEnabled(false);

                        progressdialog.dismiss();

                    } else {
                        progressdialog.dismiss();
                        String message = object.getString("message");
                        Toast.makeText(TopProvidersActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (TopProvidersActivity.this != null)
                    Toast.makeText(TopProvidersActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("service_option", service_option_param);
                params.put("lat", Utilities.getString(TopProvidersActivity.this, "lat"));
                params.put("lng", Utilities.getString(TopProvidersActivity.this, "lng"));

//                params.put("service_option", "administrative procedures");
//                params.put("lng", "74.26835797727108");
//                params.put("lat", "31.443684097178785");
                params.put("do", "search_service");
                params.put("apikey", "mtech");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(TopProvidersActivity.this).addToRequestQueue(RegistrationRequest);


    }

}
