package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.realestate.R;

public class ServicePreviewActivity extends AppCompatActivity {
    private ImageView go_back;
    private Button btn_back, btn_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_preview);
        initViews();
        clickViews();
    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        btn_back = findViewById(R.id.btn_back);
        btn_continue = findViewById(R.id.btn_continue);
    }

    private void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ServicePreviewActivity.this, UniqueCodeActivity.class);
                startActivity(intent);
            }
        });
    }
}
