package com.example.realestate.Activity.LoginDetail;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.AddPostActivities.PropertyDetailActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class SignUpActivity extends AppCompatActivity {

    private TextView login_text, terms_text;
    private Button signup_btn;
    private TextInputEditText password_et, email_et, name_et, phone_et,address_et;
    private ImageView go_back;
    private String email = "", password = "", name = "", phone = "", date_now = "" ,address = "";
    private Spinner spinner_type;
    private String usergroup = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();
        clickViews();
    }

    private void initViews() {
        login_text = findViewById(R.id.login_text);
        signup_btn = findViewById(R.id.signup_btn);
        password_et = findViewById(R.id.password_et);
        address_et = findViewById(R.id.address_et);
        email_et = findViewById(R.id.email_et);
        name_et = findViewById(R.id.name_et);
        phone_et = findViewById(R.id.phone_et);
        terms_text = findViewById(R.id.terms_text);
        go_back = findViewById(R.id.go_back);
        spinner_type = findViewById(R.id.spinner_type);


        String text = "By creating an account you agree to our Terms of Use and Privacy Policy";
        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(SignUpActivity.this, "Terms of Use", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };

        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(SignUpActivity.this, "Privacy Policy", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint textPaint) {
                super.updateDrawState(textPaint);
                textPaint.setColor(Color.BLUE);
                textPaint.setUnderlineText(true);
            }
        };

        ss.setSpan(clickableSpan1, 40, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 57, 71, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        terms_text.setText(ss);
        terms_text.setMovementMethod(LinkMovementMethod.getInstance());
//String ArrayList
        ArrayList<String> sList = new ArrayList<String>();

        sList.add("Buyer");
        sList.add("Seller");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_item, sList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_type.setAdapter(dataAdapter);
        spinner_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                usergroup = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void clickViews() {

        login_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
            }
        });
        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Calendar calendar = Calendar.getInstance();
                int day_of_month = calendar.get(Calendar.DAY_OF_MONTH);
                int month_of_year = calendar.get(Calendar.MONTH);
                int year = calendar.get(Calendar.YEAR);

//        String day =
//                date_now = sdf.format(calendar.getTime());
                date_now = month_of_year + "-" + day_of_month + "-" + year;


                email = email_et.getText().toString().trim();
                password = password_et.getText().toString().trim();
                name = name_et.getText().toString().trim();
                phone = phone_et.getText().toString().trim();
                address = address_et.getText().toString().trim();
                if (!email.isEmpty() && !password.isEmpty() && !name.isEmpty() && !phone.isEmpty()) {

                    SignUpApiCall(email, password, name, phone);
//                Toast.makeText(SignUpActivity.this, "Hit API Here", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SignUpActivity.this, "please fill in all required fields..!", Toast.LENGTH_SHORT).show();
                }

            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void SignUpApiCall(final String email, final String password, final String name, final String phone) {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(SignUpActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/register_user", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status==1) {
                        String msg = object.getString("message");
                        progressdialog.dismiss();
                        Toast.makeText(SignUpActivity.this, msg, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                        finish();
                    } else {
                        progressdialog.dismiss();
                        String message = object.getString("message");
                        Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (SignUpActivity.this != null)
                    Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("name", name);
                params.put("phone", phone);
                params.put("location", address);
                params.put("do", "register");
                params.put("user_group", usergroup);
                params.put("date", date_now);
                params.put("apikey", "travces.com");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(RegistrationRequest);


    }
}
