package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class ProjectAddressActivity extends AppCompatActivity {

    private EditText street, city, phone_no, email;
    private TextView btn_previous, btn_next;
    private String street_ad = "", city_ad = "", phone = "", email_ad = "", address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_address);
        initViews();
        clickViews();
    }

    private void initViews() {
        street = findViewById(R.id.street);
        city = findViewById(R.id.city);
        phone_no = findViewById(R.id.phone_no);
        email = findViewById(R.id.email);
        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectAddressActivity.this.finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                street_ad = street.getText().toString().trim();
                city_ad = city.getText().toString().trim();
                phone = phone_no.getText().toString().trim();
                email_ad = email.getText().toString().trim();

                if (!street_ad.isEmpty()) {
                    if (!city_ad.isEmpty()) {
                        if (!phone.isEmpty()) {
                            if (!email_ad.isEmpty()) {
                                //to Next
                                address = street_ad + " " + city_ad;

                                Utilities.saveString(ProjectAddressActivity.this, "proj_address", address);
                                Utilities.saveString(ProjectAddressActivity.this, "proj_email_address", email_ad);
                                Utilities.saveString(ProjectAddressActivity.this, "proj_city", city_ad);
                                Utilities.saveString(ProjectAddressActivity.this, "proj_phone", phone);
                                Utilities.saveString(ProjectAddressActivity.this, "proj_street", street_ad);
                                Intent intent = new Intent(ProjectAddressActivity.this, NameActivity.class);
                                startActivity(intent);
                            } else {
                                email.setError("Email Required");
                            }
                        } else {
                            phone_no.setError("Phone Number Required");
                        }
                    } else {
                        city.setError("City Required");
                    }
                } else {
                    street.setError("Street Required");
                }
            }
        });
    }
}
