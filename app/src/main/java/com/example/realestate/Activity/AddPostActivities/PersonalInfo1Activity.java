package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.realestate.Activity.PostServiceActivities.ServicePersonalInfoActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

import io.paperdb.Paper;

public class PersonalInfo1Activity extends AppCompatActivity {
    private ImageView go_back;
    private TextInputEditText user_name, user_email;
    private Spinner country_code;
    private EditText phone_number;
    private Button continue_btn;
    private String phone_no, name, email, code="";
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    public static FirebaseAuth auth;
    public static String verificationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info1);
        initViews();
        Paper.init(this);



        ArrayAdapter<CharSequence> dataAdapter = ArrayAdapter.createFromResource(PersonalInfo1Activity.this,
                R.array.country_code, android.R.layout.simple_spinner_item );
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        country_code.setAdapter(dataAdapter);
        country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                code = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        clickViews();
        StartFirebaseLogin();
    }

    private void initViews() {

        go_back = findViewById(R.id.go_back);
        user_name = findViewById(R.id.user_name);
        user_email = findViewById(R.id.user_email);
        country_code = findViewById(R.id.country_code);
        phone_number = findViewById(R.id.phone_number);
        continue_btn = findViewById(R.id.continue_btn);
    }

    private void clickViews() {
        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PersonalInfo1Activity.this,PropertyTypeActivity.class));

                phone_no =code + phone_number.getText().toString();
                name = user_name.getText().toString();
                email = user_email.getText().toString();
                if (!phone_no.isEmpty()) {
                    if (!name.isEmpty()){
                        if (!email.isEmpty()){
                            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                                    phone_no,                     // Phone number to verify
                                    60,                           // Timeout duration
                                    TimeUnit.SECONDS,                // Unit of timeout
                                    PersonalInfo1Activity.this,        // Activity (for callback binding)
                                    mCallback);                      // OnVerificationStateChangedCallbacks

                            Utilities.saveString(PersonalInfo1Activity.this, "user_email", email);
                            Utilities.saveString(PersonalInfo1Activity.this, "user_phone", phone_no);
                            Utilities.saveString(PersonalInfo1Activity.this, "user_name", name);

                            Paper.book().write("payment_name",name);
                            Paper.book().write("payment_email",email);
                            Paper.book().write("payment_phone",phone_no);
                        }else {
                            user_email.setError("Email Required");
                        }
                    }else {
                        user_name.setError("Name Required");
                    }
                }else {
                    phone_number.setError("Phone Number Required");
                }
            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PersonalInfo1Activity.this.finish();
            }
        });
    }


    private void StartFirebaseLogin() {
        auth = FirebaseAuth.getInstance();
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
//                Toast.makeText(PersonalInfo1Activity.this,"verification completed",Toast.LENGTH_SHORT).show();
                totonext();
            }
            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(PersonalInfo1Activity.this,"verification fialed",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                NextScreen();
//                Toast.makeText(PersonalInfo1Activity.this,"Code sent",Toast.LENGTH_SHORT).show();
            }
        };
    }

    public void NextScreen(){
        startActivity(new Intent(PersonalInfo1Activity.this, EnterOtpActivity.class));
    }
    public void  totonext(){
        startActivity(new Intent(PersonalInfo1Activity.this,PropertyTypeActivity.class));
    }
}
