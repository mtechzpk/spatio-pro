package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Adapters.PropertyTypeAdapter;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PropertyStatusActivity extends AppCompatActivity implements PropertyTypeAdapter.Callback {
    private RecyclerView recyclerView;
    private ImageView go_back;
private PropertyTypeAdapter pAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_status);
        go_back = findViewById(R.id.go_back);
        recyclerView = findViewById(R.id.recycler_property_type);
        getChannels();

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertyStatusActivity.this.finish();
            }
        });
    }

    private void getChannels() {


        ArrayList<PropertyTypeModel> propertyTypeModels = new ArrayList<PropertyTypeModel>();

        propertyTypeModels.add(new PropertyTypeModel(1, "New"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Second Use"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Verifid"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Blue Print"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Ready to Move"));
        propertyTypeModels.add(new PropertyTypeModel(1, "In Offer"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Verified"));
        propertyTypeModels.add(new PropertyTypeModel(1, "Coming Soon"));

        pAdapter = new PropertyTypeAdapter(propertyTypeModels, getApplicationContext(), PropertyStatusActivity.this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pAdapter);


    }

    @Override
    public void onItemClick(int pos, int id, String type_name) {
        Intent intent = new Intent(PropertyStatusActivity.this, FinalPostActivity.class);
        intent.putExtra("property_status", type_name);

        startActivity(intent);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (pAdapter!=null){
            pAdapter.notifyDataSetChanged();
        }
    }
}
