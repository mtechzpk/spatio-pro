package com.example.realestate.Activity.AddPostActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.PostServiceActivities.PremiumPlansActivity;
import com.example.realestate.Activity.PostServiceActivities.UniqueCodeActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.dialogs.HelpDialog;
import com.example.realestate.dialogs.PaymentAssistanceDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.paperdb.Paper;

public class EnterCodeActivity extends AppCompatActivity {
    private Button btn_post, btn_buy_code,btn_save;
    String date;
    private ImageView go_back;
    EditText et1, et2, et3, et4;
    ArrayList<String> amenitieslist = new ArrayList<>();
    TextView tvPaymentAssistance, tvContactUs;
    String getText,neighbourhood,submission_date,price_unit;

    private String filename = "SampleFile.txt";
    private String filepath = "MyFileStorage";
    File myExternalFile;
    String myData = "",prop_fect,img5,img6,Construction_year;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);
        Paper.init(EnterCodeActivity.this);
        initViews();
        ClickViews();
        neighbourhood=Utilities.getString(EnterCodeActivity.this,"neighbourhood");
        submission_date=Utilities.getString(EnterCodeActivity.this,"submission_date");
        prop_fect=Utilities.getString(EnterCodeActivity.this,"prop_fect");
        img5=Utilities.getString(EnterCodeActivity.this,"img5");
        img6=Utilities.getString(EnterCodeActivity.this,"img6");
        price_unit=Utilities.getString(EnterCodeActivity.this,"price_unit");
        Construction_year=Utilities.getString(EnterCodeActivity.this,"Construction_year");

        date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        amenitieslist = Paper.book().read("attentionList");


        et1.addTextChangedListener(new EnterCodeActivity.GenericTextWatcher(et1));
        et2.addTextChangedListener(new EnterCodeActivity.GenericTextWatcher(et2));
        et3.addTextChangedListener(new EnterCodeActivity.GenericTextWatcher(et3));
        et4.addTextChangedListener(new EnterCodeActivity.GenericTextWatcher(et4));

    }

    private void initViews() {
        go_back = findViewById(R.id.go_back);

        tvPaymentAssistance = findViewById(R.id.tvPaymentAssistance);
        tvContactUs = findViewById(R.id.tvContactUS);
        btn_save = findViewById(R.id.btn_save);
        btn_buy_code = findViewById(R.id.btn_buy_code);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);
        btn_post = findViewById(R.id.btn_post);
        getText = et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString();

    }

    private void ClickViews() {
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream fos = new FileOutputStream(myExternalFile);
                    fos.write(getText.getBytes());
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                et1.setText("");
//                response.setText("SampleFile.txt saved to External Storage...");

                Toast.makeText(EnterCodeActivity.this, getText + "saved to External Storage", Toast.LENGTH_SHORT).show();
            }
        });

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (et1.getText().toString().equals("") &&
                        et2.getText().toString().equals("") &&
                        et3.getText().toString().equals("") &&
                        et4.getText().toString().equals("")) {
                    Toast.makeText(EnterCodeActivity.this, "Please Enter Code..!", Toast.LENGTH_SHORT).show();
                } else {
                    postproperty();
                }

            }
        });
        btn_buy_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnterCodeActivity.this, PremiumPlansActivity.class);
                startActivity(intent);

            }
        });
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet(v);

            }
        });
        tvPaymentAssistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet1(v);
            }
        });
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void postproperty() {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(EnterCodeActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/add_property", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
//                        Paper.book().destroy();
                        Paper.book().delete("serviceoption");
                        Paper.book().delete("attentionList");
//                        Utilities.clearSharedPref(EnterCodeActivity.this);
                        String msg = object.getString("data");
                        Toast.makeText(EnterCodeActivity.this, msg, Toast.LENGTH_SHORT).show();
                        progressdialog.dismiss();
                        Intent intent = new Intent(EnterCodeActivity.this, RealEstateActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        startActivity(intent);

                    } else {
                        progressdialog.dismiss();
                        String message = object.getString("message");
                        Toast.makeText(EnterCodeActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (EnterCodeActivity.this != null)
                    Toast.makeText(EnterCodeActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                int u_id = Paper.book().read("user_id");
                params.put("user_id", String.valueOf(Utilities.getInt(EnterCodeActivity.this,"user_id")));
                params.put("ad_title", Utilities.getString(EnterCodeActivity.this, "area_title"));
                params.put("property_category", Utilities.getString(EnterCodeActivity.this, "property_type"));
                params.put("property_type", Utilities.getString(EnterCodeActivity.this, "cat_name"));
                params.put("description", Utilities.getString(EnterCodeActivity.this, "area_description"));
                params.put("offer_price", Utilities.getString(EnterCodeActivity.this, "area_price"));
                params.put("land_area", Utilities.getString(EnterCodeActivity.this, "area"));
                params.put("construction_area", Utilities.getString(EnterCodeActivity.this, "construction_area"));
                params.put("area_unit", Utilities.getString(EnterCodeActivity.this, "areaunit"));
                params.put("post_unit", Utilities.getString(EnterCodeActivity.this, "post_unit"));
                params.put("price_unit",price_unit);
                params.put("address", Utilities.getString(EnterCodeActivity.this, "address"));
                params.put("coordinates_tag", Utilities.getString(EnterCodeActivity.this, "lat") + "," + Utilities.getString(EnterCodeActivity.this, "lng"));
                params.put("bed", Utilities.getString(EnterCodeActivity.this, "bedrooms"));
                params.put("bath", Utilities.getString(EnterCodeActivity.this, "bathrooms"));
                params.put("floor", Utilities.getString(EnterCodeActivity.this, "floor_level"));
                params.put("full_name", Utilities.getString(EnterCodeActivity.this, "user_name"));
                params.put("phone", Utilities.getString(EnterCodeActivity.this, "user_phone"));
                params.put("email", Utilities.getString(EnterCodeActivity.this, "user_email"));
                params.put("amenities", amenitieslist.toString());
                params.put("property_code", et1.getText().toString() + et2.getText().toString() +
                        et3.getText().toString() + et4.getText().toString());
                params.put("image1", "data:image/jpeg;base64," + Utilities.getString(EnterCodeActivity.this, "img1"));
                params.put("image2", "data:image/jpeg;base64," + Utilities.getString(EnterCodeActivity.this, "img2"));
                params.put("image3", "data:image/jpeg;base64," + Utilities.getString(EnterCodeActivity.this, "img3"));
                params.put("image4", "data:image/jpeg;base64," + Utilities.getString(EnterCodeActivity.this, "img4"));
                params.put("image5", "data:image/jpeg;base64," + img5);
                params.put("image6", "data:image/jpeg;base64," + img6);
//                params.put("submission_date", date);
                params.put("status", "Active");

                params.put("naighberhood", neighbourhood);
                params.put("submisstion_date", submission_date);
                params.put("property_feature", prop_fect);
                params.put("year_of_construction", Construction_year);

                params.put("building_no", Utilities.getString(EnterCodeActivity.this, "pro_building"));
                params.put("streat", Utilities.getString(EnterCodeActivity.this, "pro_street"));
                params.put("city", Utilities.getString(EnterCodeActivity.this, "pro_city"));
                params.put("province", Utilities.getString(EnterCodeActivity.this, "pro_province"));
                params.put("country", Utilities.getString(EnterCodeActivity.this, "pro_country"));
                params.put("do", "add property");
                params.put("apikey", "travces.com");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getInstance(EnterCodeActivity.this).addToRequestQueue(RegistrationRequest);

    }

    private class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            switch (view.getId()) {
                case R.id.et1:
                    if (text.length() == 1)
                        et2.requestFocus();
                    break;
                case R.id.et2:
                    if (text.length() == 1)
                        et3.requestFocus();
                    else if (text.length() == 0)
                        et1.requestFocus();
                    break;
                case R.id.et3:
                    if (text.length() == 1)
                        et4.requestFocus();
                    else if (text.length() == 0)
                        et2.requestFocus();
                    break;
                case R.id.et4:
                    if (text.length() == 0)
                        et3.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    private static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }
    public void showBottomSheet(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HelpDialog userPopUp = new HelpDialog();
        userPopUp.show(fragmentManager, "sms");
    }

    public void showBottomSheet1(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        PaymentAssistanceDialog userPopUp = new PaymentAssistanceDialog();
        userPopUp.show(fragmentManager, "sms");
    }

}
