package com.example.realestate.Activity.PostServiceActivities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.PropertyDetailActivity;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Adapters.ServiceAttentionAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.Models.ServiceAttentionModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.google.gson.JsonArray;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import net.alhazmy13.mediapicker.Image.ImagePicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceDetailActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner registration_type_sp;
    private EditText registeration_no, service_name, service_description, certification;
    private ImageView company_logo, image1, image2, image3, image4, image5, image6,certification_img, go_back;
    private RecyclerView rv_service_attention;
    private Button btn_post;
    private ArrayList<ServiceAttentionModel> serviceAttentionModels;
    private ServiceAttentionAdapter serviceAttentionAdapter;
    private String register_type, register_no, ser_name, service_desc, certificate;
    String IMG1 = "", img2 = "", img3 = "", prof = "", img4 = "", img5 = "", img6 = "",certificate_img="";
    private Bitmap u_img1 = null, u_img2 = null, u_img3 = null, u_img4 = null, u_img5 = null, u_img6 = null, u_imgp = null,u_certificate_img=null;
    Uri uri1 = null, uri2 = null, uri3 = null, uri4 = null, uri5 = null, uri6 = null,logoUri=null,cerficateUri=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_detail);

        initviews();
        clickViews();

        initAdapter();

        ArrayAdapter<CharSequence> adapters = ArrayAdapter.createFromResource(this,
                R.array.register_type, android.R.layout.simple_spinner_item);
        adapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        registration_type_sp.setAdapter(adapters);
        registration_type_sp.setOnItemSelectedListener(this);
    }

    private void clickViews() {


        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register_type = registration_type_sp.getSelectedItem().toString().trim();
                register_no = registeration_no.getText().toString().trim();
                ser_name = service_name.getText().toString().trim();
                service_desc = service_description.getText().toString().trim();
                certificate = certification.getText().toString().trim();


                if (!register_type.equals("Type of Register")) {
                    if (!register_no.isEmpty()) {
                        if (!ser_name.isEmpty()) {
                            if (!service_desc.isEmpty()) {
                                if (!certificate.isEmpty()) {
                                    if (u_img1 != null && u_imgp != null) {
                                        if (u_imgp != null) {
                                            Bitmap immagex = u_imgp;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "imgp", input);
                                        }
                                        if (u_img1 != null) {
                                            Bitmap immagex = u_img1;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img1", input);
                                        }

                                        if (u_img2 != null) {
                                            Bitmap immagex = u_img2;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img2", input);
                                        }
                                        if (u_img3 != null) {
                                            Bitmap immagex = u_img3;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img3", input);
                                        }
                                        if (u_img4 != null) {
                                            Bitmap immagex = u_img4;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img4", input);
                                        }
                                        if (u_img5 != null) {
                                            Bitmap immagex = u_img5;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img5", input);
                                        }  if (u_img6 != null) {
                                            Bitmap immagex = u_img6;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "img6", input);
                                        }
                                        if (u_certificate_img != null) {
                                            Bitmap immagex = u_certificate_img;
                                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                            immagex.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
                                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                                            String imageEncoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
                                            String input = imageEncoded;
                                            input = input.replace("\n", "");
                                            input = input.trim();
                                            Utilities.saveString(ServiceDetailActivity.this, "certificate_img", input);
                                        }
                                        Utilities.saveString(ServiceDetailActivity.this, "register_type", register_type);
                                        Utilities.saveString(ServiceDetailActivity.this, "register_no", register_no);
                                        Utilities.saveString(ServiceDetailActivity.this, "ser_name", ser_name);
                                        Utilities.saveString(ServiceDetailActivity.this, "service_desc", service_desc);
                                        Utilities.saveString(ServiceDetailActivity.this, "certificate", certificate);
                                        Intent intent = new Intent(ServiceDetailActivity.this, UniqueCodeActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Toast.makeText(ServiceDetailActivity.this, "Images Required..!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    certification.setError("Certification Required");
                                }
                            } else {
                                service_description.setError("Service Description Required");
                            }
                        } else {
                            service_name.setError("Service Name Required");
                        }
                    } else {
                        registeration_no.setError("Registration No Required");
                    }
                } else {
                    registration_type_sp.setPrompt("Registration type Required");
                    Toast.makeText(ServiceDetailActivity.this, "Registration type Required", Toast.LENGTH_SHORT).show();
                }
            }
        });

        company_logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                prof = "p";

            }
        });
        image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                IMG1 = "1";

            }
        });
        image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                img2 = "2";
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                img3 = "3";
            }
        });
        image4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);

                img4 = "4";

            }
        });
        image5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                img5 = "5";
            }
        });
        image6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                img6 = "6";
            }
        });
        certification_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropMenuCropButtonTitle("Done")
                        .setCropShape(CropImageView.CropShape.RECTANGLE)
                        .start(ServiceDetailActivity.this);
                certificate_img = "c_img";
            }
        });

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initviews() {
        go_back = findViewById(R.id.go_back);

        registration_type_sp = findViewById(R.id.registration_type_sp);
        registeration_no = findViewById(R.id.registeration_no);
        service_name = findViewById(R.id.service_name);
        service_description = findViewById(R.id.service_description);
        certification = findViewById(R.id.certification);

        company_logo = findViewById(R.id.company_logo);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        image6 = findViewById(R.id.image6);
        certification_img = findViewById(R.id.certification_img);

        rv_service_attention = findViewById(R.id.rv_service_attention);
        btn_post = findViewById(R.id.btn_post);
    }


    private void initAdapter() {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(ServiceDetailActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/service_attenion",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject object = new JSONObject(response);
                            String status = object.getString("Success");
                            if (status.equals("1")) {
                                serviceAttentionModels = new ArrayList<>();

                                JSONArray objUser = object.getJSONArray("data");
                                for (int i = 0; i < objUser.length(); i++) {
                                    JSONObject jsonObject = objUser.getJSONObject(i);
                                    int id = jsonObject.getInt("id");
                                    String name = jsonObject.getString("service_attention");
//                            propertyTypeModels.add(new PropertyTypeModel(id,name));
                                    serviceAttentionModels.add(new ServiceAttentionModel(name, id));

                                }
                                progressdialog.dismiss();
                                serviceAttentionAdapter = new ServiceAttentionAdapter(ServiceDetailActivity.this, serviceAttentionModels);
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ServiceDetailActivity.this);
                                linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
                                rv_service_attention.setLayoutManager(linearLayoutManager);
                                rv_service_attention.setAdapter(serviceAttentionAdapter);
                            } else {
                                progressdialog.dismiss();
//                                String message = object.getString("message");
                                Toast.makeText(ServiceDetailActivity.this,"result not fount", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressdialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (ServiceDetailActivity.this != null)
                    Toast.makeText(ServiceDetailActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "get_services_attention");
                params.put("apikey", "mtech");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(ServiceDetailActivity.this).addToRequestQueue(RegistrationRequest);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = (String) parent.getItemAtPosition(position);
        Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK && IMG1.equals("1")) {
                uri1 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri1);
                    image1.setImageBitmap(u_img1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_OK && img2.equals("2")) {
                uri2 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img2 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri2);
                    image2.setImageBitmap(u_img2);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_OK && img3.equals("3")) {
                uri3 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img3 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri3);
                    image3.setImageBitmap(u_img3);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img4.equals("4")) {
                uri4 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img4 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri4);
                    image4.setImageBitmap(u_img4);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img5.equals("5")) {
                uri5 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img5 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri5);
                    image5.setImageBitmap(u_img5);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && img6.equals("6")) {
                uri6 = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_img6 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri6);
                    image6.setImageBitmap(u_img6);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (resultCode == RESULT_OK && prof.equals("p")) {
                logoUri = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_imgp = MediaStore.Images.Media.getBitmap(this.getContentResolver(), logoUri);
                    company_logo.setImageBitmap(u_imgp);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (resultCode == RESULT_OK && certificate_img.equals("c_img")) {
                cerficateUri = result.getUri();
                IMG1 = "";
                img2 = "";
                img3 = "";
                img4 = "";
                img5 = "";
                img6 = "";
                prof = "";
                certificate_img="";
                try {
                    u_certificate_img = MediaStore.Images.Media.getBitmap(this.getContentResolver(), cerficateUri);
                    certification_img.setImageBitmap(u_certificate_img);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


//                try {
//                    selected_img_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
//
//                    ivImage1.setImageBitmap(selected_img_bitmap);
//
//                    Bitmap immagex = selected_img_bitmap;
//                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                    immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//                    byte[] b = baos.toByteArray();
//                    imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
//
//                    input = imageEncoded;
//                    input = input.replace("\n", "");
//                    input = input.trim();
//                    input = "data:image/png;base64," + input;
//
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }


        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            Toast.makeText(this, "Cropping failed: ", Toast.LENGTH_LONG).show();
        }

    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && prof.equals("p")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//
//            u_imgp = BitmapFactory.decodeFile(mPaths.get(0));
//            company_logo.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && IMG1.equals("1")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_img1 = BitmapFactory.decodeFile(mPaths.get(0));
//            image1.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img2.equals("2")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_img2 = BitmapFactory.decodeFile(mPaths.get(0));
//            image2.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img3.equals("3")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_img3 = BitmapFactory.decodeFile(mPaths.get(0));
//            image3.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img4.equals("4")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_img4 = BitmapFactory.decodeFile(mPaths.get(0));
//            image4.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img5.equals("5")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_img5 = BitmapFactory.decodeFile(mPaths.get(0));
//            image5.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        } if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && img5.equals("6")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            img6 = "";
//            certificate_img="";
//
//            u_img6 = BitmapFactory.decodeFile(mPaths.get(0));
//            image6.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK && certificate_img.equals("c_img")) {
//            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
//            IMG1 = "";
//            img2 = "";
//            img3 = "";
//            prof = "";
//            img4 = "";
//            img5 = "";
//            certificate_img="";
//
//            u_certificate_img = BitmapFactory.decodeFile(mPaths.get(0));
////            image5.setImageBitmap(BitmapFactory.decodeFile(mPaths.get(0)));
//
//        }
//    }


}
