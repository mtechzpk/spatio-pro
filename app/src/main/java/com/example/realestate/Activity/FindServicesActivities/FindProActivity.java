package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.PostServiceActivities.ServiceTypeActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.CityEntity;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.yokeyword.indexablerv.EntityWrapper;
import me.yokeyword.indexablerv.IndexableAdapter;
import okhttp3.internal.Util;

public class FindProActivity extends AppCompatActivity {
    private EditText top_rated;
    private RelativeLayout find_pros_rel, find_pros_all;
    private String text = "";
    private ImageView go_back;
    public static List<CityEntity> list = new ArrayList<>();
    List<String> forsearch = new ArrayList<>();
    private String selection="";
    private AutoCompleteTextView autoCompleteTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_pro);
        initViews();
        clickViews();
    }

    private void initViews() {
//        top_rated = findViewById(R.id.top_rated);
        go_back = findViewById(R.id.go_back);
        find_pros_rel = findViewById(R.id.find_pros_rel);
        find_pros_all = findViewById(R.id.find_pros_all);
        final ProgressDialog progressdialog = new ProgressDialog(this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/service_option", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status == 1) {
                        progressdialog.dismiss();
                        JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("service_option");
                            list.add(new CityEntity(id, name, "abc"));
                            forsearch.add(name);

                        }


                    } else {

                        Toast.makeText(FindProActivity.this, "No Category Avilable..!", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (this != null)
                    Toast.makeText(FindProActivity.this, message, Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "get_service_option");
                params.put("apikey", "mtech");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(this).addToRequestQueue(RegistrationRequest);

    }

    private void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FindProActivity.this.finish();
            }
        });

        find_pros_rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // search here
//                text = top_rated.getText().toString().trim();
                if (!selection.equals("")) {
                    Utilities.saveString(FindProActivity.this, "service_selection", selection);
                    Intent intent = new Intent(FindProActivity.this, GetStartedActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(FindProActivity.this, "Enter Options..!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        find_pros_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FindProActivity.this, AllCategoryNewActivity.class);
                startActivity(intent);
            }
        });
        autoCompleteTextView = findViewById(R.id.textview);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, forsearch);
        autoCompleteTextView.setAdapter(arrayAdapter);

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection = (String) parent.getItemAtPosition(position);
            }
        });
    }

}
