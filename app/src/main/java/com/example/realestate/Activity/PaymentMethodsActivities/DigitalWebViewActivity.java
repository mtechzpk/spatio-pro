package com.example.realestate.Activity.PaymentMethodsActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

import com.example.realestate.R;


public class DigitalWebViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_web_view);
        WebView myWebView = findViewById(R.id.webview);
        myWebView.loadUrl("https://bit.ly/2T1SPdb");
    }
}