package com.example.realestate.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.realestate.Adapters.SlidingImage_Adapter;
import com.example.realestate.Fragments.HomeFragment;
import com.example.realestate.Fragments.MoreFragment;
import com.example.realestate.Fragments.MySpatioFragment;
import com.example.realestate.Fragments.NewsFragment;
import com.example.realestate.Fragments.SearchFragment;
import com.example.realestate.Models.HomeImageModel;
import com.example.realestate.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class LandingActivity extends AppCompatActivity {


    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    private ArrayList<HomeImageModel> homeImageModels;
    private int[] myImageList = new int[]{R.drawable.slider_1, R.drawable.slider_2,
            R.drawable.slider_4,R.drawable.slider_5, R.drawable.slider_6,
            R.drawable.slider_7,R.drawable.slider_8};

    Timer timer;

    SearchFragment searchFragment = new SearchFragment();
    MySpatioFragment mySpatioFragment = new MySpatioFragment();
    HomeFragment homeFragment = new HomeFragment();
    NewsFragment newsFragment = new NewsFragment();
    MoreFragment moreFragment = new MoreFragment();

    boolean doubleBackToExitPressedOnce = false;
    boolean openedFragment = false;

    BottomNavigationView bottomNavigationView;
    //Bottom Navigation item selected listener
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.nav_search:
                            openFragment(searchFragment);
                            return true;
                        case R.id.nav_logo:
                            openFragment(mySpatioFragment);
                            openedFragment = true;
                            return true;
                        case R.id.nav_home:
                            openFragment(homeFragment);
                            openedFragment = true;
                            return true;
                        case R.id.nav_news:
                            openFragment(newsFragment);
                            openedFragment = true;
                            return true;
                        case R.id.nav_more:
                            openFragment(moreFragment);
                            openedFragment = true;
                            return true;
                    }
                    return false;
                }
            };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        //initializing variables and implementing image slider below
        initViews();

        homeImageModels = new ArrayList<>();
        homeImageModels = populateList();

        mPager.setAdapter(new SlidingImage_Adapter(LandingActivity.this, homeImageModels));
        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = homeImageModels.size();


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                if (position == myImageList.length - 1) {

                } else {

                }

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 7000, 7000);




        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(searchFragment);

    }

    //initializing Views
    private void initViews() {
        mPager = findViewById(R.id.viewpager);
        bottomNavigationView = findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(searchFragment);
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //image Slider and roundShapes with image
//    private void imageSlider() {
//        homeImageModels = new ArrayList<>();
//        homeImageModels = populateList();
//
//        mPager.setAdapter(new SlidingImage_Adapter(LandingActivity.this, homeImageModels));
//        CirclePageIndicator indicator = (CirclePageIndicator)
//                findViewById(R.id.indicator);
//
//        indicator.setViewPager(mPager);
//        final float density = getResources().getDisplayMetrics().density;
//
//        //Set circle indicator radius
//        indicator.setRadius(5 * density);
//        NUM_PAGES = homeImageModels.size();
//
//        // Pager listener over indicator
//        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int position) {
//                currentPage = position;
//                if (position == myImageList.length - 1) {
//
//                } else {
//
//                }
//
//            }
//
//            @Override
//            public void onPageScrolled(int pos, float arg1, int arg2) {
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int pos) {
//
//            }
//        });
//
//        timer = new Timer();
//        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 2000);
//    }

    private ArrayList<HomeImageModel> populateList() {

        ArrayList<HomeImageModel> list = new ArrayList<>();

        for (int i = 0; i < myImageList.length; i++) {
            HomeImageModel homeImageModel = new HomeImageModel();
            homeImageModel.setImage_drawable(myImageList[i]);
            list.add(homeImageModel);

        }

        return list;
    }


//    @Override
//    public void onDestroyView() {
//
//        super.onDestroyView();
//        if (timer != null) {
//            timer.cancel();
//
//        }
//    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
    public class MyTimerTask extends TimerTask {
        @Override
        public void run() {

            if (LandingActivity.this == null)
                return;

            LandingActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (currentPage == NUM_PAGES - 1) {
                        currentPage = 0;
                    } else {
                        currentPage++;
                    }
                    mPager.setCurrentItem(currentPage, true);

                }
            });
        }
    }


}
