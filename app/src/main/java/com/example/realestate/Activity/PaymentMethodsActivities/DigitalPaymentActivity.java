package com.example.realestate.Activity.PaymentMethodsActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.realestate.R;
import com.example.realestate.dialogs.HelpDialog;

public class DigitalPaymentActivity extends FragmentActivity {
    LinearLayout linear_assistant;
    private ImageView go_back;
    TextView tvCreditCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_payment);

        go_back = findViewById(R.id.go_back);
        linear_assistant = findViewById(R.id.linear_assistant);
        tvCreditCard = findViewById(R.id.tvCreditCard);

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        linear_assistant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet(v);
            }
        });
        tvCreditCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DigitalPaymentActivity.this, DigitalWebViewActivity.class));
            }
        });

    }

    public void showBottomSheet(View view) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        HelpDialog userPopUp = new HelpDialog();
        userPopUp.show(fragmentManager, "sms");
    }
}
