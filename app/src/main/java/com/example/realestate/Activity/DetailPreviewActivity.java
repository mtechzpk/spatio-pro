package com.example.realestate.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.PaymentMethodsActivities.PaypalActivity;
import com.example.realestate.Activity.PostServiceActivities.ServiceDetailActivity;
import com.example.realestate.Adapters.RecyclerViewAdapter;
import com.example.realestate.Adapters.ServiceAttentionAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.NearbyData;
import com.example.realestate.Models.ServiceAttentionModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class DetailPreviewActivity extends AppCompatActivity {
    private RecyclerView.Adapter recyclerView_Adapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    private TextView prop_categ1,price_text, propertId,bed_text, address_text, covered_area_text, price_per_sqft_text, carpet_area_text, location_text,
            config_text, status_text, floor_text;
    String[] numbers = {"facility of water", "parking space", "Furnished",};
    private RecyclerView recyclerView;
    private Context context;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private NearbyData object;
//    private ImageView img_main;
    private TextInputEditText contact_name, contact_email, contact_phone;
    private EditText contact_description;
    private Button contact_btn;
    private String nam = "", mail = "", cntct = "", query = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_preview);

        Paper.init(this);
        object = Paper.book().read("object");

        initViews();
        ToolbarMethod();
        ClickViews();

    }

    private void initViews() {
        context = getApplicationContext();
//        toolbar = findViewById(R.id.toolBar);
//        recyclerView = findViewById(R.id.rv_aminities);
//        collapsingToolbar = findViewById(R.id.collapsing_toolbar);

        price_text = findViewById(R.id.price_text);
        bed_text = findViewById(R.id.bed_text);
        address_text = findViewById(R.id.address_text);
        covered_area_text = findViewById(R.id.covered_area_text);
        prop_categ1 = findViewById(R.id.prop_categ1);
//        price_per_sqft_text = findViewById(R.id.price_per_sqft_text);
//        carpet_area_text = findViewById(R.id.carpet_area_text);
        location_text = findViewById(R.id.location_text);
        propertId = findViewById(R.id.propertId);
        config_text = findViewById(R.id.config_text);
        status_text = findViewById(R.id.status_text);
        floor_text = findViewById(R.id.floor_text);
//        img_main = findViewById(R.id.img_main);

        contact_name = findViewById(R.id.contact_name);
        contact_email = findViewById(R.id.contact_email);
        contact_phone = findViewById(R.id.contact_phone);
        contact_description = findViewById(R.id.contact_description);

        contact_btn = findViewById(R.id.contact_btn);
    }

    private void ToolbarMethod() {

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationIcon(R.drawable.arrow_back);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

        //Change 2 to your choice because here 2 is the number of Grid layout Columns in each row.
//        recyclerViewLayoutManager = new GridLayoutManager(context, 2);
//        recyclerView.setLayoutManager(recyclerViewLayoutManager);
//        recyclerView_Adapter = new RecyclerViewAdapter(context, object.getAmenitieslist());
//        recyclerView.setAdapter(recyclerView_Adapter);
////        recyclerView.setNestedScrollingEnabled(false);collapsingToolbar.setTitle("");

        price_text.setText(object.getPrize());
        address_text.setText(object.getAddress());
        covered_area_text.setText(object.getArea() + " " + object.getArea_unit());
        location_text.setText(object.getAddress());
        config_text.setText(object.getBed() + getString(R.string.beds) + " " + object.getBath() + " " + getString(R.string.bath) + " ");
        floor_text.setText(object.getFloor() + " " + getString(R.string.floor_text));
        propertId.setText("sp-"+object.getPost_unit()+"-"+object.getId());
        prop_categ1.setText("On "+object.getProperty_category());
//        Picasso.get()
//                .load(object.getProperty_pictures())
//                .fit()
//                .centerInside()
//                .placeholder(R.drawable.bgg)
//                .into(img_main);

    }

    private void ClickViews() {
        contact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nam = contact_name.getText().toString().trim();
                mail = contact_email.getText().toString().trim();
                cntct = contact_phone.getText().toString().trim();
                query = contact_description.getText().toString().trim();

                if (!nam.isEmpty()) {
                    if (!mail.isEmpty()) {
                        if (!cntct.isEmpty()) {
                            if (!query.isEmpty()) {
                                ContactApiCall();
                            } else {
                                contact_description.setError("Write some description please.");
                            }
                        } else {
                            contact_email.setError("Phone Required");
                        }
                    } else {
                        contact_email.setError("Email Required");
                    }
                } else {
                    contact_name.setError("Name Required");
                }

            }
        });
    }


    private void ContactApiCall() {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(DetailPreviewActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/sendemail/send",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject object = new JSONObject(response);
                            int status = object.getInt("success");
                            if (status == 1) {
                                String Message = object.getString("message");
                                Utilities.hideProgressDialog();
                                Toast.makeText(DetailPreviewActivity.this, Message, Toast.LENGTH_SHORT).show();

                            } else {
                                Utilities.hideProgressDialog();
                                String message = object.getString("message");
                                Toast.makeText(DetailPreviewActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressdialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (DetailPreviewActivity.this != null)
                    Toast.makeText(DetailPreviewActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nam);
                params.put("provider_email", "hamza.mughal5593@gmail.com");
                params.put("phone", cntct);
                params.put("user_email", mail);
                params.put("message", query);
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(DetailPreviewActivity.this).addToRequestQueue(RegistrationRequest);

    }
}
