package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Activity.AddPostActivities.PropertyDetailActivity;
import com.example.realestate.Activity.FindServicesActivities.ProjectLocationActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class ProjectDescriptionActivity extends AppCompatActivity {
    private EditText service_description;
    private TextView btn_previous, btn_next;
    private String description = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_description);

        service_description = findViewById(R.id.service_description);
        btn_next = findViewById(R.id.btn_next);
        btn_previous = findViewById(R.id.btn_previous);

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProjectDescriptionActivity.this.finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // go to next

                description = service_description.getText().toString().trim();
                if (!description.isEmpty()) {
                    Utilities.saveString(ProjectDescriptionActivity.this,"pro_discr",description);
                    Intent intent = new Intent(ProjectDescriptionActivity.this, ProjectAddressActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ProjectDescriptionActivity.this, "Please Enter Text", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
