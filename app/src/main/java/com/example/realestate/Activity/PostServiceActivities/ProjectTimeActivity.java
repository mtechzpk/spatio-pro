package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class ProjectTimeActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView radio_flexible, radio_week, radio_2weeks, radio_3weeks;
    private TextView btn_previous, btn_next;
    private String project_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_time);
        initViews();
        clickViews();
    }

    private void initViews() {
        radio_flexible = findViewById(R.id.radio_flexible);
        radio_week = findViewById(R.id.radio_week);
        radio_2weeks = findViewById(R.id.radio_2weeks);
        radio_3weeks = findViewById(R.id.radio_3weeks);

        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);
    }

    private void clickViews() {
        radio_flexible.setOnClickListener(this);
        radio_week.setOnClickListener(this);
        radio_2weeks.setOnClickListener(this);
        radio_3weeks.setOnClickListener(this);

        btn_previous.setOnClickListener(this);
        btn_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radio_flexible:
                project_time = "flexible";
                radio_week.setImageResource(R.drawable.unmark_ic);
                radio_2weeks.setImageResource(R.drawable.unmark_ic);
                radio_3weeks.setImageResource(R.drawable.unmark_ic);
                radio_flexible.setImageResource(R.drawable.mark_ic);
                break;

            case R.id.radio_week:
                project_time = "1week";
                radio_week.setImageResource(R.drawable.mark_ic);
                radio_2weeks.setImageResource(R.drawable.unmark_ic);
                radio_3weeks.setImageResource(R.drawable.unmark_ic);
                radio_flexible.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_2weeks:
                project_time = "2weeks";
                radio_week.setImageResource(R.drawable.unmark_ic);
                radio_2weeks.setImageResource(R.drawable.mark_ic);
                radio_3weeks.setImageResource(R.drawable.unmark_ic);
                radio_flexible.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.radio_3weeks:
                project_time = "3weeks";
                radio_week.setImageResource(R.drawable.unmark_ic);
                radio_2weeks.setImageResource(R.drawable.unmark_ic);
                radio_3weeks.setImageResource(R.drawable.mark_ic);
                radio_flexible.setImageResource(R.drawable.unmark_ic);
                break;

            case R.id.btn_previous:
                ProjectTimeActivity.this.finish();
                break;

            case R.id.btn_next:
                if (project_time.isEmpty() || project_time.equals(" ")) {
                    Toast.makeText(ProjectTimeActivity.this, "Please select one option", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(ProjectTimeActivity.this, ImageReferentActivity.class);
                    Utilities.saveString(ProjectTimeActivity.this,"project_time",project_time);
                    startActivity(intent);
                }
                break;
        }
    }
}
