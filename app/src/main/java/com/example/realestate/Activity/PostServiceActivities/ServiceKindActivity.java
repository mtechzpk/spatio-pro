package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Adapters.PropertyTypeAdapter;
import com.example.realestate.Adapters.ServiceKindAdapter;
import com.example.realestate.Models.CityEntity;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.Models.ServiceKindModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServiceKindActivity extends AppCompatActivity implements ServiceKindAdapter.Callback {
    JSONArray objUser;
    JSONObject jsonObject;
    private ImageView go_back;
    private RecyclerView recyclerView;
    private ServiceKindAdapter pAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_kind);

        go_back = findViewById(R.id.go_back);
        recyclerView = findViewById(R.id.recycler_property_type);
        getChannels();

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceKindActivity.this.finish();
            }
        });

    }

    private void getChannels() {

        final ProgressDialog progressdialog = new ProgressDialog(this);
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);

        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/service_option", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<ServiceKindModel> serviceKindModels = new ArrayList<ServiceKindModel>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status == 1) {
                        progressdialog.dismiss();
                        JSONArray objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String name = jsonObject.getString("service_option");
                            serviceKindModels.add(new ServiceKindModel(id, name, "abc"));


                        }

                        pAdapter = new ServiceKindAdapter(serviceKindModels, getApplicationContext(), ServiceKindActivity.this);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1, RecyclerView.VERTICAL, false);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(pAdapter);

                    } else {

                        Toast.makeText(ServiceKindActivity.this, "No Category Avilable..!", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (this != null)
                    Toast.makeText(ServiceKindActivity.this, message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "get_service_option");
                params.put("apikey", "mtech");
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(this).addToRequestQueue(RegistrationRequest);
    }

    @Override
    public void onItemClick(int pos, int id, String prop_name) {

        Intent intent = new Intent(ServiceKindActivity.this, ServiceLocationActivity.class);
        intent.putExtra("property_cat_name", prop_name);

        startActivity(intent);


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (pAdapter != null) {
            pAdapter.notifyDataSetChanged();
        }
    }
}