package com.example.realestate.Activity.LoginDetail;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.AddPostActivities.EnterCodeActivity;
import com.example.realestate.Activity.PostServiceActivities.DataActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.Constants;
import com.example.realestate.api.ServerCallback;
import com.facebook.login.Login;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

import static java.security.AccessController.getContext;

public class LoginActivity extends AppCompatActivity {

    private TextView reset_password_text, signup_text;
    private Button login_btn;
    private ImageView go_back;
    private TextInputEditText email, password;
    private String token, newToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Paper.init(this);
        initViews();
        clickViews();

    }

    public void initViews() {
        FirebaseInstanceId.getInstance().getToken();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
//            getActivity().getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
        });

//            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
//                String newToken = instanceIdResult.getToken();
//                Log.e("newToken", newToken);
////                LoginActivity.this.getPreferences(Context.MODE_PRIVATE).edit().putString("fb", newToken).apply();
//            });


        reset_password_text = findViewById(R.id.reset_password_text);
        signup_text = findViewById(R.id.signup_text);
        login_btn = findViewById(R.id.login_btn);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        go_back = findViewById(R.id.go_back);
    }

    public void clickViews() {

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.finish();
            }
        });

        reset_password_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ResetPasswordActivity.class));
            }
        });

        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
            }
        });

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!email.getText().toString().isEmpty() && !password.getText().toString().isEmpty()) {
                    if (!password.getText().toString().isEmpty()) {
                        Utilities.saveString(LoginActivity.this, "user_password", password.getText().toString());
                        LoginApiCall(email.getText().toString(), password.getText().toString());
                    } else {
                        password.setError("Password Required");
                    }
                } else {
                    email.setError("Email Required");
                }
            }
        });

    }

    public void LoginApiCall(final String email, final String password) {
//        final ProgressDialog progressdialog;
//        progressdialog = new ProgressDialog(LoginActivity.this);
//        progressdialog.setMessage("Please wait..");
//        progressdialog.setCancelable(false);
//        progressdialog.show();
        Utilities.showProgressDialog(LoginActivity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        params.put("apikey", "travces.com");
        params.put("do", "login_user");
        params.put("token", newToken);


        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/login_user", LoginActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("Success");
//                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            JSONObject obj = jsonObject.getJSONObject("result");
                            SessionManager sessionManager = new SessionManager(LoginActivity.this);

                            int id = obj.getInt("id");
                            String email = obj.getString("email");
                            String name = obj.getString("name");
                            Utilities.saveInt(LoginActivity.this, "user_id", id);
                            Utilities.saveString(LoginActivity.this, "user_email", email);
//                        String user_id = obj.getString("id");
//                        String photooo = obj.getString("photo");

                            //                        String photo = Server.BASE_URL_PHOTO + obj.getString("photo");

                            Paper.book().write("user_id", id);


                            sessionManager.createLoginSession(name, email, id);

//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();
                            Toast.makeText(LoginActivity.this, "Login Successfuly", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginActivity.this, RealEstateActivity.class));
                            finish();

                        } else {
//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Utilities.hideProgressDialog();
                    Toast.makeText(LoginActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
