package com.example.realestate.Activity.FindServicesActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;

public class GetStartedActivity extends AppCompatActivity {
    private RelativeLayout next_relative;
    private ImageView go_back;

    private TextView lets_start_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        String service = Utilities.getString(this,"service_selection");
        go_back = findViewById(R.id.go_back);
        next_relative = findViewById(R.id.next_relative);
        lets_start_text= findViewById(R.id.lets_start_text);
        lets_start_text.setText(getString(R.string.get_started_text)+service);

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetStartedActivity.this.finish();
            }
        });
        next_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GetStartedActivity.this,ProjectLocationActivity.class);
                startActivity(intent);
            }
        });
    }
}
