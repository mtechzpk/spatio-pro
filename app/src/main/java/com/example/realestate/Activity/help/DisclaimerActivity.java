package com.example.realestate.Activity.help;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.realestate.Activity.LandingActivity;
import com.example.realestate.R;
import com.github.barteksc.pdfviewer.PDFView;

public class DisclaimerActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        PDFView pdfView = findViewById(R.id.pdfView);
        pdfView.fromAsset("disclaimer.pdf").load();
    }

    @Override
    public void onBackPressed() {
       startActivity(new Intent(DisclaimerActivity.this, LandingActivity.class));
       finish();
    }
}