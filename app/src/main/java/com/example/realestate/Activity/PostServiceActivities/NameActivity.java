package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;

import io.paperdb.Paper;

public class NameActivity extends AppCompatActivity {
    private EditText f_name, l_name;
    private TextView btn_previous, btn_next;
    private String first_name = "", last_name = "", name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        f_name = findViewById(R.id.f_name);
        l_name = findViewById(R.id.l_name);
        btn_previous = findViewById(R.id.btn_previous);
        btn_next = findViewById(R.id.btn_next);

        clickViews();
    }

    private void clickViews() {
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NameActivity.this.finish();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                first_name = f_name.getText().toString().trim();
                last_name = l_name.getText().toString().trim();
                name = first_name + " " + last_name;



                if (!first_name.isEmpty()) {
                    if (!last_name.isEmpty()) {
                        //to Next
//                        findservice();
                        Intent intent = new Intent(NameActivity.this, TopProvidersActivity.class);
                        startActivity(intent);

                        Utilities.saveString(NameActivity.this,"service_attender_lName",name);
                        Utilities.saveString(NameActivity.this,"service_attender_FName",name);
//                        Toast.makeText(NameActivity.this, "Hello " + name, Toast.LENGTH_SHORT).show();

                    } else {
                        l_name.setError("Last Name Required");
                    }
                } else {
                    f_name.setError("First Name Required");
                }
            }
        });
    }

}
