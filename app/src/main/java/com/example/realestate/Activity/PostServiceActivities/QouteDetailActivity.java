package com.example.realestate.Activity.PostServiceActivities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.DetailPreviewActivity;
import com.example.realestate.Activity.LoginDetail.SignUpActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.QouteModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;

public class QouteDetailActivity extends AppCompatActivity {
    private Button request_code_btn;
    private TextView close, company_name, owner_name, owner_address, owner_services, service_Attension;
    private QouteModel qouteModels;
    private ImageView go_back;
    private TextInputEditText contact_name, contact_email, contact_phone;
    private EditText contact_description;
    private String nam = "", mail = "", cntct = "", query = "";
    private ImageView image1, image2, image3, image4, image5, image6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qoute_detail);
        Paper.init(this);
        qouteModels = Paper.book().read("qoute_data");
        initViews();

        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QouteDetailActivity.this.finish();
            }
        });

        request_code_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nam = contact_name.getText().toString().trim();
                mail = contact_email.getText().toString().trim();
                cntct = contact_phone.getText().toString().trim();
                query = contact_description.getText().toString().trim();

                if (!nam.isEmpty()) {
                    if (!mail.isEmpty()) {
                        if (!cntct.isEmpty()) {
                            if (!query.isEmpty()) {
//                                ContactApiCall();
                                sendMailApi();
                            } else {
                                contact_description.setError("Write some description please.");
                            }
                        } else {
                            contact_email.setError("Phone Required");
                        }
                    } else {
                        contact_email.setError("Email Required");
                    }
                } else {
                    contact_name.setError("Name Required");
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initViews() {
        request_code_btn = findViewById(R.id.request_code_btn);

        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        image6 = findViewById(R.id.image6);
        close = findViewById(R.id.close);
        service_Attension = findViewById(R.id.service_Attension);
        company_name = findViewById(R.id.company_name);
        owner_name = findViewById(R.id.owner_name);
        owner_address = findViewById(R.id.owner_address);
        owner_services = findViewById(R.id.owner_services);

        contact_name = findViewById(R.id.contact_name);
        contact_email = findViewById(R.id.contact_email);
        contact_phone = findViewById(R.id.contact_phone);
        contact_description = findViewById(R.id.contact_description);
        go_back = findViewById(R.id.go_back);


        company_name.setText(qouteModels.getC_name());
        owner_name.setText(qouteModels.getPerson_name());
        owner_address.setText(qouteModels.getAddress());


        String s = qouteModels.getService_options().toString();
        s = s.substring(1, s.length() - 1);

        owner_services.setText(s);
        service_Attension.setText(qouteModels.getService_attention());
        Picasso.get().load(qouteModels.getImage1()).placeholder(R.drawable.comp_name_logo).into(image1);
        Picasso.get().load(qouteModels.getImage2()).placeholder(R.drawable.comp_name_logo).into(image2);
        Picasso.get().load(qouteModels.getImage3()).placeholder(R.drawable.comp_name_logo).into(image3);
        Picasso.get().load(qouteModels.getImage4()).placeholder(R.drawable.comp_name_logo).into(image4);
        Picasso.get().load(qouteModels.getImage5()).placeholder(R.drawable.comp_name_logo).into(image5);
        Picasso.get().load(qouteModels.getImage6()).placeholder(R.drawable.comp_name_logo).into(image6);

    }


    public void ContactApiCall() {
        Utilities.showProgressDialog(QouteDetailActivity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", nam);
        //TODO: check priovider email
        params.put("provider_email", qouteModels.getEmail());
        params.put("phone", cntct);
        params.put("user_email", mail);
        params.put("message", query);


        HashMap<String, String> headers = new HashMap<String, String>();


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/sendemail/send", QouteDetailActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("success");

                        if (status == 1) {
                            String message = jsonObject.getString("message");
                            final JSONObject objUser = jsonObject.getJSONObject("data");
                            Utilities.hideProgressDialog();
                            Toast.makeText(QouteDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(QouteDetailActivity.this, RealEstateActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
                            Toast.makeText(QouteDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(QouteDetailActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }


    public void sendMailApi() {
        Utilities.showProgressDialog(QouteDetailActivity.this, "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", nam);
        //TODO: check priovider email
        params.put("project_location", Utilities.getString(QouteDetailActivity.this, "referent_image"));
        params.put("emergencey", Utilities.getString(QouteDetailActivity.this, "emergency"));
        params.put("location_type", Utilities.getString(QouteDetailActivity.this, "location_type"));
        params.put("project_status", Utilities.getString(QouteDetailActivity.this, "project_status"));

        params.put("expected_time", Utilities.getString(QouteDetailActivity.this, "project_time"));
        params.put("photo", Utilities.getString(QouteDetailActivity.this, "referent_image"));
        params.put("short_description", query);
        params.put("streat", Utilities.getString(QouteDetailActivity.this, "proj_street"));

        params.put("city", Utilities.getString(QouteDetailActivity.this, "proj_city"));
        params.put("phone", cntct);
        params.put("email", mail);
        params.put("f_name", Utilities.getString(QouteDetailActivity.this, "service_attender_FName"));
        params.put("l_name", Utilities.getString(QouteDetailActivity.this, "service_attender_lName"));
        params.put("provider_email", qouteModels.getEmail());


        HashMap<String, String> headers = new HashMap<String, String>();


        ApiModelClass.GetApiResponse(Request.Method.POST, Server.send_request_to_provider, QouteDetailActivity.this, params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("Success");

                        if (status == 1) {
                            String message = jsonObject.getString("data");
                            Utilities.hideProgressDialog();
                            Toast.makeText(QouteDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                        } else {
                            Utilities.hideProgressDialog();
                            Toast.makeText(QouteDetailActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(QouteDetailActivity.this, ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}
