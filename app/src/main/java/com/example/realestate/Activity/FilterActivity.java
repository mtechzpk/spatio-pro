package com.example.realestate.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.PropertyDetailActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

public class FilterActivity extends AppCompatActivity {

    private Spinner sp_property_type, sp_rent_or_sale, sp_aminities;
    private ImageView go_back;
    private EditText no_min, no_max, neighbourhood,contry_filter,province_filter;
    private TextView number_beds, number_baths, minus1, minus2, plus1, plus2, btn_search, reset;
    private String price_min = "", price_max = "", property_type = "", aminities = "",
            beds = "", baths = "", rent = "", neighbourhood_st = "",country_st = "",provience_st = "", search_type = "";
    private int bed_no = 0;
    private int bath_no = 0;
    private LinearLayout others, bed_search, price_search;
    private List<String> prop_type, aminities_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Paper.init(this);
        Intent intent = getIntent();
        search_type = intent.getStringExtra("search_type");
        initViews();
        getprop_type();
        getamenities();
        ClickViews();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.rent_or_sale, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_rent_or_sale.setAdapter(adapter);
    }


    private void initViews() {

        sp_rent_or_sale = findViewById(R.id.rent_or_sale);
        sp_property_type = findViewById(R.id.sp_property_type);
        sp_aminities = findViewById(R.id.sp_aminities);
        go_back = findViewById(R.id.go_back);

        others = findViewById(R.id.others);
        bed_search = findViewById(R.id.bed_search);
        price_search = findViewById(R.id.price_search);

        btn_search = findViewById(R.id.btn_search);

        no_min = findViewById(R.id.no_min);
        no_max = findViewById(R.id.no_max);
        neighbourhood = findViewById(R.id.neighbourhood);
        province_filter = findViewById(R.id.province_filter);
        contry_filter = findViewById(R.id.contry_filter);
        number_beds = findViewById(R.id.number_beds);
        number_baths = findViewById(R.id.number_baths);
        minus1 = findViewById(R.id.minus1);
        minus2 = findViewById(R.id.minus2);
        plus1 = findViewById(R.id.plus1);
        plus2 = findViewById(R.id.plus2);

        reset = findViewById(R.id.reset);
    }

    private void ClickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, LandingActivity.class);
                startActivity(intent);
                FilterActivity.this.finish();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price_min = no_min.getText().toString().trim();
                price_max = no_max.getText().toString().trim();
                neighbourhood_st = neighbourhood.getText().toString().trim();
                country_st = contry_filter.getText().toString().trim();
                provience_st = province_filter.getText().toString().trim();
                beds = String.valueOf(bed_no);
                baths = String.valueOf(bath_no);
                property_type = sp_property_type.getSelectedItem().toString();
                aminities = sp_aminities.getSelectedItem().toString();
                rent = sp_rent_or_sale.getSelectedItem().toString();

                if (rent.equals("Select Category")) {
                    rent = "";
                } else {
                    Paper.book().write("rent", rent);
                }
                if (property_type.equals("Select Property Type")) {
                    property_type = "";
                } else {
                    Paper.book().write("property_type", property_type);
                }
                if (aminities.equals("Select Amenities")) {
                    aminities = "";
                } else {
                    Paper.book().write("aminities", aminities);
                }
                Paper.book().write("price_min", price_min);
                Paper.book().write("price_max", price_max);
                Paper.book().write("beds", beds);
                Paper.book().write("baths", baths);

                Paper.book().write("neighbourhood", neighbourhood_st);
                Paper.book().write("country_filter", country_st);
                Paper.book().write("provice_filter", provience_st);

                String filter = "filterScreen";
                Paper.book().write("filter", filter);


                Intent intent = new Intent(FilterActivity.this, LandingActivity.class);
                startActivity(intent);
                finish();

            }
        });

        plus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bed_no = bed_no + 1;
                number_beds.setText("" + bed_no);

            }
        });

        minus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bed_no < 0) {
                    bed_no = 0;
                    number_beds.setText(bed_no + "");
                }
                if (bed_no > 0) {
                    bed_no = bed_no - 1;
                    number_beds.setText(bed_no + "");
                }
            }
        });

        plus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bath_no = bath_no + 1;
                number_baths.setText("" + bath_no);

            }
        });

        minus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bath_no < 0) {
                    bath_no = 0;
                    number_baths.setText(bath_no + "");
                }
                if (bath_no > 0) {
                    bath_no = bath_no - 1;
                    number_baths.setText(bath_no + "");
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = getIntent();
                finish();
                startActivity(intent1);
            }
        });


        if (search_type.equals("price")) {

            others.setVisibility(View.GONE);
            bed_search.setVisibility(View.GONE);

        } else if (search_type.equals("beds")) {
            price_search.setVisibility(View.GONE);
            others.setVisibility(View.GONE);
        } else {
            others.setVisibility(View.VISIBLE);
            bed_search.setVisibility(View.VISIBLE);
            price_search.setVisibility(View.VISIBLE);
        }

    }

    private void getprop_type() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/property_type", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("Success");
                    if (status.equals("1")) {


                        prop_type = new ArrayList<>();
                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {


                            JSONObject object1 = jsonArray.getJSONObject(i);
                            String namee = object1.getString("property_type");


                            prop_type.add(namee);
                        }




                        String myString = "Select Property Type";
                        prop_type.add(myString);


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(FilterActivity.this, android.R.layout.simple_spinner_item, prop_type);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_property_type.setAdapter(dataAdapter);

                        dataAdapter = (ArrayAdapter) sp_property_type.getAdapter();
                        int spinnerPosition = dataAdapter.getPosition(myString);
                        sp_property_type.setSelection(spinnerPosition);

                        sp_property_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                                Utilities.saveString(FilterActivity.this, "prop_fect", parent.getItemAtPosition(position).toString());

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    } else {
//                        String message = object.getString("message");
                        Toast.makeText(FilterActivity.this, "result not fount", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (FilterActivity.this != null)
                    Toast.makeText(FilterActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                params.put("do", "property_type");
                params.put("apikey", "travces.com");
                params.put("property_type", property_type);

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(FilterActivity.this).addToRequestQueue(RegistrationRequest);


    }
    private void getamenities() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/amenities", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status==1) {


                        JSONArray jsonArray = object.getJSONArray("data");



                        aminities_type = new ArrayList<>();
                        JSONArray jsonArray1 = object.getJSONArray("data");
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            JSONObject object2 = jsonArray.getJSONObject(j);
                            String nameee = object2.getString("aminties");


                            aminities_type.add(nameee);
                        }


                        String myString = "Select Amenities";

                        aminities_type.add(myString);

                        // Creating adapter for spinner


                        ArrayAdapter<String> aminitiesAdapter = new ArrayAdapter<String>(FilterActivity.this, android.R.layout.simple_spinner_item, aminities_type);
                        aminitiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_aminities.setAdapter(aminitiesAdapter);

                        aminitiesAdapter = (ArrayAdapter) sp_aminities.getAdapter();
                        int spinnerPos = aminitiesAdapter.getPosition(myString);
                        sp_aminities.setSelection(spinnerPos);

                        sp_aminities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                                Utilities.saveString(FilterActivity.this, "aminity_fect", parent.getItemAtPosition(position).toString());

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
//                        String message = object.getString("message");
                        Toast.makeText(FilterActivity.this, "Result not found", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (FilterActivity.this != null)
                    Toast.makeText(FilterActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
//                params.put("do", "amenities");
//                params.put("apikey", "mtechapi12345");


                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(FilterActivity.this).addToRequestQueue(RegistrationRequest);


    }
}
