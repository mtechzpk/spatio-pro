package com.example.realestate.Activity.LoginDetail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.LanguagePreActivity;
import com.example.realestate.Activity.PostServiceActivities.PostServiceLoginActivity;
import com.example.realestate.Activity.ProfileActivity1;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpOrLoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private LinearLayout google_btn_linear, facebook_btn_linear, email_btn_linear;
    private ImageView go_back;
    String data = "";
    //    private TextView info;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    SignInButton signInButton;
    private GoogleApiClient googleApiClient;
    TextView textView;
    private static final int RC_SIGN_IN = 1;
    String fb_id, fb_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_or_login);

        SessionManager sessionManager = new SessionManager(getApplicationContext());
        if (sessionManager.isLoggedIn()) {
            startActivity(new Intent(SignUpOrLoginActivity.this, RealEstateActivity.class));
            finish();
        }


        data = Utilities.getString(this, "post_feact");

//        data = intent.getStringExtra("post_prop");
        InitViews();
        clickViews();
        TextView textView = (TextView) signInButton.getChildAt(0);
        textView.setText("Sign in with Google");

        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();
//        info = (TextView)findViewById(R.id.info);
        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginWithFacebookApiCall();
                AccessToken accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                fb_id = profile.getId();
                fb_name = profile.getName();
                Uri fb_image = profile.getProfilePictureUri(100, 100);

                Utilities.saveString(SignUpOrLoginActivity.this, "login", "facebook");


//                info.setText("User ID: " + loginResult.getAccessToken().getUserId() + "\n" + "Auth Token: " + loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                Toast.makeText(SignUpOrLoginActivity.this, "Login attempt canceled.", Toast.LENGTH_SHORT).show();
//                info.setText("Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(SignUpOrLoginActivity.this, "Login attempt failed.", Toast.LENGTH_SHORT).show();

//                info.setText("Login attempt failed.");
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, RC_SIGN_IN);
            }
        });
    }


    private void InitViews() {
        go_back = findViewById(R.id.go_back);

//        google_btn_linear = findViewById(R.id.google_btn_linear);
//        facebook_btn_linear = findViewById(R.id.facebook_btn_linear);
        email_btn_linear = findViewById(R.id.email_btn_linear);
        signInButton = findViewById(R.id.sign_in_button);
    }

    private void clickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        google_btn_linear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //sign in with google account
//            }
//        });

//        facebook_btn_linear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //sign in with facebook account
//            }
//        });

        email_btn_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sign in with email account


                Intent intent = new Intent(SignUpOrLoginActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            gotoProfile();
        } else {
            Toast.makeText(getApplicationContext(), "Sign in cancel", Toast.LENGTH_LONG).show();
        }
    }

    private void gotoProfile() {
        Intent intent = new Intent(SignUpOrLoginActivity.this, ProfileActivity1.class);
        startActivity(intent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void loginWithFacebookApiCall() {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(SignUpOrLoginActivity.this);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.login_with_facebook, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("status");
                    String message = object.getString("message");
                    if (status == 200) {
                        SessionManager sessionManager = new SessionManager(SignUpOrLoginActivity.this);
                        JSONObject obj = object.getJSONObject("data");
                        int id = obj.getInt("id");
                        String facebook_id = obj.getString("facebook_id");
                        String email = obj.getString("email");


                        Utilities.saveString(SignUpOrLoginActivity.this, "user_email", email);
                        Utilities.saveString(SignUpOrLoginActivity.this, "user_facebook_id", facebook_id);
                        Utilities.saveInt(SignUpOrLoginActivity.this, "user_id", id);

//                        SessionManager sessionManager = new SessionManager(LoginActivity.this);
                        Toast.makeText(SignUpOrLoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        progressdialog.dismiss();
                        startActivity(new Intent(SignUpOrLoginActivity.this, RealEstateActivity.class));
                        finish();
                        sessionManager.createLoginSession(facebook_id, email, id);
                    } else {
                        Toast.makeText(SignUpOrLoginActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "Invalid Credential";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (SignUpOrLoginActivity.this != null)
                    Toast.makeText(SignUpOrLoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", "abdul.rehman@gmail.com");
                params.put("facebook_id", fb_id);
                params.put("name", fb_name);
//

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("Accept", "application/json");
                return params;
            }


        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(SignUpOrLoginActivity.this).addToRequestQueue(RegistrationRequest);


    }
}
