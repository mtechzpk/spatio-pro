package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.Models.NotificationModel;
import com.example.realestate.R;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<NotificationModel> notificationModel;

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationModel) {
        this.context = context;
        this.notificationModel = notificationModel;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_rv, parent, false);
        return new NotificationAdapter.BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        NotificationAdapter.BookViewHolder holder1 = (NotificationAdapter.BookViewHolder) holder;
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return notificationModel.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView notification_title, notification_desc;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            notification_title = itemView.findViewById(R.id._notification_title);
            notification_desc = itemView.findViewById(R.id._notification_desc);


        }

        private void bind(int pos) {
            NotificationModel notificationModels = notificationModel.get(pos);
            notification_desc.setText(notificationModels.getDesc());
            notification_title.setText(notificationModels.getTitle());


        }

    }
}
