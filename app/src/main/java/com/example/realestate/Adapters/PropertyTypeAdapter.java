package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.R;

import java.util.ArrayList;

public class PropertyTypeAdapter extends RecyclerView.Adapter<PropertyTypeAdapter.MyViewHolder> {


    ArrayList<PropertyTypeModel> channelModels;
    Context context;
    Callback callback;


    public PropertyTypeAdapter(ArrayList<PropertyTypeModel> channelModels, Context context, Callback callback) {
        this.channelModels = channelModels;
        this.context = context;
        this.callback = callback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.row_proprty_type, parent, false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {


        holder.textview.setText(channelModels.get(position).getPropertytype());
        holder.radio_btn.setImageResource(R.drawable.unmark_ic);

        holder.initClickListener();


    }

    @Override
    public int getItemCount() {

        return channelModels.size();

    }

    public interface Callback {
        void onItemClick(int pos, int id, String type_name);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView textview;
        ImageView radio_btn;


        public MyViewHolder(View itemView) {
            super(itemView);
            textview = itemView.findViewById(R.id.textView);
            radio_btn = itemView.findViewById(R.id.radio_btn);


        }

        private void initClickListener() {

            radio_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    radio_btn.setImageResource(R.drawable.mark_ic);
                    callback.onItemClick(getAdapterPosition(), channelModels.get(getAdapterPosition()).getId(), channelModels.get(getAdapterPosition()).getPropertytype());
                }
            });
        }


    }

}
