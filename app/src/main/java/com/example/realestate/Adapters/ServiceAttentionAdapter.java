package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.realestate.Activity.PostServiceActivities.ServiceDetailActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.ServiceAttentionModel;
import com.example.realestate.R;

import java.util.ArrayList;

import io.paperdb.Paper;

public class ServiceAttentionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ServiceAttentionModel> serviceAttentionModels;
    private Callback callback;
    private ArrayList<String> selectedAttention;
    private ArrayList<String> selectedValues;
    private int lastSelectedPosition = -1;

    public ServiceAttentionAdapter(Context context, ArrayList<ServiceAttentionModel> serviceAttentionModels) {
        this.context = context;
        this.serviceAttentionModels = serviceAttentionModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_proprty_type, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return serviceAttentionModels.size();
    }

    public interface Callback {
        void onItemClick(int pos);
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView image_radio;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
            image_radio = itemView.findViewById(R.id.radio_btn);
            Paper.init(context);
            selectedAttention = new ArrayList<>();
            selectedAttention.clear();

        }

        private void bind(final int pos) {
            final ServiceAttentionModel poem = serviceAttentionModels.get(pos);
            textView.setText(poem.getServiceName());
            image_radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    image_radio.setImageResource(R.drawable.mark_ic);

                    if (selectedAttention.size() > 0) {
                        if (selectedAttention.indexOf(poem.getServiceName()) != -1) {

                            image_radio.setImageResource(R.drawable.unmark_ic);
                            selectedAttention.remove(selectedAttention.indexOf(poem.getServiceName()));
                        } else {
                            selectedAttention.add(poem.getServiceName());
                            Paper.book().write("attentionList", selectedAttention);
//                            Utilities.saveString(context, "attentionList", selectedAttention.toString());

                        }
                    } else {

                        selectedAttention.add(poem.getServiceName());

                        Paper.book().write("attentionList", selectedAttention);

//                        Utilities.saveString(context, "attentionList", selectedAttention.toString());

                    }

                }
            });

            initClickListener();
        }

        private void initClickListener() {

        }
    }
}
