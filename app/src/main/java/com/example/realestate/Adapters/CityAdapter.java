package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.Activity.PostServiceActivities.ServiceTypeActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.CityEntity;
import com.example.realestate.R;

import java.util.ArrayList;

import io.paperdb.Paper;
import me.yokeyword.indexablerv.IndexableAdapter;

public class CityAdapter extends IndexableAdapter<CityEntity> {
    final static ArrayList<String> selectedAttention = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;

    public CityAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateTitleViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.item_index_city, parent, false);
        return new IndexVH(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateContentViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.item_city, parent, false);
        return new ContentVH(view);
    }

    @Override
    public void onBindTitleViewHolder(RecyclerView.ViewHolder holder, String indexTitle) {
        IndexVH vh = (IndexVH) holder;
        vh.tv.setText(indexTitle);
    }

    @Override
    public void onBindContentViewHolder(RecyclerView.ViewHolder holder, final CityEntity entity) {
        final ContentVH vh = (ContentVH) holder;
        vh.tv.setText(entity.getName());

        vh.markic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedAttention.size() > 0) {
                    if (selectedAttention.size() < 3) {
                        if (selectedAttention.indexOf(entity.getName()) != -1) {

                            vh.markic.setImageResource(R.drawable.unmark_ic);
                            selectedAttention.remove(selectedAttention.indexOf(entity.getName()));
                            Paper.book().write("serviceoption", selectedAttention);
//                            Utilities.saveString(context, "attentionList", selectedAttention.toString());
                        } else {
                            vh.markic.setImageResource(R.drawable.mark_ic);
                            selectedAttention.add(entity.getName());
                            Paper.book().write("serviceoption", selectedAttention);
//                            Utilities.saveString(context, "attentionList", selectedAttention.toString());
                        }
                    } else {
                        if (selectedAttention.indexOf(entity.getName()) != -1) {

                            vh.markic.setImageResource(R.drawable.unmark_ic);
                            selectedAttention.remove(selectedAttention.indexOf(entity.getName()));
                            Paper.book().write("serviceoption", selectedAttention);
//                            Utilities.saveString(context, "attentionList", selectedAttention.toString());
                        } else {
                            Toast.makeText(context, "You can select only Three Services..!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    vh.markic.setImageResource(R.drawable.mark_ic);
                    selectedAttention.add(entity.getName());
                    Paper.book().write("serviceoption", selectedAttention);
//                    Utilities.saveString(context, "attentionList", selectedAttention.toString());

                }
            }
        });
    }

    private class IndexVH extends RecyclerView.ViewHolder {
        TextView tv;

        public IndexVH(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv_index);
            Paper.init(context);


        }
    }

    private class ContentVH extends RecyclerView.ViewHolder {
        TextView tv;
        ImageView markic;

        public ContentVH(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv_name);
            markic = (ImageView) itemView.findViewById(R.id.markic);
        }
    }

}
