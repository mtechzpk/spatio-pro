package com.example.realestate.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.Activity.PostServiceActivities.QouteDetailActivity;
import com.example.realestate.Models.QouteModel;
import com.example.realestate.Models.ServiceAttentionModel;
import com.example.realestate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.paperdb.Paper;

public class QouteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<QouteModel> qouteModels;

    public QouteAdapter(Context context, ArrayList<QouteModel> qouteModels) {
        this.context = context;
        this.qouteModels = qouteModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pros_to_qoute_rv, parent, false);
        return new QouteAdapter.BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        QouteAdapter.BookViewHolder holder1 = (QouteAdapter.BookViewHolder) holder;
        holder1.bind(position);
        Picasso.get().load(qouteModels.get(position).getIvProfile()).placeholder(R.drawable.comp_name_logo).into(holder1.ivProfile);
        holder1.btn_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QouteDetailActivity.class);

//                Paper.book().read("qoute_data",qouteModels.get(position));
                Paper.book().write("qoute_data", qouteModels.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return qouteModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView c_name, status, grade, reviews, service_title, service_provider_location;
        Button btn_code;
        ImageView ivProfile;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            c_name = itemView.findViewById(R.id.c_name);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            status = itemView.findViewById(R.id.status);
            service_title = itemView.findViewById(R.id.service_title);
            service_provider_location = itemView.findViewById(R.id.service_provider_location);

            btn_code = itemView.findViewById(R.id.btn_code);
            Paper.init(context);

        }

        private void bind(int pos) {
            QouteModel qouteModel = qouteModels.get(pos);
            Picasso.get().load(qouteModels.get(pos).getIvProfile()).into(ivProfile);
            c_name.setText(qouteModel.getProvide_name());
            if (!qouteModel.getCertifiation().equals("")) {
                status.setText("Certified");
            } else {
                status.setText("Not Certified");
            }

            service_provider_location.setText(qouteModel.getAddress());
            service_title.setText(qouteModel.getService_options().get(0).toString());
//            grade.setText(qouteModel.getGrade());
//            reviews.setText(qouteModel.getReviews());


        }


    }

}
