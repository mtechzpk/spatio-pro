package com.example.realestate.Adapters;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.Models.NearbyData;
import com.example.realestate.R;
import com.example.realestate.Server.Server;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScrollRecyclerAdapter extends RecyclerView.Adapter<HomeScrollRecyclerAdapter.ViewHolder> {

    private Activity activity;
    private String exId = null;

    private List<NearbyData> searchList = new ArrayList<NearbyData>();
    private onclickListner listner;
    private Dialog dialog;
    private String bit = "";

    public HomeScrollRecyclerAdapter() {
    }


    public HomeScrollRecyclerAdapter(Activity activity, List<NearbyData> allist) {
        this.activity = activity;
        this.searchList = allist;

    }

    public void onItemClickListner(onclickListner listner) {
        this.listner = listner;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.scroll_home_list_layout, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        holder.tvAmmount.setText("$" + searchList.get(position).getPrize());
        holder.tvBdNumber.setText(searchList.get(position).getBed());
        holder.tvBaNumber.setText(searchList.get(position).getBath());
        holder.tvBaNumber.setText(searchList.get(position).getBath());
        holder.area_type.setText(searchList.get(position).getArea_unit());
        holder.tvSqft.setText(searchList.get(position).getArea());
        holder.tvAddress.setText(searchList.get(position).getAddress());
        holder.prop_categ1.setText(searchList.get(position).getProperty_category());

        if (searchList.get(position).getProperty_category().equals("Sale")) {
            holder.prop_categ1.setText("On "+searchList.get(position).getProperty_category());
            holder.prop_categ1.setVisibility(View.VISIBLE);
            holder.prop_categ.setVisibility(View.GONE);

        } else if (searchList.get(position).getProperty_category().equals("Rent")) {
            holder.prop_categ.setText("On "+searchList.get(position).getProperty_category());
            holder.prop_categ.setVisibility(View.VISIBLE);
            holder.prop_categ1.setVisibility(View.GONE);
        }

        Picasso.get()
                .load(searchList.get(position).getProperty_pictures())
                .fit()
                .centerInside()
                .placeholder(R.drawable.bgg)
                .into(holder.bg_img);

//        holder.wineText.setText(searchList.get(position).getWinedetail().get(0).getName());
//        holder.wineIcon.setBackgroundResource(R.drawable.bulb);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listner.itemClick(searchList.get(position));
            }
        });
//        holder.rvTop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ArrayList<String> amenties=searchList.get(position).getAmenitieslist();
//                Toast.makeText(activity, String.valueOf(amenties), Toast.LENGTH_SHORT).show();
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public interface onclickListner {
        public void itemClick(NearbyData object);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAmmount, prop_categ, tvBdNumber, prop_categ1, tvBaNumber, tvSqft, area_type, tvAddress;
        ImageView bg_img;
        RelativeLayout rvTop;

        public ViewHolder(View view) {
            super(view);


            tvAmmount = view.findViewById(R.id.tvAmmount);
            prop_categ1 = view.findViewById(R.id.prop_categ1);
            rvTop = view.findViewById(R.id.rvTop);
            tvBdNumber = view.findViewById(R.id.tvBdNumber);
            tvBaNumber = view.findViewById(R.id.tvBaNumber);
            tvSqft = view.findViewById(R.id.tvSqft);
            prop_categ = view.findViewById(R.id.prop_categ);
            area_type = view.findViewById(R.id.area_type);
            tvAddress = view.findViewById(R.id.tvAddress);
            bg_img = view.findViewById(R.id.bg_img);
        }
    }


}
