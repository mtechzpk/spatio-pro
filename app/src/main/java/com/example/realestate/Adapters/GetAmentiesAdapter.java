package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.realestate.Models.GetAmentiesModel;
import com.example.realestate.R;

import java.util.ArrayList;

public class GetAmentiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;

    private ArrayList<GetAmentiesModel> getAmentiesModels;

    public GetAmentiesAdapter(Context context, ArrayList<GetAmentiesModel> getAmentiesModels) {
        this.context = context;
        this.getAmentiesModels = getAmentiesModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_amenties, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BookViewHolder holder1 = (BookViewHolder) holder;

        holder1.bind(position);
        holder1.tvAmenties.setText(getAmentiesModels.get(position).getAmenties());
    }

    @Override
    public int getItemCount() {
        return getAmentiesModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView tvAmenties;

        private BookViewHolder(View itemView) {
            super(itemView);

            //init views
            tvAmenties = itemView.findViewById(R.id.tvAmenties);
        }

        private void bind(int pos) {
            GetAmentiesModel messagesTabModel = getAmentiesModels.get(pos);
            tvAmenties.setText(messagesTabModel.getAmenties());
//            initClickListener();
        }
//        private void initClickListener() {
//            rlItems.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
//        }
    }
//    public interface Callback {
//        void onItemClick(int pos);
//    }
}
