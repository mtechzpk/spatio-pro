package com.example.realestate.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.realestate.R;
import com.example.realestate.dialogs.SavedpropropertiesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SavedPropertiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<SavedpropropertiesModel> savedpropropertiesModels;

    public SavedPropertiesAdapter(Context context, ArrayList<SavedpropropertiesModel> savedpropropertiesModels) {
        this.context = context;
        this.savedpropropertiesModels = savedpropropertiesModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_save_properties, parent, false);
        return new BookViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        BookViewHolder holder1 = (BookViewHolder) holder;
        holder1.tvPropertyName.setText("Title: " + savedpropropertiesModels.get(position).getPropertyName());
        holder1.phone.setText("Phone: " + savedpropropertiesModels.get(position).getPhone());
        holder1.address.setText("Address: " + savedpropropertiesModels.get(position).getAddress());
        holder1.street.setText(savedpropropertiesModels.get(position).getStreet());
        holder1.city.setText(savedpropropertiesModels.get(position).getCity());
        holder1.country.setText(savedpropropertiesModels.get(position).getCountry());
        holder1.building_no.setText(savedpropropertiesModels.get(position).getBuilding_no());
//        holder1.bed.setText(savedpropropertiesModels.get(position).getBed());
//        holder1.bath.setText(savedpropropertiesModels.get(position).getBath());
//        holder1.floor.setText(savedpropropertiesModels.get(position).getFloor());

        holder1.config_text.setText(savedpropropertiesModels.get(position).getBed() + "Bed" + " " + savedpropropertiesModels.get(position).getBath() + " " + "Bath" + " " + savedpropropertiesModels.get(position).getFloor() + "Floor");


        holder1.email.setText("Email: " + savedpropropertiesModels.get(position).getEmail());
        holder1.provider_name.setText("Name: " + savedpropropertiesModels.get(position).getProvider_name());
        holder1.priceType.setText("Price: " + savedpropropertiesModels.get(position).getPrice_unit());
        holder1.price.setText(savedpropropertiesModels.get(position).getPrice());
        holder1.propertId.setText("sp-" + savedpropropertiesModels.get(position).getPost_unit() + "-" + savedpropropertiesModels.get(position).getPropId());
        Picasso.get().load(savedpropropertiesModels.get(position).getImage()).into(holder1.ivProfile);
        holder1.bind(position);
    }

    @Override
    public int getItemCount() {
        return savedpropropertiesModels.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {

        TextView config_text, propertId, country, city, building_no, address, phone, price, tvPropertyName, email, bed, bath, floor, street, provider_name, priceType;
        ImageView ivProfile;
        LinearLayout llITem;

        private BookViewHolder(@NonNull View itemView) {
            super(itemView);
            city = itemView.findViewById(R.id.city);
            country = itemView.findViewById(R.id.country);
            propertId = itemView.findViewById(R.id.propertId);
            building_no = itemView.findViewById(R.id.building_no);
            provider_name = itemView.findViewById(R.id.provider_name);
            address = itemView.findViewById(R.id.address);
            priceType = itemView.findViewById(R.id.priceType);
            phone = itemView.findViewById(R.id.phone);
            tvPropertyName = itemView.findViewById(R.id.tvPropertyName);
            email = itemView.findViewById(R.id.email);
            config_text = itemView.findViewById(R.id.config_text);

            street = itemView.findViewById(R.id.street);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            email = itemView.findViewById(R.id.email);
            price = itemView.findViewById(R.id.price);


        }

        private void bind(int pos) {
            SavedpropropertiesModel messagesTabModel = savedpropropertiesModels.get(pos);
//            tvName.setText(messagesTabModel.getUserName());
//            initClickListener();
        }

//        private void initClickListener() {
//            llITem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
    }
}

//    public interface Callback {
//        void onItemClick(int pos);
//    }

