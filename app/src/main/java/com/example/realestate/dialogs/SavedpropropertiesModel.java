package com.example.realestate.dialogs;

public class SavedpropropertiesModel {
  int propId;
String post_unit,country,city,building_no,address,phone,price,PropertyName,email,bed,bath,floor,street,image,provider_name,total_floor,category,price_unit;

  public SavedpropropertiesModel(int propId, String post_unit, String country, String city, String building_no, String address, String phone, String price, String propertyName, String email, String bed, String bath, String floor, String street, String image, String provider_name, String total_floor, String category, String price_unit) {
    this.propId = propId;
    this.post_unit = post_unit;
    this.country = country;
    this.city = city;
    this.building_no = building_no;
    this.address = address;
    this.phone = phone;
    this.price = price;
    PropertyName = propertyName;
    this.email = email;
    this.bed = bed;
    this.bath = bath;
    this.floor = floor;
    this.street = street;
    this.image = image;
    this.provider_name = provider_name;
    this.total_floor = total_floor;
    this.category = category;
    this.price_unit = price_unit;
  }

  public int getPropId() {
    return propId;
  }

  public void setPropId(int propId) {
    this.propId = propId;
  }

  public String getPost_unit() {
    return post_unit;
  }

  public void setPost_unit(String post_unit) {
    this.post_unit = post_unit;
  }

  public String getTotal_floor() {
    return total_floor;
  }

  public void setTotal_floor(String total_floor) {
    this.total_floor = total_floor;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getPrice_unit() {
    return price_unit;
  }

  public void setPrice_unit(String price_unit) {
    this.price_unit = price_unit;
  }

  public String getProvider_name() {
    return provider_name;
  }

  public void setProvider_name(String provider_name) {
    this.provider_name = provider_name;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getBuilding_no() {
    return building_no;
  }

  public void setBuilding_no(String building_no) {
    this.building_no = building_no;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getPropertyName() {
    return PropertyName;
  }

  public void setPropertyName(String propertyName) {
    PropertyName = propertyName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getBed() {
    return bed;
  }

  public void setBed(String bed) {
    this.bed = bed;
  }

  public String getBath() {
    return bath;
  }

  public void setBath(String bath) {
    this.bath = bath;
  }

  public String getFloor() {
    return floor;
  }

  public void setFloor(String floor) {
    this.floor = floor;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}

