package com.example.realestate.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.PaymentMethodsActivities.CashPaymentActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import io.paperdb.Paper;


public class HelpDialog extends DialogFragment {
    EditText etName, etEmail, etMessage, etPhone;
    Button bSend;
    ImageView ivCancel;
    String userEmail, userName, message, phone;
    String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    StringBuilder endTime, startsTime;

    public static HelpDialog newInstance(int title) {
        HelpDialog frag = new HelpDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        frag.setArguments(args);
        return frag;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.help_dialog_box, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etName = view.findViewById(R.id.name_et);
        etEmail = view.findViewById(R.id.email_et);
        etPhone = view.findViewById(R.id.et_phone);
        etMessage = view.findViewById(R.id.et_message);
        ivCancel = view.findViewById(R.id.ivCancel);
        bSend = view.findViewById(R.id.bSend);


        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ivCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().cancel();
            }
        });
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userEmail = etEmail.getText().toString();
                userName = etName.getText().toString();
                message = etMessage.getText().toString();
                phone = etPhone.getText().toString();

                if (!userName.isEmpty()) {
                    if (!userEmail.isEmpty()) {
                        if (android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
                            if (!phone.isEmpty()) {
                                if (!message.isEmpty()) {

                                    SendMailAp1i(userName, userEmail, phone, message);


                                } else {
                                    Toast.makeText(getActivity(), "Please write something", Toast.LENGTH_SHORT).show();

                                }


                            } else {
                                Toast.makeText(getContext(), "phone required", Toast.LENGTH_SHORT).show();


                            }


                        } else {

                            Toast.makeText(getActivity(), "Enter Valid Email ", Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        Toast.makeText(getActivity(), "Name required", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    Toast.makeText(getActivity(), "Name required", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        timePicker1.setIs24HourView(true);
//
    }

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.MyAnimation_Window;
    }

    public void SendMailAp1i(final String userName, final String userEmail, final String phone, final String message) {
        Utilities.showProgressDialog(getActivity(), "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", userName);
        params.put("user_email", userEmail);
        params.put("phone", phone);
        params.put("message", message);
        params.put("provider_email", etEmail.getText().toString());


        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/sendemail/send", getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        JSONObject jsonObject = new JSONObject(String.valueOf(result));

                        int status = jsonObject.getInt("success");

                        if (status == 1) {
                            String Message = jsonObject.getString("message");
                            final JSONObject objUser = jsonObject.getJSONObject("data");
//                            String message=objUser.getString("message");
                            Utilities.hideProgressDialog();
                            Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                            getDialog().dismiss();

                        } else {
                            Utilities.hideProgressDialog();
                            String message = jsonObject.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
    private void  SendMailApi(final String userName, final String userEmail, final String phone, final String message) {
        Utilities.showProgressDialog(getActivity(), "please wait");
        final StringRequest dramarequest = new StringRequest(Request.Method.POST,"http://mtecsoft.com/spatio/api/sendemail/send", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    String msg = object.getString("Alert");
                    if (status == 1) {
                        Utilities.hideProgressDialog();
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    } else {
                        Utilities.hideProgressDialog();
                        Toast.makeText(getActivity(), "No Available", Toast.LENGTH_SHORT).show();
                    }
//                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
      Utilities.hideProgressDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utilities.hideProgressDialog();            String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("name", userName);
                params.put("user_email", userEmail);
                params.put("phone", phone);
                params.put("message", message);
                params.put("provider_email", etEmail.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        dramarequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(dramarequest);
    }
}