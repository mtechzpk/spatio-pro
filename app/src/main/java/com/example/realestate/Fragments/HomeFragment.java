package com.example.realestate.Fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.realestate.R;
import com.example.realestate.dialogs.HelpDialog;

public class HomeFragment extends Fragment {

    View view;
    TextView tvSendBlog, tvContactUs;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
//        tvSendBlog = view.findViewById(R.id.tvSendBlog);
        tvContactUs = view.findViewById(R.id.tvContactUS);
//        tvSendBlog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), "sending", Toast.LENGTH_SHORT).show();
//            }
//        });
        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheet(v);
            }
        });
        return view;
}
public void showBottomSheet(View view) {
    HelpDialog newFragment = HelpDialog.newInstance(
            R.string.search_word_meaning_dialog_title);
    newFragment.show(getFragmentManager(), "dialog");

}

}
