package com.example.realestate.Fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;

import com.example.realestate.Activity.FilterActivity;
import com.example.realestate.Activity.HelpActivity;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Activity.PostServiceActivities.SearchListActivity;
import com.example.realestate.Activity.RealEstateActivity;
import com.example.realestate.Adapters.HomeScrollRecyclerAdapter;
import com.example.realestate.Extras.SessionManager;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.android.gms.location.LocationListener;

import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.Custom.CheckConnection;
import com.example.realestate.Activity.Custom.GPSTracker;
import com.example.realestate.Activity.DetailPreviewActivity;
import com.example.realestate.Models.NearbyData;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.thebrownarrow.permissionhelper.FragmentManagePermission;
import com.thebrownarrow.permissionhelper.PermissionResult;
import com.thebrownarrow.permissionhelper.PermissionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;


public class SearchFragment extends FragmentManagePermission implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static String property_category;
    String permissionAsk[] = {PermissionUtils.Manifest_CAMERA, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE,
            PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_ACCESS_FINE_LOCATION,
            PermissionUtils.Manifest_ACCESS_COARSE_LOCATION};
    private JSONArray objUser;
    private static JSONObject jsonObject;
    private View view;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private ImageView camera, camera_main, go_back, filter_list;
    private LinearLayout help_linear, header_search_main, llSetFilterPrice, llSetSearchLocation;
    private RelativeLayout footer_search_instruction;
    private Double currentLatitude;
    private Double currentLongitude;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private EditText show_main_outocomplete;
    private TextView filter_btn, price_text, beds_search;
    private RecyclerView bottomgrid_listview;
    private String filter = "", beds = "", baths = "", price_min = "", price_max = "",
            property_type = "", rent = "", neighbourhood = "", back = "", search_type = "", aminities = "", country_filter = "", provience_filter = "";
    private HomeScrollRecyclerAdapter scrollRecyclerAdapter;


    private String map_key = "AIzaSyCzd6uOtpcuGqik5zp0xvlRZpUhIl3gJ0U";

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this frafooter_search_instructiongment
        view = inflater.inflate(R.layout.fragment_search, container, false);
        Places.initialize(getActivity(), map_key);
        Paper.init(getActivity());

        initViews();
        checkPermission();
        ClickViews();
//        getproperty("30.898186427868456","72.43192590773106");
        return view;
    }

    private void checkPermission() {

        if (!CheckConnection.haveNetworkConnection(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.network), Toast.LENGTH_LONG).show();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {
                    if (!GPSEnable()) {
                        tunonGps();
                    } else {
                        getcurrentlocation();
                    }
                }

                @Override
                public void permissionDenied() {

                }

                @Override
                public void permissionForeverDenied() {

                    openSettingsApp(getActivity());
                }
            });

        } else {
            if (!GPSEnable()) {
                tunonGps();
            } else {
                getcurrentlocation();
            }

        }
    }

    private void initViews() {

        Paper.init(getContext());

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.google_map);
        mapFragment.getMapAsync(this);
        //        searchView = view.findViewById(R.id.sv_location);
        go_back = view.findViewById(R.id.go_back);
        filter_list = view.findViewById(R.id.filter_list);
//        camera = view.findViewById(R.id.camera);
        filter_btn = view.findViewById(R.id.filter_btn);
        llSetFilterPrice = view.findViewById(R.id.llSetFilterPrice);
        llSetSearchLocation = view.findViewById(R.id.llSetSearchLocation);
        price_text = view.findViewById(R.id.price_text);
        beds_search = view.findViewById(R.id.beds_search);
//        camera_main = view.findViewById(R.id.camera_main);
        help_linear = view.findViewById(R.id.help_linear);
        footer_search_instruction = view.findViewById(R.id.footer_search_instruction);
        show_main_outocomplete = view.findViewById(R.id.show_main_outocomplete);
        header_search_main = view.findViewById(R.id.header_search_main);
        bottomgrid_listview = view.findViewById(R.id.grid_listview);

    }

    private void ClickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Paper.book().delete("price_min");
                Paper.book().delete("price_max");
                Paper.book().delete("beds");
                Paper.book().delete("baths");
                Paper.book().delete("property_type");
                Paper.book().delete("rent");
                Paper.book().delete("filter");
                Paper.book().delete("neighbourhood");
                Paper.book().delete("country_filter");
                Paper.book().delete("provice_filter");


                if (back.equals("back")) {

                    Utilities.hideKeyboard(view, getActivity());
                    footer_search_instruction.setVisibility(View.VISIBLE);
                    header_search_main.setVisibility(View.GONE);
                    back = "";

                } else {
                    getActivity().finish();
                    back = "";
                }
            }
        });

        show_main_outocomplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                footer_search_instruction.setVisibility(View.GONE);
                header_search_main.setVisibility(View.VISIBLE);
                back = "back";

                return false;
            }
        });

        filter_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), FilterActivity.class);
//                intent.putExtra("search_type", search_type);
//                startActivity(intent);
                Utilities.saveString(getActivity(), "search_type", search_type);
                FilterFragment filterFragment = new FilterFragment();
                openFragment(filterFragment);
            }
        });
        llSetFilterPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), FilterActivity.class);
//                intent.putExtra("search_type", search_type);
//                startActivity(intent);

                Utilities.saveString(getActivity(), "search_type", search_type);
                FilterFragment filterFragment = new FilterFragment();
                openFragment(filterFragment);
            }
        });

        price_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_type = "price";
//                Intent intent = new Intent(getActivity(), FilterActivity.class);
//                intent.putExtra("search_type", search_type);
//                startActivity(intent);
                Utilities.saveString(getActivity(), "search_type", search_type);
                FilterFragment filterFragment = new FilterFragment();
                openFragment(filterFragment);
            }
        });

        beds_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_type = "beds";
//                Intent intent = new Intent(getActivity(), FilterActivity.class);
//                intent.putExtra("search_type", search_type);
//                startActivity(intent);
                Utilities.saveString(getActivity(), "search_type", search_type);
                FilterFragment filterFragment = new FilterFragment();
                openFragment(filterFragment);
            }
        });

//        Paper.book().write("search_type",search_type);

        help_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), HelpActivity.class
//                ));
                HelpFragment helpFragment = new HelpFragment();
                openFragment(helpFragment);
            }
        });


        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.NAME);
        if (fields != null) {
            autocompleteFragment.setPlaceFields(fields);
        }
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                LatLng latLng = place.getLatLng();
                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;
                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.title(currentLatitude + " : " + currentLongitude);
                    map.clear();
                    map.addMarker(markerOptions);
                    getDistanceApi(currentLatitude.toString(), currentLongitude.toString());
                    CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 12);
                    map.animateCamera(camera);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getActivity(), "An error occurred: " + status, Toast.LENGTH_SHORT).show();
            }
        });

        filter_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchListActivity.class);
                startActivity(intent);
            }
        });
    }

    public Boolean GPSEnable() {

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        if (gpsTracker.canGetLocation()) {
            return true;

        } else {
            return false;
        }
    }

    public void tunonGps() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(30 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            getcurrentlocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and setting the result in onActivityResult().
                                status.startResolutionForResult(getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }

    }


    public void getcurrentlocation() {

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        //      map types

        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setCompassEnabled(true);
        map.getUiSettings().isMyLocationButtonEnabled();

//        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
//                MarkerOptions markerOptions = new MarkerOptions();
//
//                // Setting the position for the marker
//                markerOptions.position(latLng);
//
//                // Setting the title for the marker.
//                // This will be displayed on taping the marker
//                markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;
                // Clears the previously touched position
                map.clear();

//                // Animating to the touched position
//                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//
//                // Placing a marker on the touched position
//                map.addMarker(markerOptions);
                getDistanceApi(currentLatitude.toString(), currentLongitude.toString());

            }
        });
        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
//                NearbyData nearbyData = (NearbyData) marker.getTag();

                int position = (int) marker.getTag();

//                bottomgrid_listview.smoothScrollToPosition(position);
                bottomgrid_listview.getLayoutManager().scrollToPosition(position);
                bottomgrid_listview.setVisibility(View.VISIBLE);
                bottomgrid_listview.getLayoutManager().scrollToPosition(position);
                footer_search_instruction.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public void multipleMarker(List<NearbyData> list) {
        if (list != null && list.size() > 0) {
            int i = 0;
            for (NearbyData location : list) {

                Double latitude = null;
                Double longitude = null;
                try {
                    latitude = Double.valueOf(location.getLatitude());
                    longitude = Double.valueOf(location.getLongitude());

                    Marker marker = map.addMarker(new MarkerOptions()
                                    .position(new LatLng(latitude, longitude))
//                            .title(location.getName())

                                    .title("$" + location.getPrize())

//                                    .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker_icon))
                                    .icon(BitmapDescriptorFactory.fromBitmap(
                                            createCustomMarker(getActivity(), location.getPrize(), location.getProperty_category())))

//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow_back))
                    );
                    marker.setTag(i);

                } catch (NumberFormatException e) {
                }


//                map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).
//                        icon()).setTitle("iPragmatech Solutions Pvt Lmt");

                CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15);
                map.animateCamera(camera);
                i++;
            }
        }
    }

    public static Bitmap createCustomMarker(Context context, String _name, String category) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

//        CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);
//        markerImage.setImageResource(resource);
        try {
            TextView txt_name = null, txt_name1 = null;
            property_category = jsonObject.getString("category");
            if (category.equals("Rent")) {
                txt_name = (TextView) marker.findViewById(R.id.name);
                txt_name1 = (TextView) marker.findViewById(R.id.name1);
                txt_name1.setText("$" + _name);
                txt_name1.setVisibility(View.VISIBLE);
                txt_name.setVisibility(View.GONE);
            } else if (category.equals("Sale")) {
                txt_name1 = (TextView) marker.findViewById(R.id.name1);
                txt_name = (TextView) marker.findViewById(R.id.name);
                txt_name.setText("$" + _name);
                txt_name.setVisibility(View.VISIBLE);
                txt_name1.setVisibility(View.GONE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        TextView txt_name = (TextView) marker.findViewById(R.id.name);
//        txt_name.setText("$" + _name);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (location == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } else {
                //If everything went fine lets get latitude and longitude
                currentLatitude = location.getLatitude();
                currentLongitude = location.getLongitude();
                if (!currentLatitude.equals(0.0) && !currentLongitude.equals(0.0)) {

//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.title(currentLatitude + " : " + currentLongitude);
//                    map.clear();
//                    map.addMarker(markerOptions);


                    price_min = Paper.book().read("price_min");
                    price_max = Paper.book().read("price_max");
                    beds = Paper.book().read("beds");
                    baths = Paper.book().read("baths");
                    property_type = Paper.book().read("property_type");
                    rent = Paper.book().read("rent");
                    filter = Paper.book().read("filter");
                    neighbourhood = Paper.book().read("neighbourhood");
                    aminities = Paper.book().read("aminities");
                    country_filter = Paper.book().read("country_filter");
                    provience_filter = Paper.book().read("provice_filter");


                    if (filter == null) {

                        getDistanceApi(currentLatitude.toString(), currentLongitude.toString());

                    } else {
                        Paper.book().delete("filter");
                        getproperty(currentLatitude.toString(), currentLongitude.toString());
                    }

                    CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15);
                    map.animateCamera(camera);
                } else {

                    Toast.makeText(getActivity(), getString(R.string.couldnt_get_location), Toast.LENGTH_LONG).show();
                }
            }
        } else {
            askCompactPermissions(permissionAsk, new PermissionResult() {
                @Override
                public void permissionGranted() {

                }

                @Override
                public void permissionDenied() {
                }

                @Override
                public void permissionForeverDenied() {
                    Toast.makeText(getActivity(), R.string.allow_permission, Toast.LENGTH_SHORT).show();
//                    Snackbar.make(rootView, getString(R.string.allow_permission), Snackbar.LENGTH_LONG).show();
                    openSettingsApp(getActivity());
                }
            });
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(getActivity(), "onConnectionSuspended", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
//        Toast.makeText(getActivity(), "onLocationChanged", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//        Toast.makeText(getActivity(), "onConnectionFailed", Toast.LENGTH_SHORT).show();
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void getproperty(final String lat, final String lng) {
        if (beds.equals("0")) {
            beds = "";
        }
        if (baths.equals("0")) {
            baths = "";
        }

        if (aminities == null) {
            aminities = "";
        }

        if (property_type == null) {
            property_type = "";
        }

        final ProgressDialog progressdialog = new ProgressDialog(getActivity());
        progressdialog.setIndeterminate(true);
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        final ArrayList<String> amenitieslist = new ArrayList<>();
        progressdialog.show();
        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/search", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<NearbyData> nearbyData = new ArrayList<NearbyData>();
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("success");
                    if (status == 1) {
//                        Paper.book().delete("price_min");
//                        Paper.book().delete("price_max");
//                        Paper.book().delete("beds");
//                        Paper.book().delete("property_type");
//                        Paper.book().delete("baths");
//                        Paper.book().delete("rent");
//                        Paper.book().delete("neighbourhood");
//                        Paper.book().delete("filter");
//                        Paper.book().delete("aminities");
//                        Paper.book().delete("country_filter");
//                        Paper.book().delete("provice_filter");

                        progressdialog.dismiss();
                        objUser = object.getJSONArray("data");
                        for (int i = 0; i < objUser.length(); i++) {
                            jsonObject = objUser.getJSONObject(i);
                            int id = jsonObject.getInt("id");
                            String post_unit = jsonObject.getString("post_unit");
                            String coordinates_tag = jsonObject.getString("coordinates_tag");
                            String bed = jsonObject.getString("bed");
                            String area = jsonObject.getString("land_area");
                            String bath = jsonObject.getString("bath");
                            String floor = jsonObject.getString("floor");
//                            String address = jsonObject.getString("address");
                            String street = jsonObject.getString("streat");
                            String city = jsonObject.getString("city");
                            String province = jsonObject.getString("province");
                            String country = jsonObject.getString("country");
                            String image1 = jsonObject.getString("image1");
                            String image2 = jsonObject.getString("image2");
                            String image3 = jsonObject.getString("image3");
                            String image4 = jsonObject.getString("image4");
                            String image5 = jsonObject.getString("image5");
                            String image6 = jsonObject.getString("image6");
                            String year_of_construction = jsonObject.getString("year_of_construction");
                            String price_unit = jsonObject.getString("price_unit");
                            String ad_title = jsonObject.getString("title");
                            String area_unit = jsonObject.getString("area_unit");
                            String description = jsonObject.getString("body");
                            String offer_price = jsonObject.getString("price");
                            String property_pictures = jsonObject.getString("image1");
                            String full_name = jsonObject.getString("full_name");
                            String property_type = jsonObject.getString("property_type");
                            String property_category = jsonObject.getString("category");
                            String lat = "", log = "";

                            JSONArray amenitiesarray = jsonObject.getJSONArray("aminties");
                            String amnt = amenitiesarray.get(0).toString();

                            for (int loop = 0; loop < amenitiesarray.length(); loop++) {
                                amenitieslist.add(amenitiesarray.get(loop).toString());
                            }
                            String addresss = street + "," + city + "," + province + "," + country;

                            if (!coordinates_tag.equals("")) {
                                String[] separated = coordinates_tag.split(",");
                                lat = separated[0];
                                log = separated[1];
                                nearbyData.add(new NearbyData(id, post_unit, lat, log, offer_price, bed, area, bath, floor, addresss, area_unit, full_name, description, property_type, property_category, property_pictures, ad_title, price_unit, year_of_construction, image1, image2, image3, image4, image5, image6, amenitieslist));
                            }
                        }

                        multipleMarker(nearbyData);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                        bottomgrid_listview.setLayoutManager(mLayoutManager);
                        bottomgrid_listview.setItemAnimator(new DefaultItemAnimator());
                        bottomgrid_listview.setVisibility(View.GONE);
                        scrollRecyclerAdapter = new HomeScrollRecyclerAdapter(getActivity(), nearbyData);
                        bottomgrid_listview.setAdapter(scrollRecyclerAdapter);
                        scrollRecyclerAdapter.onItemClickListner(new HomeScrollRecyclerAdapter.onclickListner() {
                            @Override
                            public void itemClick(NearbyData object) {
//                                Toast.makeText(getActivity(), "itemClick test", Toast.LENGTH_SHORT).show();
                                ArrayList<String> amenties = object.getAmenitieslist();
                                Paper.book().write("object", object);
                                Paper.book().write("amenties", amenties);
                                DetailPreviewFragment searchFragment = new DetailPreviewFragment();
                                openFragment(searchFragment);
//                                Intent intent = new Intent(getActivity(), DetailPreviewActivity.class);
//                                startActivity(intent);
                            }
                        });
                        progressdialog.dismiss();

                    } else {
                        String msg = object.getString("message");
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (this != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("do", "search");
                params.put("apikey", "mtechapi12345");
                params.put("lat", lat);
                params.put("lng", lng);
                params.put("max_price", price_max);
                params.put("min_price", price_min);
                params.put("bed", beds);
                params.put("bath", baths);
                params.put("property_type", property_type);
                params.put("aminities", aminities);
                params.put("neighborhood", neighbourhood);
//                params.put("bath", baths);
                params.put("id", String.valueOf(Utilities.getInt(getActivity(), "user_id")));

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);
    }

    public void getDistanceApi(final String lat, final String lng) {
        final ArrayList<String> amenitieslist = new ArrayList<>();
        Utilities.showProgressDialog(getActivity(), "please wait");
//        final ArrayList<String> amenitieslist = new ArrayList<>();
        Map<String, String> params = new HashMap<String, String>();
        params.put("do", "get_distance");
        params.put("apikey", "travces.com");
        params.put("lat", lat);
        params.put("lng", lng);
        params.put("id", String.valueOf(Utilities.getInt(getActivity(), "user_id")));
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST, "http://spatiopro.com/api/get_distance", getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        ArrayList<NearbyData> nearbyData = new ArrayList<NearbyData>();
                        JSONObject object = new JSONObject(String.valueOf(result));
                        int status = object.getInt("success");
                        if (status == 1) {
                            Utilities.hideProgressDialog();
                            objUser = object.getJSONArray("data");
                            for (int i = 0; i < objUser.length(); i++) {
                                jsonObject = objUser.getJSONObject(i);
                                int id = jsonObject.getInt("id");
                                String post_unit = jsonObject.getString("post_unit");
                                String coordinates_tag = jsonObject.getString("coordinates_tag");
                                String bed = jsonObject.getString("bed");
                                String area = jsonObject.getString("land_area");
                                String bath = jsonObject.getString("bath");
                                String floor = jsonObject.getString("floor");
                                String image1 = jsonObject.getString("image1");
                                String image2 = jsonObject.getString("image2");
                                String image3 = jsonObject.getString("image3");
                                String image4 = jsonObject.getString("image4");
                                String image5 = jsonObject.getString("image5");
                                String image6 = jsonObject.getString("image6");
                                String year_of_construction = jsonObject.getString("year_of_construction");


//                                String address = jsonObject.getString("address");
                                String street = jsonObject.getString("streat");
                                String price_unit = jsonObject.getString("price_unit");
                                String city = jsonObject.getString("city");
                                String province = jsonObject.getString("province");
                                String country = jsonObject.getString("country");
                                String ad_title = jsonObject.getString("title");
                                String area_unit = jsonObject.getString("area_unit");
                                String description = jsonObject.getString("body");
                                String offer_price = jsonObject.getString("price");
                                String property_pictures = jsonObject.getString("image1");
                                String full_name = jsonObject.getString("full_name");
                                String property_type = jsonObject.getString("property_type");
                                property_category = jsonObject.getString("category");
                                createCustomMarker(getActivity(), offer_price, property_category);
                                String lat = "", log = "";
                                String addresss = street + "," + city + "," + province + "," + country;
                                JSONArray amenitiesarray = jsonObject.getJSONArray("aminties");
                                String amnt = amenitiesarray.get(0).toString();
                                Utilities.saveString(getActivity(), "amentiesss", amnt);
                                for (int loop = 0; loop < amenitiesarray.length(); loop++) {
                                    amenitieslist.add(amenitiesarray.get(loop).toString());
//                                    Toast.makeText(getActivity(), String.valueOf(amnt), Toast.LENGTH_SHORT).show();
                                }
                                if (!coordinates_tag.equals("")) {
                                    String[] separated = coordinates_tag.split(",");
                                    lat = separated[0];
                                    log = separated[1];
                                    nearbyData.add(new NearbyData(id, post_unit, lat, log, offer_price, bed, area, bath, floor, addresss, area_unit, full_name, description, property_type, property_category, property_pictures, ad_title, price_unit, year_of_construction, image1, image2, image3, image4, image5, image6, amenitieslist));

                                    Utilities.saveString(getActivity(), "nearbyDataList", nearbyData.toString());
                                }
                            }

                            Paper.book().write("nearbyDataList", nearbyData);
                            multipleMarker(nearbyData);

                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                            bottomgrid_listview.setLayoutManager(mLayoutManager);
                            bottomgrid_listview.setItemAnimator(new DefaultItemAnimator());
                            bottomgrid_listview.setVisibility(View.GONE);
                            scrollRecyclerAdapter = new HomeScrollRecyclerAdapter(getActivity(), nearbyData);
                            bottomgrid_listview.setAdapter(scrollRecyclerAdapter);
                            scrollRecyclerAdapter.onItemClickListner(new HomeScrollRecyclerAdapter.onclickListner() {
                                @Override
                                public void itemClick(NearbyData object) {
//                                Toast.makeText(getActivity(), "itemClick test", Toast.LENGTH_SHORT).show();
                                    Paper.book().write("object", object);
//                                    Toast.makeText(getContext(), String.valueOf(object.getAmenitieslist()), Toast.LENGTH_SHORT).show();
                                    DetailPreviewFragment searchFragment = new DetailPreviewFragment();
                                    openFragment(searchFragment);
//                                    startActivity(new Intent(getActivity(), DetailPreviewActivity.class));
                                }
                            });
//                            progressdialog.dismiss();
                            Utilities.hideProgressDialog();

                        } else {

                            String msg = object.getString("message");
                            Utilities.hideProgressDialog();
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
