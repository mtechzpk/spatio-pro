package com.example.realestate.Fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.FilterActivity;
import com.example.realestate.Activity.LandingActivity;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment {
    Fragment frg = null;

    private Spinner sp_property_type, sp_rent_or_sale, sp_aminities;
    private ImageView go_back;
    private EditText no_min, no_max, neighbourhood,contry_filter,province_filter;
    private TextView number_beds, number_baths, minus1, minus2, plus1, plus2, btn_search, reset;
    private String price_min = "", price_max = "", property_type = "", aminities = "",
            beds = "", baths = "", rent = "", neighbourhood_st = "",country_st = "",provience_st = "", search_type = "";
    private int bed_no = 0;
    private int bath_no = 0;
    private LinearLayout others, bed_search, price_search;
    private List<String> prop_type, aminities_type;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
View view;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FilterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FilterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FilterFragment newInstance(String param1, String param2) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.activity_filter, container, false);
        Paper.init(getContext());
//        Intent intent = getActivity().getIntent();
//        search_type = intent.getStringExtra("search_type");
        search_type= Utilities.getString(getContext(),"search_type");
        initViews();
        getprop_type();
        getamenities();
        ClickViews();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.rent_or_sale, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_rent_or_sale.setAdapter(adapter);
        return view;
    }

    private void initViews() {

        sp_rent_or_sale = view.findViewById(R.id.rent_or_sale);
        sp_property_type =view.findViewById(R.id.sp_property_type);
        sp_aminities = view.findViewById(R.id.sp_aminities);
        go_back = view.findViewById(R.id.go_back);

        others = view.findViewById(R.id.others);
        bed_search = view.findViewById(R.id.bed_search);
        price_search =view. findViewById(R.id.price_search);

        btn_search =view. findViewById(R.id.btn_search);

        no_min = view.findViewById(R.id.no_min);
        no_max = view.findViewById(R.id.no_max);
        neighbourhood =view. findViewById(R.id.neighbourhood);
        province_filter =view. findViewById(R.id.province_filter);
        contry_filter = view.findViewById(R.id.contry_filter);
        number_beds = view.findViewById(R.id.number_beds);
        number_baths = view.findViewById(R.id.number_baths);
        minus1 = view.findViewById(R.id.minus1);
        minus2 =view. findViewById(R.id.minus2);
        plus1 = view.findViewById(R.id.plus1);
        plus2 = view.findViewById(R.id.plus2);

        reset =view. findViewById(R.id.reset);
    }

    private void ClickViews() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LandingActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                price_min = no_min.getText().toString().trim();
                price_max = no_max.getText().toString().trim();
                neighbourhood_st = neighbourhood.getText().toString().trim();
                country_st = contry_filter.getText().toString().trim();
                provience_st = province_filter.getText().toString().trim();
                beds = String.valueOf(bed_no);
                baths = String.valueOf(bath_no);
                property_type = sp_property_type.getSelectedItem().toString();
                aminities = sp_aminities.getSelectedItem().toString();
                rent = sp_rent_or_sale.getSelectedItem().toString();

                if (rent.equals("Select Category")) {
                    rent = "";
                } else {
                    Paper.book().write("rent", rent);
                }
                if (property_type.equals("Select Property Type")) {
                    property_type = "";
                } else {
                    Paper.book().write("property_type", property_type);
                }
                if (aminities.equals("Select Amenities")) {
                    aminities = "";
                } else {
                    Paper.book().write("aminities", aminities);
                }
                Paper.book().write("price_min", price_min);
                Paper.book().write("price_max", price_max);
                Paper.book().write("beds", beds);
                Paper.book().write("baths", baths);

                Paper.book().write("neighbourhood", neighbourhood_st);
                Paper.book().write("country_filter", country_st);
                Paper.book().write("provice_filter", provience_st);

                String filter = "filterScreen";
                Paper.book().write("filter", filter);


                Intent intent = new Intent(getActivity(), LandingActivity.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

        plus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bed_no = bed_no + 1;
                number_beds.setText("" + bed_no);

            }
        });

        minus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bed_no < 0) {
                    bed_no = 0;
                    number_beds.setText(bed_no + "");
                }
                if (bed_no > 0) {
                    bed_no = bed_no - 1;
                    number_beds.setText(bed_no + "");
                }
            }
        });

        plus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bath_no = bath_no + 1;
                number_baths.setText("" + bath_no);

            }
        });

        minus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bath_no < 0) {
                    bath_no = 0;
                    number_baths.setText(bath_no + "");
                }
                if (bath_no > 0) {
                    bath_no = bath_no - 1;
                    number_baths.setText(bath_no + "");
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent1 = getActivity().getIntent();
//                getActivity().finish();
//                startActivity(intent1);
                refreshFrag();


            }
        });


        if (search_type.equals("price")) {

            others.setVisibility(View.GONE);
            bed_search.setVisibility(View.GONE);

        } else if (search_type.equals("beds")) {
            price_search.setVisibility(View.GONE);
            others.setVisibility(View.GONE);
        } else {
            others.setVisibility(View.VISIBLE);
            bed_search.setVisibility(View.VISIBLE);
            price_search.setVisibility(View.VISIBLE);
        }

    }
public void refreshFrag(){
    getFragmentManager().beginTransaction().detach(this).attach(this).commit();

}
    private void getprop_type() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/property_type", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    String status = object.getString("Success");
                    if (status.equals("1")) {


                        prop_type = new ArrayList<>();
                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {


                            JSONObject object1 = jsonArray.getJSONObject(i);
                            String namee = object1.getString("property_type");


                            prop_type.add(namee);
                        }




                        String myString = "Select Property Type";
                        prop_type.add(myString);


                        // Creating adapter for spinner
                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, prop_type);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_property_type.setAdapter(dataAdapter);

                        dataAdapter = (ArrayAdapter) sp_property_type.getAdapter();
                        int spinnerPosition = dataAdapter.getPosition(myString);
                        sp_property_type.setSelection(spinnerPosition);

                        sp_property_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                                Utilities.saveString(FilterActivity.this, "prop_fect", parent.getItemAtPosition(position).toString());

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    } else {
//                        String message = object.getString("message");
                        Toast.makeText(getActivity(), "result not fount", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
                params.put("do", "property_type");
                params.put("apikey", "travces.com");
                params.put("property_type", property_type);

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);


    }
    private void getamenities() {


        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST,"http://spatiopro.com/api/amenities", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject object = new JSONObject(response);
                    int status = object.getInt("Success");
                    if (status==1) {


                        JSONArray jsonArray = object.getJSONArray("data");



                        aminities_type = new ArrayList<>();
                        JSONArray jsonArray1 = object.getJSONArray("data");
                        for (int j = 0; j < jsonArray1.length(); j++) {
                            JSONObject object2 = jsonArray.getJSONObject(j);
                            String nameee = object2.getString("aminties");


                            aminities_type.add(nameee);
                        }


                        String myString = "Select Amenities";

                        aminities_type.add(myString);

                        // Creating adapter for spinner


                        ArrayAdapter<String> aminitiesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, aminities_type);
                        aminitiesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        sp_aminities.setAdapter(aminitiesAdapter);

                        aminitiesAdapter = (ArrayAdapter) sp_aminities.getAdapter();
                        int spinnerPos = aminitiesAdapter.getPosition(myString);
                        sp_aminities.setSelection(spinnerPos);

                        sp_aminities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

//                                Utilities.saveString(FilterActivity.this, "aminity_fect", parent.getItemAtPosition(position).toString());

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    } else {
//                        String message = object.getString("message");
                        Toast.makeText(getActivity(), "Result not found", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getActivity() != null)
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("token", token);
//                params.put("do", "amenities");
//                params.put("apikey", "mtechapi12345");


                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getActivity()).addToRequestQueue(RegistrationRequest);


    }
}