package com.example.realestate.Fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.realestate.Activity.help.DisclaimerActivity;
import com.example.realestate.Activity.help.DisclaimerFragment;
import com.example.realestate.Activity.help.PrivacyActivity;
import com.example.realestate.Activity.help.PrivacyFragment;
import com.example.realestate.Activity.help.TermsActivity;
import com.example.realestate.Activity.help.TermsFragment;
import com.example.realestate.R;

public class AboutFragment extends Fragment {
    private View view;
    private TextView back_to_more, privacy, disclaimer, terms;
    boolean openedFragment = false;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_about, container, false);
        initViews();
        clickViews();


        return view;
    }

    private void initViews() {
        back_to_more = view.findViewById(R.id.back_to_more);
        privacy = view.findViewById(R.id.privacy);
        disclaimer = view.findViewById(R.id.disclaimer);
        terms = view.findViewById(R.id.terms);
    }

    private void clickViews() {
        back_to_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment newFragment = new MoreFragment();
                openFragment(newFragment);
                openedFragment = true;

            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new PrivacyFragment();
                openFragment(newFragment);
                openedFragment = true;

            }
        });

        disclaimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getContext(), DisclaimerActivity.class));
                Fragment newFragment = new DisclaimerFragment();
                openFragment(newFragment);
                openedFragment = true;
//                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
//                alertDialog.setTitle("Disclaimer");
//                alertDialog.setMessage("Disclaimer usage conditions will be shown here.");
//                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                alertDialog.setCanceledOnTouchOutside(false);
//                alertDialog.show();

            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new TermsFragment();
                openFragment(newFragment);
                openedFragment = true;
//                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
//                alertDialog.setTitle("Terms");
//                alertDialog.setMessage("Terms of usage conditions will be shown here.");
//                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                alertDialog.setCanceledOnTouchOutside(false);
//                alertDialog.show();

            }
        });

    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
