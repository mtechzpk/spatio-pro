package com.example.realestate.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.example.realestate.Activity.PostServiceActivities.SavedPropertyActvity;
import com.example.realestate.Adapters.SavedPropertiesAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.R;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.example.realestate.dialogs.SavedpropropertiesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import libs.mjn.prettydialog.PrettyDialog;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SavedPropertyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SavedPropertyFragment extends Fragment {
    int user_id;
    private RecyclerView recyclerView, rvProfilePost;
    private SavedPropertiesAdapter pAdapter;
    private ArrayList<SavedpropropertiesModel> savedpropropertiesModels;
    View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SavedPropertyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SavedPropertyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SavedPropertyFragment newInstance(String param1, String param2) {

        SavedPropertyFragment fragment = new SavedPropertyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_saved_property, container, false);
        user_id= Utilities.getInt(getActivity(),"user_id");
        recyclerView=view.findViewById(R.id.rv_poemName);
        getCategoriesApi();
        return view;
    }

    public void getCategoriesApi() {
        Utilities.showProgressDialog(getActivity(), "please wait");
        Map<String, String> params = new HashMap<String, String>();
        params.put("user_id", String.valueOf(user_id));
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");


        ApiModelClass.GetApiResponse(Request.Method.POST,"http://spatiopro.com/api/my_property",getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        savedpropropertiesModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("success");
//                        String Message = jsonObject.getString("Message");
                        if (status == 1) {
                            final JSONArray objUser = object.getJSONArray("data");

                            for (int i = 0; i < objUser.length(); i++) {
//
                                JSONObject jsonObject = objUser.getJSONObject(i);
                                String title = jsonObject.getString("title");
                                int id = jsonObject.getInt("id");
                                String price = jsonObject.getString("price");

                                String price_unit = jsonObject.getString("price_unit");
                                String post_unit = jsonObject.getString("post_unit");
                                String country = jsonObject.getString("naighberhood");
                                String province = jsonObject.getString("province");
                                String streat = jsonObject.getString("property_feature");
                                String building_no = jsonObject.getString("building_no");
                                String bed = jsonObject.getString("bed");
                                String bath = jsonObject.getString("bath");
                                String floor = jsonObject.getString("floor");
                                String total_floor = jsonObject.getString("total_floor");
                                String email = jsonObject.getString("email");
                                String city = jsonObject.getString("property_type");
                                String category = jsonObject.getString("category");
                                String full_name = jsonObject.getString("full_name");
                                String phone_number = jsonObject.getString("phone_number");
                                String address = jsonObject.getString("address");
                                String user_image1 = jsonObject.getString("user_image");
                                String user_image = jsonObject.getString("image1");
//                            Utilities.saveString(getContext(), "name", name);
//                            Utilities.saveString(getContext(), "follow_id", follow_id);
//                            String noOfQ = jsonObject.getString("number_of_persons");

//                            Utilities.saveString(getContext(), "clerkQListId", clerkQListId);
//                            Utilities.saveString(getContext(), "noOfPerson", noOfQ);

                                savedpropropertiesModels.add(new SavedpropropertiesModel(id,post_unit,country,city,building_no,address,phone_number,price,title,email,bed,bath,floor,streat,user_image,full_name,total_floor,category,price_unit));

                            }
                            pAdapter = new SavedPropertiesAdapter(getActivity(), savedpropropertiesModels);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setAdapter(pAdapter);


                        } else {

                            if (status == 0) {
                                String error = object.getString("message");
                                Utilities.hideProgressDialog();
                                new PrettyDialog(getActivity())
                                        .setTitle(error)
                                        .setIcon(R.drawable.pdlg_icon_info)
                                        .setIconTint(R.color.colorPrimary)
//                .setMessage("PrettyDialog Message")
                                        .show();
                            }
                        }
//

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
}