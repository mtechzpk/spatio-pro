package com.example.realestate.Fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.realestate.Activity.AddPostActivities.ContinueActivity;
import com.example.realestate.Activity.AddPostActivities.PropertyCategoryActivity;
import com.example.realestate.Activity.LoginDetail.LoginActivity;
import com.example.realestate.Adapters.NotificationAdapter;
import com.example.realestate.Adapters.PropertyTypeAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.NotificationModel;
import com.example.realestate.Models.PropertyTypeModel;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewsFragment extends Fragment {

    View view;
    private RecyclerView rv_notification;

    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);
        rv_notification = view.findViewById(R.id.rv_notification);

        NotificationApiCall();
        return view;
    }

    private void NotificationApiCall() {

        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(getContext());
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, Server.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    ArrayList<NotificationModel> notificationModels = new ArrayList<NotificationModel>();
//                    JSONObject object = new JSONObject(response);

                    JSONObject object = new JSONObject(response);
                    String status = object.getString("success");
                    if (status.equals("1")) {
                        JSONArray objUser = object.getJSONArray("result");
                        for (int i = 0; i < objUser.length(); i++) {
                            JSONObject jsonObject = objUser.getJSONObject(i);
                            String id = jsonObject.getString("type");
                            String name = jsonObject.getString("notifications");
                            notificationModels.add(new NotificationModel(id, name));
                        }

                        NotificationAdapter pAdapter = new NotificationAdapter(getActivity(), notificationModels);
                        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 1, RecyclerView.VERTICAL, false);
                        rv_notification.setLayoutManager(layoutManager);
                        rv_notification.setAdapter(pAdapter);
//                        rvChannels.getRecycledViewPool().clear();
//                        pAdapter.notifyDataSetChanged();


                    } else {
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressdialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getContext() != null)
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                params.put("email", email);
//                params.put("password", password);
                //                params.put("token", token);
                params.put("do", "get-notifications");
                params.put("apikey", "mtechapi12345");

                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getContext()).addToRequestQueue(RegistrationRequest);


    }

}
