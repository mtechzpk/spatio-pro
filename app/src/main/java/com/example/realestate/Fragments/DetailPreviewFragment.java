package com.example.realestate.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.denzcoskun.imageslider.ImageSlider;
import com.denzcoskun.imageslider.constants.ScaleTypes;
import com.denzcoskun.imageslider.models.SlideModel;
import com.example.realestate.Activity.DetailPreviewActivity;
import com.example.realestate.Adapters.GetAmentiesAdapter;
import com.example.realestate.Adapters.RecyclerViewAdapter;
import com.example.realestate.Extras.Utilities;
import com.example.realestate.Models.GetAmentiesModel;
import com.example.realestate.Models.NearbyData;
import com.example.realestate.R;
import com.example.realestate.Server.MySingleton;
import com.example.realestate.Server.Server;
import com.example.realestate.api.ApiModelClass;
import com.example.realestate.api.ServerCallback;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.paperdb.Paper;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailPreviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailPreviewFragment extends Fragment {
    private GetAmentiesAdapter pAdapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    private TextView prop_categ1, description, prop_categ, price_text, propertId, bed_text, address_text, covered_area_text, price_per_sqft_text, carpet_area_text, location_text,
            config_text, status_text, propertyType, floor_text, tvYearofConstructins;
    String[] numbers = {"facility of water", "parking space", "Furnished",};
    private RecyclerView recyclerView;
    private Context context;
    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    ImageView ivBack;
    ImageSlider image_slider;
    private ArrayList<GetAmentiesModel> getAmentiesModels;
    private NearbyData object;
//    private ImageView img_main;
    private TextInputEditText contact_name, contact_email, contact_phone;
    private EditText contact_description;
    private Button contact_btn;
    TextView title;
    private String nam = "", mail = "", cntct = "", query = "";
    View view;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DetailPreviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailPreviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailPreviewFragment newInstance(String param1, String param2) {
        DetailPreviewFragment fragment = new DetailPreviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.activity_detail_preview, container, false);
        Paper.init(getContext());
        object = Paper.book().read("object");

        initViews();
        ToolbarMethod();
        ClickViews();
        getAmentiesApi();
        return view;
    }

    private void initViews() {
        context = getApplicationContext();
//        toolbar = view.findViewById(R.id.toolBar);
        recyclerView = view.findViewById(R.id.rv_aminities);
        ivBack = view.findViewById(R.id.ivBack);
        image_slider = view.findViewById(R.id.image_slider);
//        collapsingToolbar = view.findViewById(R.id.collapsing_toolbar);

        price_text = view.findViewById(R.id.price_text);
        tvYearofConstructins = view.findViewById(R.id.tvYearofConstructins);
        title = view.findViewById(R.id.title);
        bed_text = view.findViewById(R.id.bed_text);
        address_text = view.findViewById(R.id.address_text);
        covered_area_text = view.findViewById(R.id.covered_area_text);
        prop_categ1 = view.findViewById(R.id.prop_categ1);
        prop_categ = view.findViewById(R.id.prop_categ);
        description = view.findViewById(R.id.description);
//        price_per_sqft_text = findViewById(R.id.price_per_sqft_text);
//        carpet_area_text = findViewById(R.id.carpet_area_text);
        location_text = view.findViewById(R.id.location_text);
        propertId = view.findViewById(R.id.propertId);
        propertyType = view.findViewById(R.id.propertyType);
        config_text = view.findViewById(R.id.config_text);
        status_text = view.findViewById(R.id.status_text);
        floor_text = view.findViewById(R.id.floor_text);
//        img_main = view.findViewById(R.id.img_main);

        contact_name = view.findViewById(R.id.contact_name);
        contact_email = view.findViewById(R.id.contact_email);
        contact_phone = view.findViewById(R.id.contact_phone);
        contact_description = view.findViewById(R.id.contact_description);

        contact_btn = view.findViewById(R.id.contact_btn);
        ivBack.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_detailPreviewFragment_to_searchFragment));
        List<SlideModel> model = new ArrayList<>();

//        String image1 =  object.getImage1();
        String image3 = object.getImage3();
        String image4 =object.getImage4();
        String image5 = object.getImage5();
        String image2 =  object.getImage2();
        String image6 =  object.getImage6();
        String image7 =  object.getProperty_pictures();


//        if (!image1.equals("http://spatiopro.com/public")){
//            model.add(new SlideModel(image1, ScaleTypes.CENTER_CROP));
//        }

        if (!image7.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image7, ScaleTypes.CENTER_CROP));
        }
        if (!image2.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image2,ScaleTypes.CENTER_CROP));
        }
        if (!image3.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image3,ScaleTypes.CENTER_CROP));
        }
        if (!image4.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image4,ScaleTypes.CENTER_CROP));
        }
        if (!image5.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image5,ScaleTypes.CENTER_CROP));
        }
        if (!image6.equals("http://spatiopro.com/public")){
            model.add(new SlideModel(image6,ScaleTypes.CENTER_CROP));
        }

        image_slider.setImageList(model);


    }

    private void ToolbarMethod() {
        price_text.setText(object.getPrice_unit() + "-" + object.getPrize());
        title.setText("Title: " + object.getAd_title());
        address_text.setText(object.getAddress());
        description.setText(object.getDescription());
        covered_area_text.setText(object.getArea() + " " + object.getArea_unit());
        location_text.setText(object.getAddress());
        config_text.setText(object.getBed() + getString(R.string.beds) + " " + object.getBath() + " " + getString(R.string.bath) + " ");
        floor_text.setText(object.getFloor() + " " + getString(R.string.floor_text));
        propertId.setText("sp-" + object.getPost_unit() + "-" + object.getId());
        tvYearofConstructins.setText("Year of Construction: " + "\n" + object.getYear_of_construction());
        propertyType.setText(object.getProperty_type());

        if (object.getProperty_category().equals("Sale")) {
            prop_categ1.setText(object.getProperty_category());
            prop_categ1.setVisibility(View.VISIBLE);
            prop_categ.setVisibility(View.GONE);

        } else if (object.getProperty_category().equals("Rent")) {
            prop_categ.setText(object.getProperty_category());
            prop_categ.setVisibility(View.VISIBLE);
            prop_categ1.setVisibility(View.GONE);
        }

        prop_categ1.setText("On " + object.getProperty_category());
//        Picasso.get()
//                .load(object.getProperty_pictures())
//                .fit()
//                .centerInside()
//                .placeholder(R.drawable.bgg)
//                .into(img_main);

    }

    public void getAmentiesApi() {
        Utilities.showProgressDialog(getActivity(), "loading");
        Map<String, String> params = new HashMap<String, String>();
        params.put("property_id", String.valueOf(object.getId()));
        HashMap<String, String> headers = new HashMap<String, String>();
//        headers.put("Accept", "application/json");
        ApiModelClass.GetApiResponse(Request.Method.POST, Server.property_aminties, getActivity(), params, headers, new ServerCallback() {
            @Override
            public void onSuccess(JSONObject result, String ERROR) {

                if (ERROR.isEmpty()) {

                    Utilities.hideProgressDialog();

                    try {
                        getAmentiesModels = new ArrayList<>();
                        JSONObject object = new JSONObject(String.valueOf(result));

                        int status = object.getInt("Success");

                        if (status == 1) {
                            Utilities.hideProgressDialog();

                            final JSONArray objUser = object.getJSONArray("data");
                            for (int i = 0; i < objUser.length(); i++) {
                                JSONObject jsonObject = objUser.getJSONObject(i);

                                String amenties = jsonObject.getString("aminties");
                                getAmentiesModels.add(new GetAmentiesModel(amenties));
                            }
                            pAdapter = new GetAmentiesAdapter(getContext(), getAmentiesModels);
                            GridLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
                            recyclerView.setLayoutManager(linearLayoutManager);
                            recyclerView.setAdapter(pAdapter);


                        } else {
                            Utilities.hideProgressDialog();

                        }
//

                    } catch (JSONException e) {
                        Utilities.hideProgressDialog();
                        e.printStackTrace();
                    }


                } else {
                    Utilities.hideProgressDialog();
                    Toast.makeText(getActivity(), ERROR, Toast.LENGTH_SHORT).show();

                }
            }
        });

    }

    private void ClickViews() {
        contact_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nam = contact_name.getText().toString().trim();
                mail = contact_email.getText().toString().trim();
                cntct = contact_phone.getText().toString().trim();
                query = contact_description.getText().toString().trim();

                if (!nam.isEmpty()) {
                    if (!mail.isEmpty()) {
                        if (!cntct.isEmpty()) {
                            if (!query.isEmpty()) {
                                ContactApiCall();
                            } else {
                                contact_description.setError("Write some description please.");
                            }
                        } else {
                            contact_email.setError("Phone Required");
                        }
                    } else {
                        contact_email.setError("Email Required");
                    }
                } else {
                    contact_name.setError("Name Required");
                }

            }
        });
    }


    private void ContactApiCall() {
        final ProgressDialog progressdialog;
        progressdialog = new ProgressDialog(getContext());
        progressdialog.setMessage("Please wait..");
        progressdialog.setCancelable(false);
        progressdialog.show();

        final StringRequest RegistrationRequest = new StringRequest(Request.Method.POST, "http://spatiopro.com/api/sendemail/send",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject object = new JSONObject(response);
                            int status = object.getInt("success");
                            if (status == 1) {
                                String Message = object.getString("message");
                                Utilities.hideProgressDialog();
                                Toast.makeText(getContext(), Message, Toast.LENGTH_SHORT).show();

                            } else {
                                Utilities.hideProgressDialog();
                                String message = object.getString("message");
                                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressdialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressdialog.dismiss();
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (volleyError instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }
                if (getContext() != null)
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", nam);
                params.put("provider_email", "hamza.mughal5593@gmail.com");
                params.put("phone", cntct);
                params.put("user_email", mail);
                params.put("message", query);
                return params;
            }
        };

        RegistrationRequest.setRetryPolicy(new DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MySingleton.getInstance(getContext()).addToRequestQueue(RegistrationRequest);

    }
}