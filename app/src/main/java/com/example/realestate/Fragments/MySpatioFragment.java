package com.example.realestate.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.realestate.Activity.PostServiceActivities.SavedPropertyActvity;
import com.example.realestate.Activity.help.PrivacyFragment;
import com.example.realestate.R;

public class MySpatioFragment extends Fragment {

    View view;
    RelativeLayout rl_savedProperty;
    boolean openedFragment = false;

    public MySpatioFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_spatio, container, false);
        rl_savedProperty = view.findViewById(R.id.rl_savedProperty);
        rl_savedProperty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), SavedPropertyActvity.class));
                Fragment newFragment = new SavedPropertyFragment();
                openFragment(newFragment);
                openedFragment = true;
            }
        });
        return view;
    }

    private void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
