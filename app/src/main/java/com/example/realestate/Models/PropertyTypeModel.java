package com.example.realestate.Models;

public class PropertyTypeModel {
    int id = 0;
    String propertytype = "";

    public PropertyTypeModel(int id, String propertytype) {
        this.id = id;
        this.propertytype = propertytype;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPropertytype() {
        return propertytype;
    }

    public void setPropertytype(String propertytype) {
        this.propertytype = propertytype;
    }
}
