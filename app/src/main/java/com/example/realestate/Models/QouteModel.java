package com.example.realestate.Models;

import java.util.ArrayList;

public class QouteModel {
    String c_name = "";
    String service_type = "";
    String address = "";
    String register_type = "";
    String person_name = "";
    String description = "";
    String certifiation = "";
    String email= "";
    String provide_name= "";
    String ivProfile;
    String image1,image2,image3,image4,image5,image6,service_attention;
    ArrayList<String> service_options = new ArrayList<>();

    public QouteModel(String c_name, String service_type, String address, String register_type, String person_name, String description, String certifiation, String email, String provide_name, String ivProfile, String image1, String image2, String image3, String image4, String image5, String image6, String service_attention, ArrayList<String> service_options) {
        this.c_name = c_name;
        this.service_type = service_type;
        this.address = address;
        this.register_type = register_type;
        this.person_name = person_name;
        this.description = description;
        this.certifiation = certifiation;
        this.email = email;
        this.provide_name = provide_name;
        this.ivProfile = ivProfile;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.image4 = image4;
        this.image5 = image5;
        this.image6 = image6;
        this.service_attention = service_attention;
        this.service_options = service_options;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage6() {
        return image6;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    public String getService_attention() {
        return service_attention;
    }

    public void setService_attention(String service_attention) {
        this.service_attention = service_attention;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getIvProfile() {
        return ivProfile;
    }

    public void setIvProfile(String ivProfile) {
        this.ivProfile = ivProfile;
    }

    public String getProvide_name() {
        return provide_name;
    }

    public void setProvide_name(String provide_name) {
        this.provide_name = provide_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getService_type() {
        return service_type;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRegister_type() {
        return register_type;
    }

    public void setRegister_type(String register_type) {
        this.register_type = register_type;
    }

    public String getPerson_name() {
        return person_name;
    }

    public void setPerson_name(String person_name) {
        this.person_name = person_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCertifiation() {
        return certifiation;
    }

    public void setCertifiation(String certifiation) {
        this.certifiation = certifiation;
    }

    public ArrayList<String> getService_options() {
        return service_options;
    }

    public void setService_options(ArrayList<String> service_options) {
        this.service_options = service_options;
    }
}
