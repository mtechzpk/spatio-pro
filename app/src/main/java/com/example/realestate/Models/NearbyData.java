package com.example.realestate.Models;

import java.util.ArrayList;

public class NearbyData {
    int id;
    String post_unit;
    String latitude;
    String longitude;
    String prize;
    String bed;
    String area;
    String bath;
    String floor;
    String address;
    String area_unit;
    String full_name;
    String description;
    String property_type;
    String property_category;
    String property_pictures;
    String ad_title,price_unit,year_of_construction,image1,image2,image3,image4,image5,image6;
    ArrayList<String> amenitieslist = new ArrayList<>();

    public NearbyData(int id, String post_unit, String latitude, String longitude, String prize, String bed, String area, String bath, String floor, String address, String area_unit, String full_name, String description, String property_type, String property_category, String property_pictures, String ad_title, String price_unit, String year_of_construction, String image1, String image2, String image3, String image4, String image5, String image6, ArrayList<String> amenitieslist) {
        this.id = id;
        this.post_unit = post_unit;
        this.latitude = latitude;
        this.longitude = longitude;
        this.prize = prize;
        this.bed = bed;
        this.area = area;
        this.bath = bath;
        this.floor = floor;
        this.address = address;
        this.area_unit = area_unit;
        this.full_name = full_name;
        this.description = description;
        this.property_type = property_type;
        this.property_category = property_category;
        this.property_pictures = property_pictures;
        this.ad_title = ad_title;
        this.price_unit = price_unit;
        this.year_of_construction = year_of_construction;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.image4 = image4;
        this.image5 = image5;
        this.image6 = image6;
        this.amenitieslist = amenitieslist;
    }

    public String getPrice_unit() {
        return price_unit;
    }

    public void setPrice_unit(String price_unit) {
        this.price_unit = price_unit;
    }

    public String getYear_of_construction() {
        return year_of_construction;
    }

    public void setYear_of_construction(String year_of_construction) {
        this.year_of_construction = year_of_construction;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getImage6() {
        return image6;
    }

    public void setImage6(String image6) {
        this.image6 = image6;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPost_unit() {
        return post_unit;
    }

    public void setPost_unit(String post_unit) {
        this.post_unit = post_unit;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAd_title() {
        return ad_title;
    }

    public void setAd_title(String ad_title) {
        this.ad_title = ad_title;
    }

    public ArrayList<String> getAmenitieslist() {
        return amenitieslist;
    }

    public void setAmenitieslist(ArrayList<String> amenitieslist) {
        this.amenitieslist = amenitieslist;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getBath() {
        return bath;
    }

    public void setBath(String bath) {
        this.bath = bath;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(String area_unit) {
        this.area_unit = area_unit;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getProperty_type() {
        return property_type;
    }

    public void setProperty_type(String property_type) {
        this.property_type = property_type;
    }

    public String getProperty_category() {
        return property_category;
    }

    public void setProperty_category(String property_category) {
        this.property_category = property_category;
    }

    public String getProperty_pictures() {
        return property_pictures;
    }

    public void setProperty_pictures(String property_pictures) {
        this.property_pictures = property_pictures;
    }
}
