package com.example.realestate.Models;

public class GetAmentiesModel {
    private String amenties;

    public GetAmentiesModel(String amenties) {
        this.amenties = amenties;
    }

    public String getAmenties() {
        return amenties;
    }

    public void setAmenties(String amenties) {
        this.amenties = amenties;
    }
}
