package com.example.realestate.Models;

public class AreaTypeModel {
    int id = 0;
    String area_type = "";

    public AreaTypeModel(int id, String area_type) {
        this.id = id;
        this.area_type = area_type;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArea_type(String area_type) {
        this.area_type = area_type;
    }
}
