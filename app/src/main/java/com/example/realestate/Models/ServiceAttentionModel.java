package com.example.realestate.Models;

public class ServiceAttentionModel {
    private String serviceName;
    private int id;

    private boolean isCheck;

    public ServiceAttentionModel(String serviceName, int id) {
        this.serviceName = serviceName;
        this.id = id;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
