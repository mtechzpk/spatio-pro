package com.example.realestate.api;

public interface Constants {

    interface URL {

        String BASE_URL = "http://mtecsoft.com/vreminder/public/api/";
        String IMAGE_BASE_URL = "http://123openthedoor.com/vreminder/public/";


        String SIGN_UP = BASE_URL + "signup";
        String LOGIN = BASE_URL + "login";
        String IMAGE_REMINDER = BASE_URL + "add_image_reminder";
        String GET_IMAGE_REMINDER = BASE_URL + "get_image_reminders";
        String DELETE_REMINDER = BASE_URL + "delete_reminder";
        String ADD_TEXT_REMINDER = BASE_URL + "add_text_reminder";
        String GET_TEXT_REMINDER = BASE_URL + "get_text_reminder";
        String EDIT_IMAGE_REMINDER = BASE_URL + "edit_image_reminder";
        String EDIT_TEXT_REMINDER = BASE_URL + "edit_image_reminder";
        String ADD_VOICE_REMINDER = BASE_URL + "add_voice_reminder";
        String GET_VOICE_REMINDER = BASE_URL + "get_voice_reminders";
        String FORGOT_PASSWORD = BASE_URL + "forgot_password";
        String VERIFY_CODE = BASE_URL + "verify_code";
        String RESET_PASS = BASE_URL + "reset_password";
        String ADD_COLLEGE_IMAGE = BASE_URL + "add_images_to_collage";
        String GET_COLLEGE_IMAGE = BASE_URL + "get_collage_images";



    }
}